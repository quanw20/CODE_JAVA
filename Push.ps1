# 推送到gitee
Function Push-Java {
    Set-Location D:\workspaceFolder\CODE_JAVA 
    $now = Get-Date 
    $msg = "==> " + $now.ToString('yyyy年MM月dd日 HH:mm:ss') + " 提交 <=="
    Write-Output $msg >> .\gitpush.log 
    git status >> .\gitpush.log 
    git add . >> .\gitpush.log
    git commit -m $msg  >> .\gitpush.log
    git push -u gitee master  >> .\gitpush.log
}
Push-Java