# Java常出现的错误

## java
### 设置环境变量


我的环境变量
```
%SystemRoot%\system32;%SystemRoot%;%SystemRoot%\System32\Wbem;%SYSTEMROOT%\System32\WindowsPowerShell\v1.0;D:\Program Files\Java\graalvm-ee-java17-22.0.0\bin;C:\Users\QUANWEI\AppData\Local\Programs\Python\Python39\Scripts;%PyCharm%;C:\Program Files\CMake\bin;C:\Program Files\dotnet;C:\Program Files\Git\bin;C:\Program Files\Microsoft VS Code\bin;C:\Program Files\nodejs;C:\Program Files (x86)\dotnet;D:\ZIPS\ffmpeg-N-103179-gac0408522a-win64-gpl\bin;D:\ZIPS\mingw64\bin;D:\ZIPS\Neovim\bin;

path

C:\Program Files (x86)\Common Files\Oracle\Java\javapath;%SystemRoot%\system32;%SystemRoot%;%SystemRoot%\System32\Wbem;%SYSTEMROOT%\System32\WindowsPowerShell\v1.0;%JAVA_HOME%\bin;C:\Users\QUANWEI\AppData\Local\Programs\Python\Python39\Scripts;%PyCharm%;C:\Program Files\CMake\bin;C:\Program Files\dotnet;C:\Program Files\Microsoft VS Code\bin;C:\Program Files\nodejs;C:\Program Files (x86)\dotnet;D:\ZIPS\ffmpeg-N-103179-gac0408522a-win64-gpl\bin;D:\ZIPS\mingw64\bin;D:\ZIPS\Neovim\bin;C:\Program Files\Git\cmd;C:\Windows\WinSxS;C:\ProgramData\chocolatey;C:\Program Files (x86)\dotnet\;C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Tools\Llvm\bin;%GRAALVM_HOME%\bin;
```
### 1. cannot be resolved to a variable

无法解析为变量  
    一种情况，变量名和之前声明的不一样，应该是写错了，用快捷键拼全试试  
    二种情况，数据类型出现问题，不匹配  

### 2. The method sort(int[]) in the type Arrays is not applicable for the arguments (int[], (<no type> x, <no type> y) -> {})

```java
    int[] a = new int[m];
    for (int i = 0; i < m; i++) 
        a[i] = i + 1;
    Arrays.sort(a, (x, y) -> x - y);
```

改为

```java
    Integer[] a = new Integer[m];
    for (Integer i = 0; i < m; i++)
        a[i] = i + 1;
Arrays.sort(a, (x, y) -> x - y);
```

### 3. 字符串编码问题

```java
String string = new String(str.getBytes("GBK"), "ISO-8859-1");
```

### Scanner nextLine

```java
        int n = in.nextInt();
        in.nextLine();// 读取残留的回车符
        String line = in.nextLine(); //如果不加上一句读取到的是\r\n
```

### ConcurrentModificationException

遍历一个集合时如何避免ConcurrentModificationException 在迭代时只可以用迭代器进行删除! 

单线程情况：

（1）使用Iterator提供的remove方法，用于删除当前元素。

（2）建立一个集合，记录需要删除的元素，之后统一删除。

（3）不使用Iterator进行遍历，需要之一的是自己保证索引正常。

（4）使用并发集合类来避免ConcurrentModificationException，比如使用CopyOnArrayList，而不是ArrayList。

多线程情况：

（5）使用并发集合类，如使用ConcurrentHashMap或者CopyOnWriteArrayList。

## vscode 莫名出现 错误: 找不到或无法加载主类 algorithms.datastructure.tree.Serialize
清理工作区缓存


## .gitignore文件不起作用

除掉本地项目的Git缓存，通过重新创建Git索引

```bash
# 0. 进入项目路径
# 1. 清除本地当前的Git缓存
git rm -r --cached .

# 2. 应用.gitignore等本地配置文件重新建立Git索引
git add .

# 3. （可选）提交当前Git版本并备注说明
git commit -m 'update .gitignore'
```