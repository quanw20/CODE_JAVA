# CODE_JAVA

<a href='https://gitee.com/quanw20/CODE_JAVA/stargazers'><img src='https://gitee.com/quanw20/CODE_JAVA/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/quanw20/CODE_JAVA'><img src='https://gitee.com/quanw20/CODE_JAVA/widgets/widget_4.svg' alt='Fork me on Gitee'></img></a>

## 介绍
**以下是 CODE_JAVA 项目 说明**  
CODE_JAVA 是一个用来练习Java的日常使用,和尝试Java新特性的项目

## 文件目录

---
.  
├── gitpush.log     
├── lib `jar文件`       
├── LICENSE `许可证`        
├── PROBLEMS.md `遇到的问题`        
├── push `自动推送`     
├── Push.ps1 `自动推送`     
├── README.md       
├── src     
│   ├── algorithms `算法`       
│   ├── designpattern `设计模式`        
│   ├── jni `Java Native Invoke`        
│   ├── lang `Java 基础语法`        
│   ├── newskill `新特性`       
│   ├── resources `资源目录`        
│   └── test `测试目录`     
└── ToDo.md `todo list`     

---

## 1 ***标识符***

## 1.1 对所有标识符都通用的规则

标识符只能使用ASCII字母和数字，因此每个有效的标识符名称都能匹配正则表达式\w+。

在Google其它编程语言风格中使用的特殊前缀或后缀，如name_, mName, s_name和kName，在Java编程风格中都不再使用。

## 1.2 标识符类型的规则

### 1.2.1 包名

包名全部小写，连续的单词只是简单地连接起来，不使用下划线。


### 1.2.2 类名

类名都以UpperCamelCase风格编写。

类名通常是名词或名词短语，接口名称有时可能是形容词或形容词短语。现在还没有特定的规则或行之有效的约定来命名注解类型。

测试类的命名以它要测试的类的名称开始，以Test结束。例如，HashTest或HashIntegrationTest。

### 1.2.3 方法名

方法名都以lowerCamelCase风格编写。

方法名通常是动词或动词短语。

下划线可能出现在JUnit测试方法名称中用以分隔名称的逻辑组件。一个典型的模式是：test<MethodUnderTest>_<state>，例如testPop_emptyStack。 并不存在唯一正确的方式来命名测试方法。

### 1.2.4 常量名

常量名命名模式为CONSTANT_CASE，全部字母大写，用下划线分隔单词。那，到底什么算是一个常量？

每个常量都是一个**静态final**字段，但不是所有静态final字段都是常量。在决定一个字段是否是一个常量时， 考虑它是否真的感觉像是一个常量。例如，如果任何一个该实例的观测状态是可变的，则它几乎肯定不会是一个常量。 只是永远不打算改变对象一般是不够的，它要真的一直不变才能将它示为常量。

```java
// Constants
static final int NUMBER = 1;
static final ImmutableList<String> NAMES = ImmutableList.of("Ed", "Ann");
static final Joiner COMMA_JOINER = Joiner.on(',');  // because Joiner is immutable
static final SomeMutableType[] EMPTY_ARRAY = {};
enum SomeEnum { ENUM_CONSTANT }

// Not constants
static String nonFinal = "non-final";
final String nonStatic = "non-static";
static final Set<String> mutableCollection = new HashSet<String>();
static final ImmutableSet<SomeMutableType> mutableElements = ImmutableSet.of(mutable);
static final Logger logger = Logger.getLogger(MyClass.getName());
static final String[] nonEmptyArray = {"these", "can", "change"};
```

这些名字通常是名词或名词短语。

## 1.2.5 非常量字段名

非常量字段名以lowerCamelCase风格编写。

这些名字通常是名词或名词短语。

## 1.2.6 参数名

参数名以lowerCamelCase风格编写。

参数应该避免用单个字符命名。

## 1.2.7 局部变量名

局部变量名以lowerCamelCase风格编写，比起其它类型的名称，局部变量名可以有更为宽松的缩写。

虽然缩写更宽松，但还是要避免用单字符进行命名，除了临时变量和循环变量。

即使局部变量是final和不可改变的，也不应该把它示为常量，自然也不能用常量的规则去命名它。

## 1.2.8 类型变量名

类型变量可用以下两种风格之一进行命名：

单个的大写字母，后面可以跟一个数字(如：E, T, X, T2)。
以类命名方式，后面加个大写的T(如：RequestT, FooBarT)。

### 1.2.9 驼峰式命名法(CamelCase)

驼峰式命名法分大驼峰式命名法(UpperCamelCase)和小驼峰式命名法(lowerCamelCase)。 有时，我们有不只一种合理的方式将一个英语词组转换成驼峰形式，如缩略语或不寻常的结构(例如"IPv6"或"iOS")。Google指定了以下的转换方案。

名字从散文形式(prose form)开始:

把短语转换为纯ASCII码，并且移除任何单引号。例如："Müller’s algorithm"将变成"Muellers algorithm"。
把这个结果切分成单词，在空格或其它标点符号(通常是连字符)处分割开。
推荐：如果某个单词已经有了常用的驼峰表示形式，按它的组成将它分割开(如"AdWords"将分割成"ad words")。 需要注意的是"iOS"并不是一个真正的驼峰表示形式，因此该推荐对它并不适用。
现在将所有字母都小写(包括缩写)，然后将单词的第一个字母大写：最后将所有的单词连接起来得到一个标识符。
每个单词的第一个字母都大写，来得到大驼峰式命名。
除了第一个单词，每个单词的第一个字母都大写，来得到小驼峰式命名。
示例：


```java
Prose form                Correct               Incorrect
------------------------------------------------------------------
"XML HTTP request"        XmlHttpRequest        XMLHTTPRequest
"new customer ID"         newCustomerId         newCustomerID
"inner stopwatch"         innerStopwatch        innerStopWatch
"supports IPv6 on iOS?"   supportsIpv6OnIos     supportsIPv6OnIOS
"YouTube importer"        YouTubeImporter
                          YoutubeImporter*
```

加星号处表示可以，但不推荐。

Note：在英语中，某些带有连字符的单词形式不唯一。例如："nonempty"和"non-empty"都是正确的，因此方法名checkNonempty和checkNonEmpty也都是正确的。

---

## 2 ***Java常用命令***

### 2.1 javac命令 编译.java -> 生成.class文件

```bash
javac [options] <source files>
```

### 2.2 java命令 执行一个类

```bash
# (to execute a class)
java [options] <mainclass> [args...]
# (to execute a jar file)
java [options] -m <module>[/<mainclass>] [args...]
```

### 2.3 jar命令 打包.class -> .jar

```bash
jar {ctxui}[vfmn0PMe] [jar-file] [manifest-file] [entry-point] [-C dir] files ...

# Example 1: to archive two class files into an archive called classes.jar:
       jar cvf classes.jar Foo.class Bar.class

# Example 2: use an existing manifest file 'mymanifest' and archive all the files in the foo/ directory into 'classes.jar':
       jar cvfm classes.jar mymanifest -C foo/ .
```

### 2.4 javadoc命令 生成文档

```bash
javadoc [options] [packagenames] [sourcefiles] [@files]
```

## 3 名词

POJOs有时候也称作Plain Ordinary Java Objects，表示一个数据集合。

POJO只是一个普通的，已删除限制的Java Bean。 Java Bean必须满足以下要求：
1. 默认无参数构造函数
2. 对于值foo为可变属性，遵循get()方法和set()方法的Bean协议；如果值foo是不可变的，则不使用setFoo。
3. 必须实现java的序列化(java.io.Serializable)
POJO不强制执行这些操作。顾名思义：在JDK下编译的对象可以被认为是一个普通的Java对象。没有应用服务器，没有基类，没有需要使用的接口。

## 数据库对象命名规范

| 类型 | 前缀 |
|         -|   -|
|表前缀     | t_ |
|视图前缀   | v_ |
|同义词前缀  | s_ |
|簇表前缀    | c_ |
|序列前缀    | seq_ |
|存储过程前缀| p_ |
|函数前缀    | f_ |
|包前缀     | pkg_ |
|类前缀     | tp_ |
|主键前缀    | pk_ |
|外键前缀    | fk_ |
|唯一索引前缀| ux_ |
|普通索引前缀| idx_ |
|位图索引前缀| bx_ |
|函数索引前缀| fx_ |

## **JAVA 学习路径**

## 学习

    1. 掌握一门编译语言 Java
    2. 掌握一门脚本语言 Python
    3. 掌握算法与数据结构 
    4. 掌握开发环境 VScode, IDEA
    5. 丰富前沿知识 

### [第一阶段 Java 基础](https://quanw20.gitee.io/2022/01/28/Java%E5%9F%BA%E7%A1%80/)

    Java编程的基础语法知识点:
    1. JAVA的特点(JDK, JRE, JVM)
    2. 变量和基本数据类型
        1)整型(byte, short, int(20亿), long)
            java整数没有无符号类型
        2)浮点型(float, double)
            POSTIVE_INFINITY
            NEGATIVE_INFINTY
            NaN
        3)char(2字节)
        4)boolean
        5)包装类型
            Byte, Short, Integer, Long
            Float, Double
            Char
            Boolean
    3. 大数
        1)BigInteger
        2)BigDecimal
    4. 运算符
        1)位运算: &, |, ^(xor), ~(not)
        2)>>>0填充高位,>>符号位填充高位
    5. 字符串
        1)String
        2)StringBuilder
        3)StringBuffer
    6. 控制流程
        1)if-else if-else
        2)switch
        3)for, for_each
        4)while, do-while
        5)break, continue
    7. 数组
        1)数组是对象
        2)Arrays工具类
    8. 面向对象
        1)封装
            修饰符, geter, setter
        2)继承
            extends, implements, super, this
        3)多态(Polymorphic)
            override,
        4)抽象类
            abstract
        5)接口
            interface
        6)反射, 注解
        7)Object类
        8)异常
        9)泛型
    9.  集合 JCF
        1)Collections
        2)List
        3)Map, Set
        5)Queue, PriorityQueue
    10. IO
        1)Blocking-IO
        2)Non-Blocking-IO
        2)Asynchronous-IO
    11. 网络
        1)URL
        2)IP
        3)Socket 
    12. JDBC
        1)MySql
        2)PostgreSql
        3)SQLite
    13. 多线程
        1)Thread
        2)Runnable
        3)线程池
    14. GUI
        1)AWT
        2)Swing
        3)Java FX
    15. 函数式
        1)Stream API
        2)Funcational interface

### [第二阶段 JAVA Web](https://gitee.com/quanw20/jsp-curriculum-desing)

    1. html
    2. css
    3. JavaScript
    4. jQuery
    5. XML
    6. Tomcat
    7. Servlet
    8. Filter
    9. Listener
    10. JSP
    11. JSON
    12. Ajax

### 第三阶段 框架

    后端  
    1. Spring Framework  
        IoC   
        AOP  
    2. Spring MVC  
    3. Mibatis  
    4. Spring Data  
        JDBC  
        JPA  
    5. Spring Boot  
    6. Spring Cloud  
    7. Netty  
    8. Vert.x  

    前端  
    1.React  
        jsx  
        hooks  
        fetch  
        axios   

