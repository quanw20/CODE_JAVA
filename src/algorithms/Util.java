/*
 * Copyright (c) 2002, 2021, Tang Quanwei. All rights reserved.
 *
 *  _____                    ___                                _ 
 * |_   _|_ _ _ __   __ _   / _ \ _   _  __ _ _ ____      _____(_)
 *   | |/ _` | '_ \ / _` | | | | | | | |/ _` | '_ \ \ /\ / / _ \ |
 *   | | (_| | | | | (_| | | |_| | |_| | (_| | | | \ V  V /  __/ |
 *   |_|\__,_|_| |_|\__, |  \__\_\\__,_|\__,_|_| |_|\_/\_/ \___|_|
 *                  |___/
 * 
 * 
 *                                          )  (  (    (
 *                                          (  )  () @@  )  (( (
 *                                      (      (  )( @@  (  )) ) (
 *                                    (    (  ( ()( /---\   (()( (
 *      _______                            )  ) )(@ !O O! )@@  ( ) ) )
 *     <   ____)                      ) (  ( )( ()@ \ o / (@@@@@ ( ()( )
 *  /--|  |(  o|                     (  )  ) ((@@(@@ !o! @@@@(@@@@@)() (
 * |   >   \___|                      ) ( @)@@)@ /---\-/---\ )@@@@@()( )
 * |  /---------+                    (@@@@)@@@( // /-----\ \\ @@@)@@@@@(  .
 * | |    \ =========______/|@@@@@@@@@@@@@(@@@ // @ /---\ @ \\ @(@@@(@@@ .  .
 * |  \   \\=========------\|@@@@@@@@@@@@@@@@@ O @@@ /-\ @@@ O @@(@@)@@ @   .
 * |   \   \----+--\-)))           @@@@@@@@@@ !! @@@@ % @@@@ !! @@)@@@ .. .
 * |   |\______|_)))/             .    @@@@@@ !! @@ /---\ @@ !! @@(@@@ @ . .
 *  \__==========           *        .    @@ /MM  /\O   O/\  MM\ @@@@@@@. .
 * Q   |   |-\   \          (       .      @ !!!  !! \-/ !!  !!! @@@@@ .
 * U   |   |  \   \          )   -TANG-   .  @@@@ !!     !!  .(. @.  .. .
 * A   |   |   \   \        (    /   .(  . \)). ( |O  )( O! @@@@ . )      .
 * N   |   |   /   /         ) (      )).  ((  .) !! ((( !! @@ (. ((. .   .
 * W   |   |  /   /   ()  ))   ))   .( ( ( ) ). ( !!  )( !! ) ((   ))  ..
 * E   |   |_<   /   ( ) ( (  ) )   (( )  )).) ((/ |  (  | \(  )) ((. ).
 * I___<_____\\__\__(___)_))_((_(____))__(_(___.oooO_____Oooo.(_(_)_)((_
 */
package algorithms;

import java.util.Arrays;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 静态工具类
 */
public class Util {

    // 随机数种子
    private static final long SEED = 0L;
    // nextInt 范围
    private static final int BOUND = 100;
    // 一个素数
    public static final int PRIME = 5301;

    /**
     * 静态随机数对象 每次都初始化成相同的
     */
    public static final Random random = new Random(SEED);

    /**
     * 获取一个数组 元素有序
     *
     * @param a <起始下标(包括)>,[结束下标(不包括)],[步长]
     * @return 数组
     */
    public static int[] range(int... a) {
        int begin = 0, end, step = 1, length = 1;
        int[] ret;
        switch (a.length) {
            case 1:
                end = a[0];
                length = end;
                break;
            case 2:
                begin = a[0];
                end = a[1];
                length = end - begin;
                break;
            case 3:
                begin = a[0];
                end = a[1];
                step = a[2];
                length = (end - begin) / step;
            default:
                break;
        }
        ret = new int[length];
        for (int i = 0, j = begin; i < length; ++i) {
            ret[i] = j;
            j += step;
        }
        return ret;
    }

    /**
     * 获取一个由随机数组成的数组
     *
     * @param length 数组长度
     * @return 一个由随机数组成的数组
     */
    public static int[] getRandomArray(int length) {
        int[] ret = new int[length];
        for (int i = 0; i < length; i++) {
            ret[i] = random.nextInt(BOUND);
        }
        return ret;
    }

    /**
     * 获取一个二维数组 [0,row*col-1]
     *
     * @param row 行数
     * @param col 列数
     * @return 一个二维数组
     */
    public static int[][] m2d(int row, int col) {
        int[][] ret = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                ret[i][j] = i * col + j;
            }
        }
        return ret;
    }

    /**
     * 获取一个二维数组 元素随机
     *
     * @param row 行数
     * @param col 列数
     * @return 一个二维数组 元素随机
     */
    public static int[][] m2dr(int row, int col) {
        int[][] ret = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                ret[i][j] = random.nextInt(-80, BOUND);
            }
        }
        return ret;
    }

    /**
     * 打印一维数组
     *
     * @param a 数组
     */
    public static void print(int[] a) {
        for (int i : a) {
            System.out.printf("%-3d", i);
        }
        System.out.println();
    }

    /**
     * 打印二维数组
     *
     * @param a 二维数组
     * @return str
     */
    public static String print(int[][] a) {
        if (a == null) {
            System.out.println("矩阵为空");
        }
        StringBuilder sb = new StringBuilder(a.length * a[0].length + a[0].length);
        for (int[] a1 : a) {
            for (int j = 0; j < a1.length; j++) {
                sb.append(String.format("%4d", a1[j]));
            }
            sb.append("\n");
        }
        System.out.println(sb);
        return sb.toString();
    }

    /**
     * 打印二维数组
     *
     * @param a
     * @return
     */
    public static String print(char[][] a) {
        if (a == null) {
            System.out.println("矩阵为空");
        }
        StringBuilder sb = new StringBuilder(a.length * a[0].length + a[0].length);
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                sb.append(String.format("%2c", a[i][j]));
            }
            sb.append("\n");
        }
        System.out.println(sb);
        return sb.toString();
    }

    /**
     * 交换数组对应下标的值
     *
     * @param a 数组
     * @param i 下标
     * @param j 下标
     */
    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static boolean less(int a, int b) {
        return a < b;
    }

    public static boolean greater(int a, int b) {
        return a > b;
    }

    /**
     * 将int[] 包装为 Integer[]
     *
     * @param a int[]
     * @return Integer[]
     */
    public static Integer[] box(int[] a) {
        // 数组元素转换为数值流
        IntStream stream = Arrays.stream(a);
        // 流中元素全部装箱
        Stream<Integer> st = stream.boxed();
        // 将流转换为数组
        return st.toArray(Integer[]::new);
    }

    /**
     * 数组深拷贝 同clone
     *
     * @param a 数组
     * @return 深拷贝
     */
    public static int[] deepCopy(int[] a) {
        int[] ret = new int[a.length];
        System.arraycopy(a, 0, ret, 0, a.length);
        return ret;
    }

    /**
     * 数组深拷贝
     *
     * 二维数组的clone是浅拷贝
     *
     * @param a 二维数组
     * @return 深拷贝
     */
    public static int[][] deepCopy(int[][] a) {
        int[][] ret = new int[a.length][];
        for (int i = 0, l = a.length; i < l; i++) {
            ret[i] = a[i].clone();
        }
        return ret;
    }

    /**
     * 二维数组深拷贝
     *
     * @param a 源数组
     * @param rowIndex 起始行下标
     * @param rows 要复制行数
     * @param colIndex 起始列下标
     * @param cols 要复制的列数
     * @return 结果数组 或者 {@code null}
     */
    public static int[][] deepCopy(int[][] a, int rowIndex, int rows, int colIndex, int cols) {
        if (rows == 0 || cols == 0) {
            return null;
        }
        int[][] ret = new int[rows][cols];
        int idx = 0;
        for (int i = 0; i < rows; i++) {
            System.arraycopy(a[i + rowIndex], colIndex, ret[idx++], 0, cols);
        }
        return ret;
    }

    /**
     * 快速幂 a^b%m
     *
     * @param a 底数
     * @param b 指数
     * @param m 模
     * @return a^b%m
     */
    public static long pow(int a, int b, int m) {
        long ret = 1;
        while (b > 0) {
            if ((b & 1) != 0) {// b最后一位是奇数1
                ret = ret * a % m;
            }
            a = a * a % m;
            b >>= 1;
        }
        return ret;
    }

    /**
     * 快速幂 a^b%INT_MAX
     *
     * @param a 底数
     * @param b 指数
     * @return 幂
     */
    public static int pow(int a, int b) {
        long ret = 1;
        while (b > 0) {
            if ((b & 1) != 0)// b最后一位是奇数1
            {
                ret *= a;
            }
            a *= a;
            b >>= 1;
        }
        return (int) ret % Integer.MAX_VALUE;
    }

    /**
     * 将一个全是数值的字符串转化成int[]
     *
     * <pre>
     * eg:
     *
     * "[1, 2, 3, 4, 5]" => {1, 2, 3, 4, 5}
     * </pre>
     *
     * @param input 全是数值的字符串
     * @return int[]
     */
    public static int[] toIntArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for (int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    /**
     * 去重
     *
     * @param a
     * @return
     */
    public static int[] deWeight(int[] a) {
        return Arrays.stream(a).distinct().toArray();
    }

    /**
     * 泛型去重(好像有点点问题:- )
     *
     * @param <T>
     * @param a
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] distinct(T[] a) {
        return (T[]) Arrays.stream(a).distinct().toArray();
    }

    /**
     * 数组{@code a }是否包含元素{@code e}
     *
     * @param a 数组
     * @param e 元素
     * @return 是否包含
     */
    public static boolean contains(int[] a, int e) {
        for (int i = 0; i < a.length; ++i) {
            if (a[i] == e) {
                return true;
            }
        }
        return false;
    }

    /**
     * 用引用类型填充数组, 避免使用相同引用
     *
     * ArrayList<Integer>[] a = new ArrayList[10]; Util.fill(a, () -> new
     * ArrayList<>()); System.out.println(a[1] == a[2]);// false
     *
     * @param <T> 引用类型, 基本类型使用Arrays.fill()
     * @param a 待填充数组
     * @param func 填充使用的函数
     */
    public static <T> void fill(T[] a, Supplier<T> func) {
        for (int i = 0, l = a.length; i < l; ++i) {
            a[i] = func.get();
        }
    }

    /**
     * forEach
     *
     * @param <T> 引用类型
     * @param a 数组
     * @param consumer 要做的事
     */
    public static <T> void forEach(T[] a, Consumer<T> consumer) {
        for (T t : a) {
            consumer.accept(t);
        }
    }

    /**
     * 给定范围中最大值的下标
     *
     * @param a 数组
     * @param s 起始位置(包括)
     * @param e 结束位置(不包括)
     * @return 最大值的下标 or {@code -1}表示s>=e
     */
    public int maxIndex(int[] a, int s, int e) {
        int max = Integer.MIN_VALUE, index = -1;
        for (int i = s; i < e; i++) {
            if (a[i] > max) {
                max = a[i];
                index = i;
            }
        }
        return index;
    }

    /**
     * 给定范围中最小值的下标
     *
     * @param a 数组
     * @param s 起始位置(包括)
     * @param e 结束位置(不包括)
     * @return 最小值的下标 or {@code -1}表示s>=e
     */
    public int minIndex(int[] a, int s, int e) {
        int min = Integer.MAX_VALUE, index = -1;
        for (int i = s; i < e; i++) {
            if (a[i] < min) {
                min = a[i];
                index = i;
            }
        }
        return index;
    }
}
