package algorithms.dp.section;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * NC50493
 * 链接：https://ac.nowcoder.com/acm/problem/50493
 * 
 * 将n堆石子绕圆形操场排放，现要将石子有序地合并成一堆。规定每次只能选相邻的两堆合并成新的一堆，并将新的一堆的石子数记做该次合并的得分。
 * 请编写一个程序，读入堆数n及每堆的石子数，并进行如下计算：
 * 选择一种合并石子的方案，使得做n-1次合并得分总和最大。
 * 选择一种合并石子的方案，使得做n-1次合并得分总和最小。
 * 
 * 输入描述:
 * 输入第一行一个整数n，表示有n堆石子。
 * 第二行n个整数，表示每堆石子的数量。
 * 输出描述:
 * 第一行为合并得分总和最小值，
 * 第二行为合并得分总和最大值。
 */
public class NC50493 {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        int n = in.nextInt();
        int[] a = new int[n + 1 << 1];
        int[] s = new int[n + 1 << 1];
        int[][] ma = new int[n + 1 << 1][n + 1 << 1];// 合并[i,j]的所有石子的得分
        int[][] mi = new int[n + 1 << 1][n + 1 << 1];// 合并[i,j]的所有石子的得分
        for (int i = 1; i <= 2 * n; i++)
            for (int j = 1; j <= 2 * n; j++)
                mi[i][j] = i == j ? 0 : 127 / 3;
        for (int i = 1; i <= n; ++i)
            a[i + n] = a[i] = in.nextInt();
        for (int i = 1; i <= n << 1; ++i)
            s[i] = s[i - 1] + a[i];
        for (int len = 2; len <= n; ++len) { // 枚举长度
            for (int l = 1; l <= n * 2 - len + 1; ++l) {// 枚举起点
                int r = l + len - 1; // 计算终点()
                for (int k = l; k < r; ++k) {// 枚举中间节点
                    ma[l][r] = Math.max(ma[l][r], ma[l][k] + ma[k + 1][r] + s[r] - s[l - 1]);
                    mi[l][r] = Math.min(mi[l][r], mi[l][k] + mi[k + 1][r] + s[r] - s[l - 1]);
                }
            }
        }
        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;
        for (int i = 1; i <= n; ++i) {
            max = Math.max(max, ma[i][i + n - 1]);
            min = Math.min(min, mi[i][i + n - 1]);
        }
        System.out.println(min);
        System.out.println(max);
    }
}