package algorithms.dp.knapsack;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import algorithms.Util;

/**
 * 0-1 背包
 * 
 * 每个物品只有选和不选两种情况
 * 
 * 从obj(w,v)中选取重量不超过weight的物品,返回价值的最大值
 * 输入:
 * objs={{2,3},{1,2},{3,4},{2,2}}
 * weight=5
 * 输出:
 * 7
 */
public class _01Knapsack {

    /**
     * dfs 二进制
     * O(2^n*n)
     * 
     * @param objs
     * @param weight
     * @return
     */
    public static int binary01(int[][] objs, int weight) {
        int max = 0;
        int n = Util.pow(2, objs.length);
        for (int i = 0; i < n; i++) {
            int w = 0, v = 0;
            for (int j = objs.length - 1; j >= 0; j--) {
                if (((i >> j) & 1) == 1) {
                    w += objs[j][0];
                    v += objs[j][1];
                }
            }
            if (w <= weight && v > max) {
                max = v;
            }
        }
        return max;
    }

    /**
     * dfs
     * O(2^n)
     * 
     * @param objs
     * @param weight
     * @return
     */
    public static int dfs01(int[][] objs, int weight) {
        final class Dfs {
            static int[][] objs;

            static int dfs(int i, int weight) {
                if (weight == 0)
                    return 0;
                if (i == objs.length)
                    return 0;
                int v1 = dfs(i + 1, weight);// 不选
                if (weight >= objs[i][0]) {
                    int v2 = objs[i][1] + dfs(i + 1, weight - objs[i][0]);// 选
                    return v2/* Math.max(v1, v2) */;
                }
                return v1;
            }
        }
        Dfs.objs = objs;
        return Dfs.dfs(0, weight);
    }

    /**
     * 有重叠子问题->使用记忆型递归
     * 
     * @param objs
     * @param weight
     * @return
     */
    public static int dfs01Record(int[][] objs, int weight) {
        class Dfs {
            static int[][] rec;
            static int[][] objs;

            public static int dfs(int i, int w) {
                if (w == 0)
                    return 0;
                if (i == objs.length)
                    return 0;
                // 1.计算前先查询
                if (rec[i][w] >= 0)
                    return rec[i][w];
                int v1 = dfs(i + 1, w);// 不选
                int ans = 0;
                if (w >= objs[i][0]) {
                    int v2 = objs[i][1] + dfs(i + 1, w - objs[i][0]);// 选
                    ans = v2/* Math.max(v1, v2) */;
                } else {
                    ans = v1;
                }
                // 2.返回前先记录
                rec[i][w] = ans;
                return ans;

            }
        }
        Dfs.rec = new int[objs.length][weight + 1];
        for (int i = 0, l = Dfs.rec.length; i < l; i++) {
            Arrays.fill(Dfs.rec[i], -1);
        }
        Dfs.objs = objs;
        return Dfs.dfs(0, weight);
    }

    /**
     * 使用动态规划
     * 从obj(w,v)中选取重量不超过weight的物品,返回价值的最大值
     * 自底向上
     * 
     * 当前背包=={能选->max(选,不选)
     * ---------{不能选->(不选)
     * 
     * <pre>
     * +-----+------------+------------+------------+------------+------------+------------+
     * |(w,v)|0           |1           |2           |3           |4           |5           |
     * +-----+------------+------------+------------+------------+------------+------------+
     * |(2,3)|0           |0           |3=max(3+0,0)|5=max(2+3,3)|5=max(2+3,3)|5=max(2+3,3)|
     * +-----+------------+------------+------------+------------+------------+------------+
     * |(1,2)|0           |2=max(2+0,0)|3=max(2+0,3)|5=max(2+3,3)|6=max(4+2,5)|7=max(4+3,5)|
     * +-----+------------+------------+------------+------------+------------+------------+
     * |(3,4)|0           |2           |3           |5=max(4+0,5)|6=max(2+3,6)|7=max(2+5,6)|
     * +-----+------------+------------+------------+------------+------------+------------+
     * |(2,2)|0           |2           |3=max(2+0,3)|5=max(2+2,5)|6=max(2+3,6)|7=max(2+5,7)|
     * +-----+------------+------------+------------+------------+------------+------------+
     * </pre>
     * 
     * @param objs
     * @param weight
     * @return
     */
    public static int dp01(int[][] objs, int weight) {
        int n = objs.length;
        int m = weight + 1;
        int[][] dp = new int[n][m];// 定义并初始化dp数组第一行
        for (int i = 0, j = objs[0][0]; i < m; i++)
            if (i >= j)
                dp[0][i] = objs[0][1];
        for (int i = 1; i < n; i++) // 计算后面每一行
            for (int j = 0; j < m; j++) {
                if (j >= objs[i][0]) {// 可以选
                    int s = objs[i][1] + dp[i - 1][j - objs[i][0]];// 选
                    int r = dp[i - 1][j];// 不选
                    dp[i][j] = Math.max(s, r);
                } else // 没得选
                    dp[i][j] = dp[i - 1][j];// 不选
            }
        return dp[n - 1][m - 1];
    }

    /**
     * 使用滚动数组
     * @param objs
     * @param weight
     * @return
     */
    public static int dp01_2(int[][] objs, int weight) {
        int[] dp = new int[weight + 1];
        for (int i = 0; i < objs.length; ++i)
            for (int j = weight; j > 0; --j) // (之前的状态和现在的状态都在数组里面)从后向前避免使用到当前的状态
                if (j >= objs[i][0])
                    dp[j] = Math.max(objs[i][1] + dp[j - objs[i][0]], dp[j]);// 选和不选中最大的那个
        return dp[weight];
    }

    @Test
    public void test01() {
        int[][] objs = { { 2, 3 }, { 1, 2 }, { 3, 4 }, { 2, 2 } };
        int weight = 5;
        int a = binary01(objs, 5);
        System.out.println(a);

        int dfs01 = dfs01(objs, 5);
        System.out.println(dfs01);

        int dfs01Record = dfs01Record(objs, 5);
        System.out.println(dfs01Record);

        int dp01 = dp01(objs, weight);
        System.out.println(dp01);

        int dp01_2 = dp01_2(objs, weight);
        System.out.println(dp01_2);
    }
}
