package algorithms.dp.tree;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 放一个士兵在节点上可以镇压所有相邻的节点，求镇压整棵树所需的最小士兵数
 * https://ac.nowcoder.com/acm/contest/25022/1004
 */
public class StrategicGame {
    static int N = 1501;
    static int n;
    static int[][] f;// 1:放， 0:不放
    static List<Integer>[] son = new ArrayList[N];

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        int x, y, z;
        while (in.hasNextInt()) {
            int root = -1;
            n = in.nextInt();
            f = new int[n][2];
            for (int i = 0; i < n; ++i)
                son[i] = new ArrayList<>();
            for (int i = 0; i < n; ++i) {
                x = in.nextInt();
                if (root == -1)
                    root = x;
                y = in.nextInt();
                for (int j = 0; j < y; ++j) {
                    z = in.nextInt();
                    son[x].add(z);
                }
            }
            dfs(root);
            System.out.println(Math.min(f[root][0], f[root][1]));
        }
        in.close();
    }

    private static void dfs(int root) {
        ++f[root][1];// 放
        for (int i : son[root]) {// 没有子节点->退出
            dfs(i);
            f[root][1] += Math.min(f[i][0], f[i][1]);// 放
            f[root][0] += f[i][1];// 不放
        }
    }
}
