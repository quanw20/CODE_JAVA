package algorithms.dp;

import java.util.Arrays;

import algorithms.Util;

/**
 * 动态规划(多阶段决策最优化问题)
 * 最优子结构(子问题最优性)(重叠子问题)
 * 本质是递推,核心是找到状态转移的方式,写出dp方程
 * 
 * 阶段:
 * 状态:
 * 决策:
 * 
 * 形式:
 * 记忆型递归(重叠子问题)
 * 递推
 */

public class DynamicProgroming {

    /**
     * 带备忘录的fib
     * 
     * @param n
     * @return
     */

    public static int fib(int n) {
        int[] rec = new int[n + 1];
        int fib = fib(rec, n);
        System.out.println(Arrays.toString(rec));
        return fib;
    }

    /**
     * 
     * @param rec
     * @param n
     * @return
     */
    private static int fib(int[] rec, int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        if (rec[n] > 0)
            return rec[n];
        rec[n] = fib(rec, n - 1) + fib(rec, n - 2);
        return rec[n];
    }


    /**
     * 钢条切割 -- dfs
     * 有一段长度为length的钢条
     * 可以切割为n(n>=0,n=0表示不切割)段卖,价格因长度而不同
     * 
     * steels={1,5,8,16,10,17,17,20,24,30}
     * length=10
     * 
     * @param steels 下标(长度)对应的价格
     * @param length 总长度
     * @return 最大价格
     */
    public static int cutSteel(int[] steels, int length) {
        if (length == 0)
            return 0;
        int ans = 0;
        for (int i = 1; i <= length; i++) {
            int val = steels[i - 1] + cutSteel(steels, length - i);
            ans = Math.max(val, ans);
        }
        return ans;
    }

    /**
     * 钢条切割 -- dfs+记忆型递归
     * 
     * 贪心只利用上一步解,动规可能用到所有历史解
     * 
     * @param steels
     * @param length
     * @return
     */
    public static int cutSteelRecord(int[] steels, int length) {
        // rec[len] 记录长度为len时的最大价值
        int[] rec = new int[length + 1];
        Arrays.fill(rec, -1);
        // 注意
        rec[0] = 0;
        int ans = core(rec, steels, length);
        return ans;
    }

    /**
     * 长度为length时的最大价值
     * 
     * @param rec
     * @param steels
     * @param length
     * @return
     */
    private static int core(int[] rec, int[] steels, int length) {
        // if (length == 0)
        // return 0;
        if (length == 1) {
            return steels[0];
        }
        int ans = 0;
        for (int i = 1; i <= length; i++) {
            if (rec[length - i] == -1) {
                rec[length - i] = core(rec, steels, length - i);
            } else {
                int val = steels[i - 1] + rec[length - i];
                ans = Math.max(val, ans);
            }
        }
        rec[length] = ans;
        return ans;
    }

    /**
     * 数字三角
     * 寻找一条从顶点到边的路径,使得经过的数字和最大
     * 
     * triangle = {
     * { 7 },
     * { 3, 8 },
     * { 8, 1, 0 },
     * { 2, 7, 4, 4 },
     * { 4, 5, 2, 6, 5 }
     * };
     * 
     * 最大
     * 30
     * 
     * @param tri
     * @return
     */
    public static int digitalTriangle(int[][] tri) {
        int[][] rec = new int[tri.length - 1][tri[tri.length - 1].length - 1];
        for (int i = 0; i < rec.length; i++)
            Arrays.fill(rec[i], -1);
        int ans = core(rec, tri, 0, 0);
        Util.print(rec);
        System.out.println(cntOfRecCalled);
        return ans;
    }

    static int cntOfRecCalled = 0;

    private static int core(int[][] rec, int[][] tri, int cur, int len) {
        if (len == tri.length - 1)
            return tri[len][cur];
        int ans = 0;
        if (rec[len][cur] != -1) {
            ans = rec[len][cur];
            cntOfRecCalled++;
        } else {
            int left = core(rec, tri, cur, len + 1);
            int right = core(rec, tri, cur + 1, len + 1);

            ans = tri[len][cur] + Math.max(left, right);
            rec[len][cur] = ans;
        }
        return ans;
    }

    @SuppressWarnings("unused")
    private static int core(int[][] tri, int cur, int len) {
        if (len == tri.length - 1)
            return tri[len][cur];
        int left = core(tri, cur, len + 1);
        int right = core(tri, cur + 1, len + 1);
        int ans = tri[len][cur] + Math.max(left, right);
        return ans;
    }

    /**
     * dp自底向上
     * 
     * 
     * @param tri
     * @return
     */
    public static int digitalTriangleDp(int[][] tri) {
        // int[][] dp = tri.clone();// 二维数组克隆是浅拷贝
        int[][] dp = Util.deepCopy(tri);
        for (int i = tri.length - 2; i >= 0; i--) {
            for (int j = 0, len = tri[i].length; j < len; j++) {
                dp[i][j] = tri[i][j] + Math.max(dp[i + 1][j], dp[i + 1][j + 1]);
            }
        }
        Util.print(dp);
        return dp[0][0];
    }

    /**
     * 使用滚动数组
     * 
     * @param tri
     * @return
     */
    public static int digitalTriangleDpA(int[][] tri) {
        int l = tri.length;
        int[] dp = tri[l - 1].clone();
        for (int i = l - 1; i > 0; i--) {
            for (int j = 0, len = tri[i].length - 1; j < len; j++) {
                dp[j] = Math.max(dp[j], dp[j + 1]) + tri[i][j];
            }
            Util.print(dp);
        }
        dp[0] = Math.max(dp[1], dp[2]) + tri[0][0];
        return dp[0];
    }

    /**
     * 最长公共子序列(LCS)
     * 序列不一定连续,子串一定连续
     * in:
     * BA34C
     * A1BC2
     * out:
     * 2
     * 
     * <pre>
     * s2\s1 B  A   3   4   C
     * A     0  1   1   1   1
     * 1     0  1   1   1   1
     * B     1  1   1   1   1
     * C     1  1   1   1   2
     * 2     1  1   1   1   2
     * </pre>
     * 
     * @param s1
     * @param s2
     * @return
     */
    public static int LSC(String s1, String s2) {
        int l1 = s1.length();
        int l2 = s2.length();
        int[][] dp = new int[l1][l2];
        char c1 = s2.charAt(0);
        // 初始化第一行和第一列
        boolean flag = false;
        for (int i = 0; i < l1; i++) {
            if (flag)
                dp[i][0] = 1;
            else if (s1.charAt(i) == c1) {
                dp[i][0] = 1;
                flag = true;
            }
        }
        c1 = s1.charAt(0);
        flag = false;
        for (int i = 1; i < l2; i++) {
            if (flag)
                dp[0][i] = 1;
            if (s2.charAt(i) == c1) {
                dp[0][i] = 1;
                flag = true;
            }
        }
        // 打表
        for (int i = 1; i < l1; i++) {// s1 - i - l1
            for (int j = 1; j < l2; j++) { // s2 - j - l2
                // 递推方程
                if (s2.charAt(j) == s1.charAt(i)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        return dp[l1 - 1][l2 - 1];
    }

    /**
     * 最大连续子序列和
     * in:
     * -2,11,-4,13,-5,2
     * out:
     * 20
     * dp[0]=-2
     * dp[1]=max(11,11-2)=11
     * dp[2]=max(-4,11-4)=7
     * dp[3]=max(13,13+7)=20 <= max
     * dp[4]=max(-5,20-5)=15
     * dp[5]=max(2,2+15)=17
     * 
     * 状态的无后效性: 即当前状态记录了历史信息,一旦确定就不会再改变,未来的决策只能在已有的状态上进行
     * 
     * @param a
     * @return
     */
    public static int maxCSS(int[] a) {
        int[] dp = new int[a.length];
        dp[0] = a[0];
        // 以a[i]为末尾的连续
        for (int i = 1, l = a.length; i < l; i++) {
            // dp方程
            dp[i] = Math.max(a[i], dp[i - 1] + a[i]);
        }
        // 遍历找最大值
        int max = 0;
        for (int i = 1, l = a.length; i < l; i++) {
            if (dp[i] > dp[max])
                max = i;
        }
        return dp[max];
    }

    /**
     * 最长不下降序列(Longest Increasing Sequence, LIS)
     * in:
     * 1,2,3,-1,-2,7,9
     * out:
     * 5
     * 分析:1,2,3,7,9
     * 
     * dp[i]记录以下标i的元素结尾的序列的最大长度
     * ans是dp中最大的
     * 
     * @param a
     * @return
     */
    public static int LIS(int[] a) {
        int[] dp = new int[a.length];
        int ans = -1;
        for (int i = 0, l = a.length; i < l; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (a[j] < a[i] && (dp[j] + 1 > dp[i])) {
                    dp[i] = dp[j] + 1;
                }
            }
            ans = Math.max(ans, dp[i]);
        }
        return ans;
    }

    /**
     * LIS improved
     * 用后面小的覆盖前面较大的
     * 
     * @param a
     * @return
     */
    public static int LISi(int[] a) {
        int[] dp = new int[a.length];
        dp[0] = a[0];
        int cnt = 0;
        for (int i = 0, l = a.length; i < l; i++) {
            boolean flag = false;
            for (int j = 0; j < cnt; j++) {
                // 如果前面有较大的就覆盖
                if (dp[j] >= a[i]) {// >=避免重复
                    dp[j] = a[i];
                    flag = true;
                }
            }
            // 没有就长度++
            if (!flag)
                dp[cnt++] = a[i];
        }
        return cnt;// 相当于从1开始计数
    }

    /**
     * 最长回文子串(Longest Palindromic Substring, LPS)
     * 动态规划O(n^2)
     * 状态转移: 一个回文串去掉首尾还是回文串
     * 
     * @param s
     * @return
     */
    public static String LPS(String s) {
        int l = s.length();
        if (l < 2)
            return s;

        int[][] dp = new int[l][l];
        int begin = 0, max = 1;
        // 没有用到
        // for (int i = 0; i < l; i++) {
        // dp[i][i] = 1;
        // }
        for (int len = 2; len <= l; len++) {
            for (int i = 0; i + len - 1 < l; i++) {
                int j = i + len - 1;
                if (s.charAt(i) == s.charAt(j)) {
                    // (j-1)-(i+1)+1 < 2
                    if (j - i < 3) {// asa aa a 这种情况
                        dp[i][j] = 1;
                    } else {
                        dp[i][j] = dp[i + 1][j - 1];// 左下角先填写
                    }
                }
                if (dp[i][j] == 1 && len > max) {
                    max = len;
                    begin = i;
                }
            }
        }
        return s.substring(begin, begin + max);
    }

    /**
     * 动态规划
     * O(n^2)
     * 
     * @param s
     * @return
     */
    public static String longestPalindrome(String s) {
        int len = s.length();
        if (len < 2) {
            return s;
        }

        int maxLen = 1;
        int begin = 0;
        // dp[i][j] 表示 s[i..j] 是否是回文串
        boolean[][] dp = new boolean[len][len];
        // 初始化：所有长度为 1 的子串都是回文串
        for (int i = 0; i < len; i++) {
            dp[i][i] = true;
        }

        char[] charArray = s.toCharArray();

        // 从左到右 竖着填写因为要先填 dp[i + 1][j - 1]
        for (int j = 1; j < len; j++) {// 列
            for (int i = 0; i < j - 1; i++) {// 行
                if (charArray[i] == charArray[j]) {
                    // (j-1)-(i+1)+1 < 2
                    if (j - i < 3) {
                        dp[i][j] = true;
                    } else {
                        dp[i][j] = dp[i + 1][j - 1];// 左下角先填写
                    }
                }

                // 只要 dp[i][L] == true 成立，就表示子串 s[i..L] 是回文，此时记录回文长度和起始位置
                if (dp[i][j] && j - i + 1 > maxLen) {
                    maxLen = j - i + 1;
                    begin = i;
                }
            }
        }
        return s.substring(begin, begin + maxLen);
    }

    /**
     * LPS 中心扩展法
     * O(n^2)
     * 
     * @param s
     * @return
     */
    public static String LPSExpand(String s) {
        int l = s.length();
        if (l < 2)
            return s;
        int begin = 0, len = 1, max = 0;
        for (int i = 0; i < l - 1; i++) {
            int oddLen = pxpand(s, i, i);
            int evenLen = pxpand(s, i, i + 1);
            max = Math.max(oddLen, evenLen);
            if (len < max) {
                len = max;
                // 画图
                begin = i - (max - 1) / 2;
            }
        }
        return s.substring(begin, begin + len);
    }

    /**
     * 
     * @param s
     * @param i 包括
     * @param j 包括
     * @return 以i,j为中心的回文子串的长度
     */
    private static int pxpand(String s, int i, int j) {
        while (i >= 0 && j < s.length()) {
            if (s.charAt(i) != s.charAt(j))
                break;
            i--;
            j++;
        }
        // j-i+1 -2
        return j - i - 1;
    }

    /**
     * 
     * @param s
     * @return
     */
    public static String LPSManacher(String s) {
        // 为统一奇, 偶数回文串的表示, 在s中插入s没有的字符,如'#'
        StringBuilder t = new StringBuilder("#");
        for (int i = 0, l = s.length(); i < l; i++) {
            t.append(s.charAt(i));
            t.append('#');
        }
        String str = t.toString();
        char[] ca = str.toCharArray();
        int maxLen = 1, begin = 0;
        int l = str.length();
        int[] p = new int[l];// 以i为中心的回文半径(奇数长度时不包括中心)
        int center = 0;// 与maxRight对应的回文中心的下标
        int maxRight = 0;// 当前使用中心扩散法能够走到的最右下标

        // i>=maxRight
        // i<maxRight
        // 1. p[mirror]<maxRight-i -> p[i]=p[mirror]
        // 2. p[mirror]==maxRight-i -> p[i]>=maxRight-i, 需从maxRighr开始扩散
        // 2. p[mirror]>maxRight-i -> p[i]=maxRight-i
        // 综上: p[i]min(p[mirror],maxRight-i),然后尝试中心扩散
        for (int i = 0; i < l; i++) {
            if (i < maxRight) {
                int mirror = 2 * center - i;// i关于center的对称点
                p[i] = Math.min(maxRight - i, p[mirror]);
            }
            // 尝试用中心扩散法更新p[i]的值
            int left = i - (1 + p[i]);
            int right = i + (1 + p[i]);
            while (left >= 0 && right < l && ca[left] == ca[right]) {
                p[i]++;
                left--;
                right++;
            }
            // 更新maxRight, 他是遍历过的i的 i +p[i] 的最大者
            if (i + p[i] > maxRight) {
                maxRight = i + p[i];
                center = i;// 更新center
            }
            // 记录
            if (p[i] > maxLen) {
                maxLen = p[i];
                begin = (i - maxLen) / 2;
            }
        }

        return s.substring(begin, begin + maxLen);
    }

}
