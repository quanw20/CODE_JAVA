package algorithms.dp;

/**
 * 给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。
 * 
 * 你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。
 * 
 * 返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0 。
 * 
 * 
 */
public class _121买卖股票的最佳时机 {
    public int maxProfit(int[] prices) {
        int x = 0;// 买入
        int[] dp = new int[prices.length + 1];
        dp[0] = -prices[0];
        for (int i = 0; i < prices.length; ++i) {
            dp[i + 1] = Math.max(-prices[x] + prices[i], dp[i]);
            if (prices[i] < prices[x])
                x = i;
        }
        return dp[prices.length];
    }
}
