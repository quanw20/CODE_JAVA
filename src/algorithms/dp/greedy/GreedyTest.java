package algorithms.dp.greedy;

import org.junit.jupiter.api.Test;

import algorithms.dp.greedy.Greedy.Inteval;

import static algorithms.dp.greedy.Greedy.*;

import java.util.Arrays;
import java.util.List;

public class GreedyTest {
    @Test
    public void testCoins() {
        int n = 176;
        int[] coins = { 1, 5, 10, 20, 50, 100 };
        int[] coins2 = coins(n, coins);
        System.out.println(Arrays.toString(coins2));
    }

    @Test
    public void testAcrossRiver() {
        int acrossRiver = acrossRiver(4, new int[] { 1, 2, 5, 10 });
        System.out.println(acrossRiver);
    }

    @Test
    public void testSegement() {
        int[] s = { 2, 1, 1, 6, 5, 8 };
        int[] t = { 5, 3, 4, 7, 7, 10 };
        List<Inteval> maxJobs = maxJobs(s.length, s, t);
        System.out.println(maxJobs);

        int[][] intervals = new int[s.length][2];
        for (int i = 0; i < s.length; i++) {
            intervals[i][0] = s[i];
            intervals[i][1] = t[i];

        }
    }

    // @Test
    // public void test() {
    // int removeCoveredIntervals = new Solution()
    // .videoStitching(new int[][] { { 0, 1 }, { 6, 8 }, { 0, 2 }, { 5, 6 }, { 0, 4
    // }, { 0, 3 }, { 6, 7 },
    // { 1, 3 }, { 1, 4 }, { 2, 5 }, { 2, 6 }, { 3, 4 }, { 4, 5 }, { 5, 7 }, { 7, 9
    // }
    // }, 9);
    // System.out.println(removeCoveredIntervals);
    // }
    @Test
    public void testBackpack() {
        int[][] objs = { { 3, 1 }, { 24, 2 }, { 10, 2 }, { 6, 1 }, { 15, 6 } };
        int[] backpack = backpack(objs, 10);
        System.out.println(backpack[0] + " " + backpack[1]);
    }

}
