package algorithms.dp.greedy;

import java.util.*;

/**
 * 贪心算法: 由上一步的最优解推导下一步的最优解,
 * 而上一步之前的(历史)最优解则不作保留
 * 
 * 最优子结构: 如果问题的一个最优解中包含了子问题的最优解,
 * 则该问题具有最优子结构
 * 
 * 对比dfs,不是进行各种可选分支的试探,而是当下就可用的某种策略选择,
 * 无需考虑未来,只要一直这么选下去,就能得出最优解
 * 
 */
public class Greedy {
    /**
     * 凑硬币
     * 
     * @param n     要凑的
     * @param coins 面值
     * @param cnts  个数
     * @return 最少硬币数
     */
    public static int coins(int n, int[] coins, int[] cnts) {
        class Solve {
            static int[] coins;
            static int[] cnts;

            static int coins(int n, int cur) {
                if (n <= 0)
                    return 0;
                if (cur == 0)
                    return n;
                int val = coins[cur];
                int x = n / val;
                int cnt = cnts[cur];
                int t = x <= cnt ? x : cnt;
                return t + coins(n - t * val, cur - 1);
            }
        }
        Solve.cnts = cnts;
        Solve.coins = coins;

        return Solve.coins(n, Solve.cnts.length - 1);
    }

    /**
     * 用coins凑出n
     * 
     * @param n     要凑的钱
     * @param coins 面值
     * @return 每种钞票使用的张数
     */
    public static int[] coins(int n, int[] coins) {
        int[] cnt = new int[coins.length];
        for (int i = coins.length - 1; i >= 0; i--) {
            int face = coins[i];
            if (face <= n) {
                int c = n / face;
                n = n - c * face;
                cnt[i] = c;
            }
        }
        return cnt;
    }

    /**
     * 快速渡河问题(猜+证明)
     * 
     * 输入:
     * 4
     * 1 2 5 10
     * 输出: 17
     * 
     * 1 2先过去,1回来,10 5过去,2回来,1 2过去
     * 
     * 1 2过去,1回来,1 5过去,1回来,1 10过去
     * 
     * @param n     人数
     * @param speed 每个人划船的速度(已排序)
     * @return
     */
    public static int acrossRiver(int n, int[] speed) {
        int remain = n;
        int ans = 0;
        while (remain > 0) {
            if (remain == 1) {
                ans += speed[0];
                break;
            } else if (remain == 2) {
                ans += speed[1];
                break;
            } else if (remain == 3) {
                ans += speed[0] + speed[1] + speed[2];
                break;
            } else {
                int s = speed[1] + speed[0] + speed[remain - 1] + speed[1];
                int t = speed[remain - 1] + speed[remain - 2] + 2 * speed[0];
                ans += Math.min(s, t);
                remain -= 2;
            }
        }
        return ans;
    }

    /**
     * 区间调度问题
     * 
     * 有n项工作在si时开始,在ti时结束
     * 对于每项工作可以选择参与与否,但参与工作的时间段不能重复,要求尽可能多的参与工作
     * 
     * 输入:
     * 5
     * 1 2 4 6 8
     * 3 5 7 9 10
     * 输出:
     * 3
     * 
     * @param n 工作个数
     * @param s 开始时间
     * @param t 结束时间
     * @return 最多无重叠的工作个数
     */
    public static List<Inteval> maxJobs(int n, int[] s, int[] t) {

        Inteval[] jobs = new Inteval[n];
        for (int i = 0; i < n; i++) {
            jobs[i] = new Inteval(s[i], t[i]);
        }
        Arrays.sort(jobs);
        List<Inteval> ls = new ArrayList<>();
        System.out.println(Arrays.deepToString(jobs));
        ls.add(jobs[0]);
        int x = jobs[0].t;
        for (Inteval j : jobs) {
            if (j.s > x) {
                x = j.t;
                ls.add(j);
            }
        }
        return ls;
    }

    /**
     * 区间调度
     * 
     * @param intvs
     * @return
     */
    public int intervalSchedule(int[][] intvs) {
        if (intvs.length == 0)
            return 0;
        // 按 end 升序排序
        Arrays.sort(intvs, (a, b) -> a[1] - b[1]);// 整肃减负数可能会溢出
        int count = 1;// 至少有一个区间不相交
        int end = intvs[0][1];// 排序后，第一个区间就是 x
        for (int[] interval : intvs) {
            int start = interval[0];
            if (start >= end) {// 找到下一个选择的区间了
                count++;
                end = interval[1];
            }
        }
        return count;
    }

    /**
     * 区间调度
     */
    static class Inteval implements Comparable<Inteval> {
        public int s, t;

        Inteval(int si, int ti) {
            s = si;
            t = ti;
        }

        @Override
        public int compareTo(Inteval o) {// 结束时间早的,开始时间晚的
            return this.t == o.t ? this.s - o.s : this.t - o.t;
        }

        @Override
        public String toString() {
            return "(" + s + ", " + t + ")";
        }

    }

    /**
     * 移除覆盖的区间
     * 
     * 区间问题肯定按照区间的起点或者终点进行排序,排序之后更容易找到相邻区间之间的联系
     * 
     * @param intervals 区间
     * @return 剩余的区间数
     */
    public static int removeCoveredIntervals(int[][] intervals) {
        // 按照起点升序排列，起点相同时降序排列
        Arrays.sort(intervals, (a, b) -> {
            if (a[0] > b[0])
                return 1;
            else if (a[0] < b[0])
                return -1;
            else {
                if (a[1] < b[1])
                    return 1;
                else if (a[1] > b[1])
                    return -1;
                else
                    return 0;
            }
        });
        // 计数
        int cnt = 0;
        int left = intervals[0][0], right = intervals[0][1];
        for (int i = 1; i < intervals.length; i++) {
            int[] intv = intervals[i];
            // 1.覆盖
            if (intv[0] >= left && intv[1] <= right) {
                cnt++;
            }
            // 2.相交
            if (intv[0] <= right && intv[1] >= right) {
                right = intv[1];
            }
            // 3.相离
            if (intv[0] >= right) {
                left = intv[0];
                right = intv[1];
            }

        }
        return intervals.length - cnt;
    }

    /**
     * 区间重叠
     * 
     * @param clips
     * @param time
     * @return
     */
    public int videoStitching(int[][] clips, int time) {
        // 按照起点升序排列，起点相同时降序排列
        Arrays.sort(clips, (a, b) -> {
            if (a[0] > b[0])
                return 1;
            else if (a[0] < b[0])
                return -1;
            else {
                if (a[1] < b[1])
                    return 1;
                else if (a[1] > b[1])
                    return -1;
                else
                    return 0;
            }
        });

        // 记录选择的短视频个数
        int res = 0;

        int curEnd = 0, nextEnd = 0;
        int i = 0, n = clips.length;
        while (i < n && clips[i][0] <= curEnd) {
            // 在第 res 个视频的区间内贪心选择下一个视频
            while (i < n && clips[i][0] <= curEnd) {
                nextEnd = Math.max(nextEnd, clips[i][1]);
                i++;
            }
            // 找到下一个视频，更新 curEnd
            res++;
            curEnd = nextEnd;
            if (curEnd >= time) {
                // 已经可以拼出区间 [0, T]
                return res;
            }
        }
        // 无法连续拼出区间 [0, T]
        return -1;
    }

    /**
     * 部分背包
     * 
     * @param objs
     * @param volume
     * @return
     */
    public static int[] backpack(int[][] objs, int volume) {
        int cnt = 0;
        int price = 0;
        Obj[] os = new Obj[objs.length];
        for (int i = 0; i < objs.length; i++) {
            os[i] = new Obj(objs[i][0], objs[i][1]);
        }
        Arrays.sort(os);
        for (int i = 0; i < os.length; i++) {
            if (os[i].weight <= volume) {
                cnt++;
                price += os[i].price;
                volume -= os[i].weight;
            } else {
                if (volume > 0) {
                    price += os[i].singlePrice * volume;
                    cnt++;
                }
            }
        }

        return new int[] { cnt, price };
    }

    static class Obj implements Comparable<Obj> {
        int price;
        int weight;
        int singlePrice;

        public Obj(int p, int w) {
            price = p;
            weight = w;
            singlePrice = price / weight;
        }

        @Override
        public int compareTo(Obj o) {
            return o.singlePrice - singlePrice;
        }
    }

    /**
     * 用最少的船渡河
     * 每艘船最大重量为volume,最多可载两人
     * 
     * 重量最小的人的重量最大的人一起
     * 
     * @param weight 每个人的重量
     * @param volume 船的容量
     * @return
     */
    public static int minBoats(int[] weight, int volume) {
        int boat = 0;
        Arrays.sort(weight);
        int n = weight.length;
        int p1 = 0, p2 = n - 1;
        while (n > 0) {
            if (weight[p1] + weight[p2] > volume) {
                boat++;
                p2--;
                n--;
            } else {
                boat++;
                p1++;
                p2--;
                n -= 2;
            }
        }
        return boat;
    }
}
