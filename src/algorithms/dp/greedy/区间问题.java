package algorithms.dp.greedy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

public class 区间问题 {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("D:\\workspaceFolder\\CODE_JAVA\\src\\algorithms\\dp\\greedy\\区间问题.txt"),
                StandardCharsets.UTF_8);
        int n = in.nextInt();
        int[][] st = new int[n][2];
        for (int i = 0; i < n; ++i) {
            st[i][0] = in.nextInt();
        }
        for (int i = 0; i < n; ++i) {
            st[i][1] = in.nextInt();
        }
        // 按结束时间排
        Arrays.sort(st, (x, y) -> x[1] - y[1]);
        int start = 0;
        int cnt = 0;
        for (int[] is : st) {
            if (is[0] > start) {
                ++cnt;
                start = is[1];
            }
        }
        System.out.println(cnt);
    }
}
