package algorithms.dp;

/**
 * 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
 * 
 * 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
 */
public class _70爬楼梯 {
    int[] a = new int[46];
    {
        a[1] = 1;
        a[2] = 2;
    }

    // 记忆型递归
    public int climbStairs(int n) {
        if (a[n] != 0)
            return a[n];
        a[n - 1] = climbStairs(n - 1);
        a[n - 2] = climbStairs(n - 2);
        return a[n - 1] + a[n - 2];
    }
}
