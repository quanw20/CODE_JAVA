package algorithms.dp;

/**
 * dp+位运算
 */
public class _338比特位计数 {
    /**
     * 逐个计数
     * 
     * @param n
     * @return
     */
    public int[] countBits1(int n) {
        int[] bits = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            bits[i] = countOnes(i);
        }
        return bits;
    }

    /**
     * 计算x的二进制中1的个数
     * 
     * @param x
     * @return
     */
    public int countOnes(int x) {
        int ones = 0;
        while (x > 0) {
            x &= (x - 1);// 该运算将 x 的二进制表示的最后一个 1 变成 0
            ones++;
        }
        return ones;
    }

    /**
     * 最高位有效
     * x:待求1的数
     * y:最高位与x相同,其他位为0
     * z:最高位为0,其他与x相同
     * bits[x]=bits[z]+1;
     * 
     * @param n
     * @return
     */
    public int[] countBits2(int n) {
        int[] bits = new int[n + 1];
        int highBit = 0;
        for (int i = 1; i <= n; i++) {
            if ((i & (i - 1)) == 0) { // i是2的整数次幂
                highBit = i;
            }
            bits[i] = bits[i - highBit] + 1;
        }
        return bits;
    }

    /**
     * 最低位有效
     * 
     * x:待求1的数
     * y:x去除最低位
     * x(奇数)=y+1
     * x(偶数)=y
     * 
     * @param n
     * @return
     */
    public int[] countBits3(int n) {
        int[] bits = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            bits[i] = bits[i >> 1] + (i & 1);
        }
        return bits;
    }

    /**
     * y:将最低不为0的哪一位变成0 -> 减小了=计算过的
     * bits[x]=bits[y]+1
     * 
     * @param n
     * @return
     */
    public int[] countBits4(int n) {
        int[] bits = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            bits[i] = bits[i & (i - 1)] + 1;
        }
        return bits;
    }

}
