package algorithms.dp.path;

import java.util.List;

/**
 * 给你一个正方形字符数组 board ，你从数组最右下方的字符 'S' 出发。
 * 
 * 你的目标是到达数组最左上角的字符 'E' ，数组剩余的部分为数字字符 1, 2, ..., 9 或者障碍
 * 'X'。在每一步移动中，你可以向上、向左或者左上方移动，可以移动的前提是到达的格子没有障碍。
 * 
 * 一条路径的 「得分」 定义为：路径上所有数字的和。
 * 
 * 请你返回一个列表，包含两个整数：第一个整数是 「得分」 的最大值，第二个整数是得到最大得分的方案数，请把结果对 10^9 + 7 取余。
 * 
 * 如果没有任何路径可以到达终点，请返回 [0, 0] 。
 * 
 */
public class _1301最大得分的路径数目 {
    int N;
    int[][] f;// (i,j)[0] 最大值,(i,j)[1] 方案数

    public int[] pathsWithMaxScore(List<String> board) {
        N = board.size();
        f = new int[N * N][2];
        for (int i = N - 1; i >= 0; --i) {
            for (int j = N - 1; j >= 0; --j) {
                if (board.get(i).charAt(j) == 'S')
                    continue;

            }
        }
        return f[0];
    }

    int to(int i, int j) {
        return i * N + j;
    }

    int[] parse(int i) {
        return new int[] { i / N, i % N };
    }
}
