package algorithms.dp.path;

/**
 * _576出界的路径数
 */
public class _576出界的路径数 {
    int mod = 1000000007, M, N, S;
    int[][] f;
    int[][] dirs = { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };

    // index = x * n + y;
    // (x, y) = (index / n, index % n);
    public int findPaths(int m, int n, int maxMove, int startRow, int startColumn) {
        f = new int[n * m][maxMove + 1];
        M = m;
        N = n;
        S = maxMove;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0)
                    add(i, j);
                if (i == m - 1)
                    add(i, j);
                if (j == 0)
                    add(i, j);
                if (j == n - 1)
                    add(i, j);
            }
        }

        // f[(x,y)][step] = f[(x-1,y)][step-1] + f[(x,y-1)][step-1]
        // + f[(x+1,y)][step-1] + f[(x,y+1)][step-1]
        for (int cur = 1; cur <= maxMove; ++cur) { // 更新 f[i][j]依赖于 f[x][j-1]
            for (int i = 0, l = m * n; i < l; ++i) {
                int x = parseIdx(i)[0], y = parseIdx(i)[1];
                for (int[] d : dirs) {
                    int nx = x + d[0], ny = y + d[1];
                    if (nx >= 0 && nx < m && ny >= 0 && ny < n) {
                        f[i][cur] += f[getIndex(nx, ny)][cur - 1];
                        f[i][cur] %= mod;
                    }
                }
            }
        }
        return f[startRow * n + startColumn][maxMove];
    }

    void add(int i, int j) {
        int index = getIndex(i, j);
        for (int k = 1; k <= S; ++k) {
            ++f[index][k];
        }
    }

    // 将 (x, y) 转换为 index
    int getIndex(int x, int y) {
        return x * N + y;
    }

    // 将 index 解析回 (x, y)
    int[] parseIdx(int idx) {
        return new int[] { idx / N, idx % N };
    }

    public static void main(String[] args) {
        _576出界的路径数 a = new _576出界的路径数();
        int m = 2, n = 2, maxMove = 2, startRow = 0, startColumn = 0;
        int fp = a.findPaths(m, n, maxMove, startRow, startColumn);
        System.out.println(fp);
    }
}