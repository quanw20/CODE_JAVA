package algorithms.dp.path;

import java.util.Arrays;

/**
 * 总结
 * 事实上，对于两种 动态规划通用解法 的选择，我们应该遵循以下原则进行选择：
 * 
 * 对于一些我们熟悉的题目，或是维度不多的题目，应当优先选择经验解法。
 * 对于一些我们没接触过的题目，应当使用技巧解法。
 * 关于「经验解法」有以下注意点：
 * 
 * 在「经验解法」中，猜测「状态定义」时应当结合最后一步进行猜测。
 * 当有了合理的「状态定义」之后，通常「状态方程」总是呼之欲出的。
 * 因此如果「状态方程」很难推导出来，或者推导出来的「转移方程」无法满足转移要求（【不重不漏】或者【不漏】）。很大程度上是我们的「状态定义」猜错了，需要重新猜测。
 * 
 * 关于「技巧解法」有以下注意点：
 * 通常我们需要有一个「记忆化搜索」的解决方案，然后在此解决方案的基础上转成「动态规划」。
 * 事实上，「记忆化搜索」本身与「动态规划」并无效率区别，因此如果我们真的有一个「记忆化搜索」方案的话，其实并没有“翻译”成「动态规划」的必要。
 * 但「技巧解法」强调的是，我们只需要有一个「记忆化搜索」的 DFS 函数签名即可，而不用真的去实现一个「记忆化搜索」。
 * 所谓的“翻译”过程也是帮助我们从 DFS
 * 函数签名中得到一个可靠的「状态定义」而已，当有了「状态定义」之后，我们仍然要和「经验解法」一样，去分析「状态方程」。
 * 
 * 
 */
public class _1575统计所有可行路径 {
    int[][] dp; // dp[i][fuel] 代表从位置 i 出发，当前剩余的油量为 fuel 的前提下，到达目标位置的「路径数量」
    int mod = 1000000007;

    // 缓存器：用于记录「特定状态」下的结果
    // cache[i][fuel] 代表从位置 i 出发，当前剩余的油量为 fuel 的前提下，到达目标位置的「路径数量」
    int[][] cache;

    /**
     * dfs
     * 
     * 设计好递归函数的「入参」和「出参」
     * 设置好递归函数的出口（Base Case）
     * 编写「最小单元」处理逻辑
     * 
     * 有效情况 如果我们当前所在的位置 i 就是目的地 finish 的话，那就算成是一条有效路径，我们可以对路径数量进行+1
     * 
     * 当油量消耗完，所在位置又不在 finish，那么就算走到头了，算是一次「无效情况」，可以终止递归
     * 油量不为 0，但无法再移动到任何位置，也算是一次「无效情况」，可以终止递归
     * 
     * @param locations
     * @param start
     * @param finish
     * @param fuel
     * @return
     */
    public int countRoutes(int[] ls, int start, int end, int fuel) {
        int n = ls.length;
        // 初始化缓存器
        // 之所以要初始化为 -1
        // 是为了区分「某个状态下路径数量为 0」和「某个状态尚未没计算过」两种情况
        cache = new int[n][fuel + 1];
        for (int i = 0; i < n; i++) {
            Arrays.fill(cache[i], -1);
        }
        return dfs(ls, start, end, fuel);
    }

    /**
     * 计算「路径数量」
     * 
     * @param ls   入参 locations
     * @param u    当前所在位置（ls 的下标）
     * @param end  目标哦位置（ls 的下标）
     * @param fuel 剩余油量
     * @return 在位置 u 出发，油量为 fuel 的前提下，到达 end 的「路径数量」
     */
    int dfs(int[] ls, int u, int end, int fuel) {// 不变参数: ls,end 变参:u, fuel 因此可以定义二维数组f[][] 来表示两个变参
        // 如果缓存中已经有答案，直接返回
        if (cache[u][fuel] != -1) {
            return cache[u][fuel];
        }
        // 如果一步到达不了，说明从位置 u 不能到达 end 位置
        // 将结果 0 写入缓存器并返回
        int need = Math.abs(ls[u] - ls[end]);
        if (need > fuel) {
            cache[u][fuel] = 0;
            return 0;
        }
        int n = ls.length;
        // 计算油量为 fuel，从位置 u 到 end 的路径数量
        // 由于每个点都可以经过多次，如果 u = end，那么本身就算一条路径
        int sum = u == end ? 1 : 0;
        for (int i = 0; i < n; i++) {// 枚举所有的位置，看从当前位置 u 出发，可以到达的位置有哪些
            if (i != u) {
                need = Math.abs(ls[i] - ls[u]);
                if (fuel >= need) {
                    sum += dfs(ls, i, end, fuel - need);
                    sum %= mod;
                }
            }
        }
        cache[u][fuel] = sum;
        return sum;
    }

    public int countRoutesDp(int[] ls, int start, int end, int fuel) {
        int n = ls.length;
        // 由dfs的参数的到f的定义
        int[][] f = new int[n][fuel + 1];// f[i][j] 代表从位置 i 出发，当前剩余油量为 j 的前提下，到达目的地的路径数量
        for (int i = 0; i <= fuel; ++i)
            f[end][i] = 1;
        // 由dfs递归部分得到状态转移方程
        for (int cur = 0; cur <= fuel; ++cur)
            for (int i = 0; i < n; ++i)
                for (int k = 0; k < n; ++k)
                    if (i != k) {
                        int need = Math.abs(ls[i] - ls[k]);
                        if (cur >= need) {
                            f[i][cur] += f[k][cur - need];
                            f[i][cur] %= mod;
                        }
                    }
        return f[start][fuel];
    }

    public static void main(String[] args) {
        int[] locations = { 2, 3, 6, 8, 4 };
        int start = 1, finish = 3, fuel = 5;
        _1575统计所有可行路径 a = new _1575统计所有可行路径();
        int cr = a.countRoutes(locations, start, finish, fuel);
        System.out.println(cr);
    }
}
