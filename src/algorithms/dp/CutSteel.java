package algorithms.dp;

import java.util.Arrays;


/**
 * 钢条切割
 * 有一段长度为length的钢条
 * 可以切割为n(n>=0,n=0表示不切割)段卖,价格因长度而不同
 * 
 * steels={1,5,8,16,10,17,17,20,24,30}
 * length=10
 * 
 */
public class CutSteel {
    int _r1_dfs(int[] s, int l) {
        if (l == 0)
            return 0;
        int ans = 0;
        for (int i = 1; i <= l; ++i) {
            int ret = s[i - 1] + _r1_dfs(s, l - i);
            ans = Math.max(ret, ans);
        }
        return ans;
    }

    static int[] rec;

    int _r2_record_dfs(int[] s, int l) {
        if (l == 0)
            return 0;
        int ans = 0;
        for (int i = 1; i <= l; ++i) {
            if (rec[i - 1] == -1) {
                rec[i - 1] = s[i - 1] + _r1_dfs(s, l - i);
            } else {
                int ret = s[i - 1] + rec[l - i];
                ans = Math.max(ret, ans);
            }
        }
        rec[l] = ans;
        return ans;
    }

    public static void main(String[] args) {
        int[] s = { 1, 5, 8, 16, 10, 17, 17, 20, 24, 30 };
        int _r1_dfs = new CutSteel()._r1_dfs(s, s.length);
        System.out.println(_r1_dfs);

        rec = new int[s.length + 1];
        Arrays.fill(rec, -1);
        rec[0] = 0;

    }
}

class Solution {

    public int maxProfit(int[] prices) {
        int x = 0;
        int[] dp = new int[prices.length + 1];
        dp[0] = -prices[0];
        for (int i = 0; i < prices.length; ++i) {
            dp[i + 1] = Math.max(-prices[x] + prices[i], dp[i]);
            if (prices[i] < prices[x])
                x = i;
        }
        return dp[prices.length];
    }

    public static void main(String[] args) {
        // List<Integer> g = new Solution().getRow(5);
        // System.out.println(g);
        System.out.println(new Solution().maxProfit(new int[]{7,1,5,3,6,4}));
    }
}