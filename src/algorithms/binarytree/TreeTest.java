package algorithms.binarytree;

class TreeNode {
  int val;
  TreeNode left;
  TreeNode right;
  TreeNode(int val ){
    this.val=val;
  }
}

public class TreeTest {
  public static void main(String[] args) {
      TreeNode root = new TreeNode(1);
      root.left=new TreeNode(2);
      root.right=new TreeNode(3);
      
      preOrder(root);
  }

static  void preOrder(TreeNode root){
    if(root==null)return;
    System.out.println(root.val);
    preOrder(root.left);
    preOrder(root.right); 
  }
}
