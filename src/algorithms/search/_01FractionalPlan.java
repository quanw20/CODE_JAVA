package algorithms.search;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 01分数规划，简单的来说，就是有一些二元组（si，pi），从中选取一些二元组，使得∑si / ∑pi最大（最小）。
 * 
 * 这种题一类通用的解法就是，我们假设x = ∑si / ∑pi的最大（小）值，那么就有x * ∑pi = ∑si ,即∑(si -x*pi)=0。
 * 也就是说，当某一个值x满足上述式子的时候，它就是要求的值。我们可以想到枚举……不过再想想，这个可以二分答案。
 * 
 * https://ac.nowcoder.com/acm/contest/22353/S
 * https://ac.nowcoder.com/acm/problem/14662
 */
public class _01FractionalPlan {
    int[][] a;
    private int n, k;

    void solve() throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            n = in.nextInt();
            k = in.nextInt();// k个手办
            a = new int[n][3];
            for (int j = 0; j < n; ++j) {
                a[j][0] = in.nextInt();
                a[j][1] = in.nextInt();
            }
            int l = 1, r = 100000000;
            long max = Long.MIN_VALUE;
            while (l < r) {
                int mid = l + (r - l >> 1);
                long ret = f(a, mid);
                if (ret < 0) {
                    r = mid - 1;
                } else {
                    max = Math.max(max, ret);
                    l = mid + 1;
                }
            }
            System.out.println(max);
        }
    }

    private long f(int[][] a, int x) {
        for (int i = 0; i < a.length; i++)
            a[i][2] = a[i][1] - x * a[i][0];
        Arrays.parallelSort(a, (c, y) -> {
            return y[2] - c[2] >= 0 ? y[2] - c[2] == 0 ? 0 : 1 : -1;
        });
        long sum = 0l, v = 0l, c = 0l;
        for (int i = 0; i < k; i++) {
            sum += a[i][2];
            v += a[i][1];
            c += a[i][0];
        }
        if (sum == 0)
            return -1l;
        else
            return v / c;
    }

    public static void main(String[] args) throws IOException {
        new _01FractionalPlan().solve();
    }

}
