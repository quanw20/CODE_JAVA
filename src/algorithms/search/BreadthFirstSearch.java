package algorithms.search;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;
import java.util.Map.Entry;

public class BreadthFirstSearch {
    public static final int INF = 10000000;
    static char[][] maze;
    static int[][] d;
    static int M, N, sx, sy, gx, gy;
    static int[] dx = { 1, 0, -1, 0 }, dy = { 0, 1, 0, -1 };

    static int bfs() {
        Queue<Entry<Integer, Integer>> q = new LinkedList<>();
        for (int i = 0; i < N; ++i)
            for (int j = 0; j < M; ++j)
                d[i][j] = INF;
        d[sx][sy] = 0;
        q.add(Map.entry(sx, sy));
        while (!q.isEmpty()) {
            Entry<Integer, Integer> p = q.poll();
            for (int i = 0; i < 4; ++i) {
                int nx = p.getKey() + dx[i], ny = p.getValue() + dy[i];
                if (0 <= nx && nx < N
                        && 0 <= ny && ny < M
                        && maze[nx][ny] != '#'
                        && d[nx][ny] == INF) {
                    q.add(Map.entry(nx, ny));
                    d[nx][ny] = d[p.getKey()][p.getValue()] + 1;
                }
            }
        }
        return d[gx][gy];
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        N = in.nextInt();
        M = in.nextInt();
        in.nextLine();
        maze = new char[N][M];
        d = new int[N][M];
        for (int i = 0; i < N; i++) {
            maze[i] = in.nextLine().toCharArray();
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (maze[i][j] == 'S') {
                    sx = i;
                    sy = j;
                }
                if (maze[i][j] == 'G') {
                    gx = i;
                    gy = j;
                }
            }
        }
        int bfs = bfs();
        System.out.println(bfs);
        in.close();
    }
}
/**
 * 10 10
 * #S######.#
 * ......#..#
 * .#.##.##.#
 * .#........
 * ##.##.####
 * ....#....#
 * ##.#####.#
 * ....#.....
 * .####.###.
 * ....#...G#
 */