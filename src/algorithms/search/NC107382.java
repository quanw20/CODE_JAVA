package algorithms.search;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 二分
 */
public class NC107382 {
    int n;
    int[][] a;
    int[] b;
    int[] c;

    void solve() {
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        a = new int[n][4];
        b = new int[n * n];
        c = new int[n * n];
        for (int j = 0; j < 4; j++)
            for (int i = 0; i < n; i++)
                a[i][j] = in.nextInt();
        int idx = 0;
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j) {
                b[idx] = a[i][0] + a[j][0];
                c[idx] = a[i][2] + a[j][3];
                ++idx;
            }
        // System.out.println(Arrays.toString(b));
        Arrays.sort(c);
        int res = 0;
        for (int i = 0; i < b.length; ++i) {
            res += rightBound(c, -b[i]) - leftBound(c, -b[i]);
        }
        System.out.println(res);

    }

    int f(int[] a, int m) {
        return a[m];
    }

    int leftBound(int[] arr, int target) {
        int lo = 0, hi = arr.length;// 右边界取不到
        while (lo < hi) {
            int mid = lo + (hi - lo >> 1);
            if (f(arr, mid) >= target)
                hi = mid;
            else
                lo = mid + 1;
        }
        return lo;
    }

    int rightBound(int[] arr, int target) {
        int lo = 0, hi = arr.length;
        while (lo < hi) {
            int mid = lo + (hi - lo >> 1);
            if (f(arr, mid) <= target)
                lo = mid + 1;
            else
                hi = mid;
        }
        // 应该返回最后一次的mid
        return lo;// 返回时lo==hi,但hi是取不到的一个值
    }

    public static void main(String[] args) {
        new NC107382().solve();
    }
}
