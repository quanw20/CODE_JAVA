package algorithms.search;

/**
 * 三分 ：单峰/单谷函数求最大/最小值
 */
public class TrisectionSearch {

    // 实数判断相等 f1-f2<1e-8;

    double f(double x) {
        return -x * x;
    }

    double EPS = .000000001;

    double Solve(double l, double r) {// 返回极值点
        double mid, mmid, fmid, fmmid;
        while (l + EPS < r) {
            mid = (l + r) / 2;
            mmid = (mid + r) / 2;
            fmid = f(mid);
            fmmid = f(mmid);
            if (fmid >= fmmid)// 假设求解最大极值.最小值则取小于号
                r = mmid;
            else
                l = mid;
        }
        return l;
    }

    public static void main(String[] args) {
        System.out.println(new TrisectionSearch().Solve(-10, 10));
    }
}
