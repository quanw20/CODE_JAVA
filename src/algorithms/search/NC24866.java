package algorithms.search;

import java.io.IOException;
import java.util.Scanner;

/**
 * 前缀和+二分
 * 链接：https://ac.nowcoder.com/acm/problem/24866
 * 来源：牛客网
 */
public class NC24866 {
    public static void main(String[] args) throws IOException {
        // Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int Q = in.nextInt();
        int[] B = new int[N + 1];
        for (int i = 1; i <= N; ++i)
            B[i] = in.nextInt() + B[i - 1];
        for (int i = 0; i < Q; ++i) {
            int q = in.nextInt();
            System.out.println(ub(B, q) + 1);
        }
    }

    static int ub(int[] nums, int target) {
        int lo = 1, hi = nums.length;
        while (lo < hi) {
            int mid = lo + (hi - lo >> 1);
            if (nums[mid] <= target) {
                lo = mid + 1;
            } else {
                hi = mid;
            }
        }
        // 应该返回最后一次的mid
        return lo - 1;// 返回时lo==hi,但hi是取不到的一个值
    }
}
