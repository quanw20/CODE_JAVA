package algorithms.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GBSearch {
  public static <T> int binarySearch(List<Comparable> arr, Comparable key) {
    int l = 0;
    int r = arr.size() - 1;
    while (l <= r) {
      int mid = l + (r - l) / 2;
      if (arr.get(mid).compareTo(key) == 0) {
        return mid;
      } else if (arr.get(mid).compareTo(key) < 0) {
        l = mid + 1;
      } else {
        r = mid - 1;
      }
      System.out.println(arr.get(mid));
    }
    return -1;
  }

  public static void main(String[] args) {
    List<Comparable> arr = List.of("haha", "dasd", "as", "躺枪", "权威");
    ArrayList<Comparable> ls = new ArrayList<>(arr);
    ls.sort(Comparator.naturalOrder());
    System.out.println(ls);
    System.out.println(binarySearch(ls, "权威"));
  }
}
