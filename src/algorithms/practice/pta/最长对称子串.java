package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class 最长对称子串 {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        String line = in.nextLine();
        char[] ca = line.toCharArray();
        int max = 0;
        for (int i = 1; i < ca.length - 1; ++i) {
            int l = check(ca, i);
            int m = check1(ca, i);
            max = m > max ? m : max;
            max = l > max ? l : max;
        }
        System.out.println(max);
        in.close();
    }
//     Is PATTAP symmetric?

    private static int check1(char[] ca, int i) {
        int l = i, r = i + 1;
        while (l >= 0 && r < ca.length && ca[l] == ca[r]) {
            --l;
            ++r;
        }
        return r - l - 1;
    }

    private static int check(char[] ca, int i) {
        int l = i - 1, r = i + 1;
        while (l >= 0 && r < ca.length && ca[l] == ca[r]) {
            --l;
            ++r;
        }
        return r - l - 1;
    }
}
