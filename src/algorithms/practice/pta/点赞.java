package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;

public class 点赞 {
    public static void main(String[] args) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        while (n-- != 0) {
            int k = in.nextInt();
            while (k-- != 0) {
                int f = in.nextInt();
                map.put(f, map.getOrDefault(f, 0) + 1);
            }
        }
        int maxk = 0, maxv = 0;
        for (Entry<Integer, Integer> e : map.entrySet()) {
            if (e.getValue() >= maxv) {
                maxv = e.getValue();
                if (e.getKey() > maxk) {
                    maxk = e.getKey();
                }
            }
        }
        System.out.println(maxk + " " + maxv);
        in.close();
    }

}
