package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class 输出GPLT {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        char[] a = in.next().toUpperCase().toCharArray();
        Arrays.sort(a);
        int g, l, p, t;
        for (g = 0; g < a.length && a[g] != 'G'; ++g)
            ;
        for (l = g; l < a.length && a[l] != 'L'; ++l)
            ;
        for (p = l; p < a.length && a[p] != 'P'; ++p)
            ;
        for (t = p; t < a.length && a[t] != 'T'; ++t)
            ;
        int c = 1; // 计数 -> 如果等于0就退出适合有多处判断
        while (c != 0) {
            c = 0;
            if (g != -1 && a[g] == 'G') {
                System.out.print('G');
                ++g;
                ++c;
            }
            if (p != -1 && a[p] == 'P') {
                System.out.print('P');
                ++p;
                ++c;
            }
            if (l != -1 && a[l] == 'L') {
                System.out.print('L');
                ++l;
                ++c;
            }
            if (t != -1 && a[t] == 'T') {
                System.out.print('T');
                ++t;
                ++c;
            }
        }
    }
}
