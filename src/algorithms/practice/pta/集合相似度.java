
package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.HashSet;
import java.util.Scanner;

public class 集合相似度 {

    static Scanner in = new Scanner(new BufferedInputStream(System.in));

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        int n = in.nextInt() + 1;
        int m;
        HashSet<Integer>[] hs = new HashSet[n];

        for (int i = 1; i < n; ++i) {
            hs[i] = new HashSet<>();
        }
        for (int i = 1; i < n; ++i) {
            m = in.nextInt();
            while (m-- != 0) {
                hs[i].add(in.nextInt());
            }
        }
        m = in.nextInt();
        while (m-- != 0) {
            int a = in.nextInt();
            int b = in.nextInt();
            HashSet<Integer> inter = (HashSet<Integer>) hs[a].clone();
            inter.retainAll(hs[b]);
            HashSet<Integer> union = (HashSet<Integer>) hs[a].clone();
            union.addAll(hs[b]);
            // System.out.println(inter + " " + union);
            System.out.printf("%.2f%%\n", inter.size() * 1.0 / union.size() * 100);

        }
    }
}
