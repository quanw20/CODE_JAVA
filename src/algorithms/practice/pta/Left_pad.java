package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class Left_pad {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        char c = in.next().toCharArray()[0];
        in.nextLine();
        String s = in.nextLine();
        System.out.println(c);
        if (n >= s.length()) {
            for (int i = 0, l = n - s.length(); i < l; ++i) {
                System.out.print(c);
            }
            System.out.print(s);
        } else {
            System.out.println(s.substring(s.length() - n, s.length()));
        }
        in.close();
    }
}
