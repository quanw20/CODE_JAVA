package algorithms.practice.t2013;

import java.math.BigInteger;

//2^11213-1 的后100位
public class _03梅森数 {
    public static void main(String[] args) {
        BigInteger pow = BigInteger.TWO.pow(11213).subtract(BigInteger.ONE);
        StringBuilder sb = new StringBuilder(pow.toString(10));
        CharSequence s = sb.reverse().subSequence(0, 100);
        System.out.println(new StringBuilder(s).reverse());
    }
}
