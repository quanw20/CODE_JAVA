package algorithms.practice.t2013;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * JDK 时间API
 * 
 * <pre>
 * 时间单位
 * DayOfWeek(1.8)
 *      MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY;
 * 
 * ChronoUnit(1.8)
 * 
 * TimeUnit(1.5)
 * 
 * </pre>
 */
public class _01世纪末的星期 {
    /**
     * _01世纪末的星期
     * 
     * 曾有邪教称1999年12月31日是世界末日。当然该谣言已经不攻自破。
     * 还有人称今后的某个世纪末的12月31日，如果是星期一则会....
     * 有趣的是，任何一个世纪末的年份的12月31日都不可能是星期一!!
     * 于是，“谣言制造商”又修改为星期日......
     * 1999年的12月31日是星期五，请问：未来哪一个离我们最近的一个世纪末年（即xx99年）的12月31日正好是星期天（即星期日）？
     * 
     * 
     * 请回答该年份（只写这个4位整数，不要写12月31等多余信息）
     * 
     * 2299
     * 分析：
     * 时间日期API
     */
    public static void main(String[] args) {
        LocalDate d1 = LocalDate.of(1999, 12, 31);
        while (true) {
            if (d1.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                System.out.println(d1.getYear());
                break;
            }
            d1 = d1.plus(100, ChronoUnit.YEARS);
        }
    }
}