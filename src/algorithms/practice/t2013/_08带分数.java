package algorithms.practice.t2013;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 问题描述
 * 100 可以表示为带分数的形式：100 = 3 + 69258 / 714。
 * 
 * 还可以表示为：100 = 82 + 3546 / 197。
 * 
 * 注意特征：带分数中，数字1~9分别出现且只出现一次（不包含0）。
 * 
 * 类似这样的带分数，100 有 11 种表示法。
 * 
 * 输入格式
 * 从标准输入读入一个正整数N (N<1000*1000)
 * 
 * 输出格式
 * 程序输出该数字用数码1~9不重复不遗漏地组成带分数表示的全部种数。
 * 
 * 注意：不要求输出每个表示，只统计有多少表示法！
 * 
 * 样例输入1
 * 100
 * 样例输出1
 * 11
 * 样例输入2
 * 105
 * 样例输出2
 * 6
 * 
 * 思路:
 * 1~9 全排列 -> 两个下标分三段检查相等 -> 计数
 * 
 */
public class _08带分数 {
    static ArrayList<char[]> al = new ArrayList<>();
    
    static {
        prem("123456789".toCharArray(), 0, 8);
    }

    static void prem(char[] a, int start, int end) {
        if (start == end) {
            al.add(a.clone());
            return;
        }
        for (int i = start; i <= end; i++) {
            swap(a, i, start);
            prem(a, start + 1, end);
            swap(a, i, start);
        }
    }

    private static void swap(char[] a, int i, int start) {
        char temp = a[i];
        a[i] = a[start];
        a[start] = temp;
    }

    static boolean check(char[] a, int n) {
        String s = new String(a);
        for (int i = 1; i <= 7; i++) {
            for (int j = i + 1; j <= 8; j++) {
                int left = Integer.parseInt(s, 0, i, 10);
                if (left > n)
                    continue;
                int up = Integer.parseInt(s, i, j, 10);
                int dn = Integer.parseInt(s, j, a.length, 10);
                if (1.0 * up / dn == n - left) {// 浮点数与整数比较
                    // System.out.println(left + " " + up + " " + dn);
                    return true;
                }
            }
        }
        return false;
    }

    static int cnt = 0;

    public static void main(String[] args) {
        // System.out.println(al.size());
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        al.forEach(e -> {
            if (check(e, n))
                ++cnt;
        });
        System.out.println(cnt);

        in.close();
    }
}
