package algorithms.practice.t2015;

import java.util.Arrays;
import java.util.Scanner;

public class _09垒骰子_dp {
  static boolean[][] cf;
  private static final int MOD = (int) 1e9 + 7;
  private static final int[] op = { 0, 4, 5, 6, 1, 2, 3 };// 对面

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    final int n = in.nextInt();
    int m = in.nextInt();
    cf = new boolean[7][7];// 冲突
    for (int i = 0; i < m; ++i) {
      int x = in.nextInt();
      int y = in.nextInt();
      cf[x][y] = cf[y][x] = true;
    }

    in.close();//
    int[][] a = new int[2][7];
    a[0] = new int[] { 0, 1, 1, 1, 1, 1, 1 };
    for (int k = 1; k < n; ++k) {
      for (int i = 1; i <= 6; ++i) {
        a[1][i] = 0;
        for (int j = 1; j <= 6; ++j) {
          if (!cf[i][op[j]]) {
            a[1][i] = (a[1][i] + 4 * a[0][j]) % MOD;
          }
        }
      }
      a[0] = a[1];
    }
    System.out.println((Arrays.stream(a[1]).sum() % MOD) * 4 % MOD);

  }
}
