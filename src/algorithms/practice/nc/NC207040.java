package algorithms.practice.nc;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 牛客幼儿园的小朋友们围成了一个圆圈准备玩丢手绢的游戏，但是小朋友们太小了，不能围成一个均匀的圆圈，即每个小朋友的间隔可能会不一致。
 * 为了大家能够愉快的玩耍，我们需要知道离得最远的两个小朋友离得有多远（如果太远的话牛老师就要来帮忙调整队形啦！）。
 * 因为是玩丢手绢，所以小朋友只能沿着圆圈外围跑，所以我们定义两个小朋友的距离为沿着圆圈顺时针走或者逆时针走的最近距离。
 * 输入描述:
 * 第一行一个整数N，表示有N个小朋友玩丢手绢的游戏。
 * 接下来的第2到第n行，第i行有一个整数，表示第i-1个小朋友顺时针到第i个小朋友的距离。
 * 最后一行是第N个小朋友顺时针到第一个小朋友的距离。
 * 输出描述:
 * 输出一个整数，为离得最远的两个小朋友的距离。
 */
public class NC207040 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[2 * n];
        int sum = 0;
        for (int i = 0; i < n; ++i) {
            a[i] = in.nextInt();
            sum += a[i];
        }
        int h = sum / 2;
        int res = 0;
        int l = 0, r = 0, len = 0;
        while (l < n) {
            while (len < h)
                len += a[r++ % n];
            if (len == h)
                res = Math.max(res, len);
            else if (len > h)
                res = Math.max(res, sum - len);
            len -= a[l++];
        }
        System.out.println(res);
    }
}
