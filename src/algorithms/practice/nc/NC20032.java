package algorithms.practice.nc;

import java.util.Scanner;

/**
 * NC20032
 */
public class NC20032 {
    static final int N = 5001;
    static final int[][] d = new int[N][N];

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int R = in.nextInt();
        int x, y;
        for (int i = 0; i < n; ++i) {
            x = in.nextInt() + 1;
            y = in.nextInt() + 1;
            d[x][y] = in.nextInt();
        }
        for (int i = 1; i < N; ++i) {
            for (int j = 1; j < N; ++j) {
                d[i][j] += d[i - 1][j] + d[i][j - 1] - d[i - 1][j - 1];
            }
        }
        int ret = 0;
        for (int i = R; i < N; ++i) {
            for (int j = R; j < N; ++j) {
                ret = Math.max(ret, d[i][j] + d[i - R][j - R] - d[i - R][j] - d[i][j - R]);
            }
        }
        System.out.println(ret);
    }
}