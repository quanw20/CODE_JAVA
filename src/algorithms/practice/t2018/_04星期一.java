package algorithms.practice.t2018;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * 1901/1/1 -> 2000/12/31 间有多少个星期一
 * 
 * 5217
 */
public class _04星期一 {
    private static int cnt;

    public static void main(String[] args) {
        LocalDate d = LocalDate.of(1901, 1, 1);
        LocalDate t = LocalDate.of(2000, 12, 31);
        for (LocalDate i = d; !i.equals(t); i = i.plus(1, ChronoUnit.DAYS)) {
            if (i.getDayOfWeek().equals(DayOfWeek.MONDAY)) {
                cnt++;
            }
        }
        System.out.println(cnt);
    }
}
