package algorithms.practice.t2014;

/**
 * 题目描述
 * 话说大诗人李白，一生好饮。幸好他从不开车。
 * 一天，他提着酒壶，从家里出来，酒壶中有酒2斗。他边走边唱：
 * 无事街上走，提壶去打酒。
 * 逢店加一倍*2，遇花喝一斗-1。
 * 这一路上，他一共遇到店5次，遇到花10次，已知最后一次遇到的是花，他正好把酒喝光了。
 * 请你计算李白遇到店和花的次序，可以把遇店记为a，遇花记为b。则：babaabbabbabbbb
 * 就是合理的次序。像这样的答案一共有多少呢？请你计算出所有可能方案的个数（包含题目给出的）。
 * 注意：通过浏览器提交答案。答案是个整数。不要书写任何多余的内容。
 * 
 * 题目分析
 * 深搜到酒==1,最后一次是花
 * 
 */
public class _02李白打酒 {
  static boolean[] arr = new boolean[16];
  static int cnt = 0;

  /**
   * 
   * @param a   店
   * @param b   花
   * @param cur 步数
   */
  static void dfs(int a, int b, int cur) {
    if (cur > 14) {
      // 计算
      int ret = 2;
      for (int i = 1; i <= 14; i++) {
        ret = arr[i] ? ret * 2 : ret - 1;
      }
      if (ret == 1)
        ++cnt;
      return;
    }
    if (a != 0) {
      arr[cur] = true;
      dfs(a - 1, b, cur + 1);
      arr[cur] = false;
    }
    if (b != 0) {
      dfs(a, b - 1, cur + 1);
    }
  }

  static void dfs2(int dian, int hua, int jiu) {
    if (dian == 0 && hua == 0 && jiu == 1) {
      ++cnt;
    }
    if (dian != 0)
      dfs2(dian - 1, hua, jiu * 2);
    if (hua != 0)
      dfs2(dian, hua - 1, jiu - 1);
  }

  public static void main(String[] args) {
    // dfs(5, 9, 1);
    dfs2(5, 9, 2);
    System.out.println(cnt);
  }

}
