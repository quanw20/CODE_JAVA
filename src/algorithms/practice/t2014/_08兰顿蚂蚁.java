package algorithms.practice.t2014;

import java.util.Scanner;

/**
 * 兰顿蚂蚁，是于1986年，由克里斯·兰顿提出来的，属于细胞自动机的一种。
 * 平面上的正方形格子被填上黑色或白色。在其中一格正方形内有一只“蚂蚁”。蚂蚁的头部朝向为：上下左右其中一方。
 * 蚂蚁的移动规则十分简单：若蚂蚁在黑格，右转90度，将该格改为白格，并向前移一格；若蚂蚁在白格，左转90度，将该格改为黑格，并向前移一格。
 * 规则虽然简单，蚂蚁的行为却十分复杂。刚刚开始时留下的路线都会有接近对称，像是会重复，但不论起始状态如何，蚂蚁经过漫长的混乱活动后，会开辟出一条规则的“高速公路”。
 * 蚂蚁的路线是很难事先预测的。
 * 你的任务是根据初始状态，用计算机模拟兰顿蚂蚁在第n步行走后所处的位置。
 * 【数据格式】
 * 
 * 输入数据的第一行是 m n 两个整数（3 < m, n < 100），表示正方形格子的行数和列数。
 * 接下来是 m 行数据。
 * 每行数据为 n 个被空格分开的数字。0 表示白格，1 表示黑格。
 * 
 * 接下来是一行数据：x y s k, 其中x y为整数，表示蚂蚁所在行号和列号（行号从上到下增长，列号从左到右增长，都是从0开始编号）。s
 * 是一个大写字母，表示蚂蚁头的朝向，我们约定：上下左右分别用：UDLR表示。k 表示蚂蚁走的步数。
 * 
 * 输出数据为两个空格分开的整数 p q, 分别表示蚂蚁在k步后，所处格子的行号和列号。
 * 
 * 例如, 输入：
 * 5 6
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 * 0 0 1 0 0 0
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 * 2 3 L 5
 * 程序应该输出：
 * 1 3
 * 
 * 再例如, 输入：
 * 3 3
 * 0 0 0
 * 1 1 1
 * 1 1 1
 * 1 1 U 6
 * 程序应该输出：
 * 0 0
 * 
 * 分析：
 * 模拟，注意方向
 */
public class _08兰顿蚂蚁 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int m, n;
    m = in.nextInt();
    n = in.nextInt();
    int[][] a = new int[m][n];// 0 表示白格，1 表示黑格
    for (int i = 0; i < m; ++i) {
      for (int j = 0; j < n; ++j) {
        a[i][j] = in.nextInt();
      }
    }
    int x = in.nextInt();
    int y = in.nextInt();
    String s = in.next();
    int d = switch (s) {
      case "U" -> 0;
      case "R" -> 1;
      case "D" -> 2;
      case "L" -> 3;
      default -> throw new IllegalArgumentException("Unexpected value: " + s);
    };
    int k = in.nextInt();
    // System.out.println();
    go(a, x, y, d, k);
  }

  private static void go(int[][] a, int x, int y, int d, int k) {
    if (k == 0) {
      System.out.println(x + " " + y);
      return;
    }
    if (a[x][y] == 0) {// 若蚂蚁在0白格，左转90度-1，将该格改为1黑格，并向前移一格
      a[x][y] = 1;
      d = d == 0 ? 3 : d - 1;
    } else if (a[x][y] == 1) {// 蚂蚁在黑格，右转90度+1，将该格改为0白格，并向前移一格；
      a[x][y] = 0;
      d = d == 3 ? 0 : d + 1;
    }
    int[] q = switch (d) {
      case 0 -> new int[] { -1, 0 }; // "U" -> 0;
      case 1 -> new int[] { 0, 1 }; // "R" -> 1;
      case 2 -> new int[] { 1, 0 };// "D" -> 2;
      case 3 -> new int[] { 0, -1 };// "L" -> 3;
      default -> throw new IllegalArgumentException("Unexpected value: " + d);
    };
    go(a, x + q[0], y + q[1], d, k - 1);
  }
}
