package algorithms.practice.t2014;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 * 题目描述
 * 由4个不同的数字，组成的一个乘法算式，它们的乘积仍然由这4个数字组成。
 * 比如：
 * 210 x 6 = 1260
 * 8 x 473 = 3784
 * 27 x 81 = 2187
 * 都符合要求。
 * 如果满足乘法交换律的算式算作同一种情况，那么，包含上边已列出的3种情况，一共有多少种满足要求的算式。
 * 请填写该数字，通过浏览器提交答案，不要填写多余内容（例如：列出所有算式）。
 * 
 * 题目分析
 * 枚举
 * 
 * 
 * 29
 */
public class _03神奇算式 {

  private static int cnt = 0;

  public static void main(String[] args) {
    HashSet<String> set = new HashSet<>();

    for (int i = 1; i <= 9; i++) {
      for (int j = 0; j <= 9; j++) {
        if (j == i)
          continue;
        for (int k = 0; k <= 9; k++) {
          if (k == i && k == j)
            continue;
          for (int r = 0; r <= 9; r++) {
            if (r == i && r == j && r == k)
              continue;
            int s1 = i * (j * 100 + k * 10 + r);
            int s2 = (i * 10 + j) * (k * 10 + r);
            int s3 = r * (i * 100 + j * 10 + k);
            String s = i + "" + j + "" + k + "" + r;
            String s11 = String.valueOf(s1);
            if (!set.contains(s11))
              if (check(s11, s)) {
                ++cnt;
                set.add(s11);
                set.add(s11.substring(1) + s11.charAt(0));
              }
            String s22 = String.valueOf(s2);
            if (!set.contains(s22))
              if (check(s22, s)) {
                ++cnt;
                set.add(s22);
                set.add(s22.substring(2) + s22.substring(0, 2));
              }
          }
        }
      }
    }
    System.out.println(cnt);
  }

  private static boolean check(String t, String s) {
    char[] c1 = t.toCharArray();
    char[] c2 = s.toCharArray();
    Arrays.sort(c1);
    Arrays.sort(c2);
    return Arrays.compare(c1, c2) == 0;
  }

}
