package algorithms.practice.t2014;

import java.math.BigInteger;

/**
 * 题目描述
 * 今有 100 根绳子，当然会有 200 个绳头。
 * 如果任意取绳头两两配对，把所有绳头都打结连接起来。最后会形成若干个绳圈（不考虑是否套在一起）。
 * 我们的问题是：请计算最后将形成多少个绳圈的概率最大？
 * 注意：结果是一个整数，请通过浏览器提交该数字。不要填写多余的内容。
 * 
 * 分析：
 * 
 * n-1根绳子的组合数：c[n-1]
 * n根绳子的组合数：c[n] = c[n-1]种组合 * (n-1)个插入点 * 2种插入方式 + c[n-1]种组合 * 1不加入
 * c[n]=(2n-1)*c[n-1]
 * 
 * p[x绳][y圈] =》 x根绳子围y圈的概率
 * p[1][1]=1
 * p[n-1][1]
 * p[n][1] = p[n-1][1]概率 * c[n-1]组合数 * (n-1)个插入点 * 2种插入方式  / c[n]组合数
 * p[n][1] = p[n-1][1]*(2n-2)/(2n-1)
 * 
 * p[100][1..100]中最大值的下标
 * 
 * ->3
 */
public class _07绳圈 {
  public static void main(String[] args) {
    double[][] p = new double[101][101];// [绳][圈]
    p[1][1] = 1;
    for (int i = 2; i <= 100; ++i) {
      p[i][1] = p[i - 1][1] * (i - i) * 2 / (2 * i - 1);
      for (int j = 2; j <= i; ++j) {
        p[i][j] = (p[i - 1][j] * (i - 1) * 2 + p[i - 1][j - 1]) / (2 * i - 1);
      }
    }
    double max = -1;
    int maxi = -1;
    for (int i = 1; i < 101; ++i) {
      if (p[100][i] > max) {
        max = p[100][i];
        maxi = i;
      }
    }
    System.out.println(maxi);
  }
}
