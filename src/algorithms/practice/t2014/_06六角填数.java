package algorithms.practice.t2014;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * 生成全排列 一个一个检查
 */
public class _06六角填数 {
  static ArrayList<int[]> arr = new ArrayList<>();

  static void perm(int[] a, int start) {
    if (start == a.length) {
      arr.add(a.clone());
      return;
    }

    for (int i = start; i < a.length; ++i) {
      swap(a, i, start);
      perm(a, start + 1);
      swap(a, i, start);
    }
  }

  private static void swap(int[] a, int i, int j) {
    int temp = a[i];
    a[i] = a[j];
    a[j] = temp;

  }

  public static void main(String[] args) {
    int[] a = { 2, 4, 5, 6, 7, 9, 10, 11, 12 };
    perm(a, 0);
    for(int[] x:arr){
      check(x);
    }
    arr.forEach(x -> System.out.println(Arrays.toString(x)));
  }

  private static void check(int[] x) {
  }
}
