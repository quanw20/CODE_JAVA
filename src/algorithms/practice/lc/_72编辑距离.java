package algorithms.practice.lc;

import java.util.Arrays;
import java.util.stream.IntStream;

public class _72编辑距离 {
  public static void main(String[] args) {
    String s1 = "rad";
    String s2 = "apple";
    int[][] a = new int[s2.length()][s2.length()];
    for (int i = 0; i < a.length; i++) {
      Arrays.fill(a[i], -1);
    }
    int dfs = dfsMemo(s1.toCharArray(), s1.length() - 1,
        s2.toCharArray(), s2.length() - 1, a);
    System.out.println(dfs);
    System.out.println(dp(s1.toCharArray(), s2.toCharArray()));
  }

  private static int dfs(char[] ca1, int i, char[] ca2, int j) {
    if (j == -1) {
      return i + 1;
    }
    if (i == -1)
      return j + 1;

    int min = 0;
    if (ca1[i] == ca2[j]) {
      min = dfs(ca1, i - 1, ca2, j - 1);
    } else {
      min = IntStream.of(
          dfs(ca1, i - 1, ca2, j - 1) + 1,
          dfs(ca1, i - 1, ca2, j) + 1,
          dfs(ca1, i, ca2, j - 1) + 1).min().getAsInt();
    }
    return min;
  }

  private static int dfsMemo(char[] ca1, int i, char[] ca2, int j, int[][] memo) {
    if (i == -1)
      return j + 1;
    if (j == -1)
      return i + 1;
    // 查备忘录，避免重叠子问题
    if (memo[i][j] != -1) {
      // System.out.println("get");
      return memo[i][j];
    }
    // 状态转移，结果存入备忘录
    if (ca1[i] == ca2[j]) {
      memo[i][j] = dfsMemo(ca1, i - 1, ca2, j - 1, memo);
    } else {
      memo[i][j] = IntStream.of(
          dfsMemo(ca1, i, ca2, j - 1, memo) + 1,
          dfsMemo(ca1, i - 1, ca2, j, memo) + 1,
          dfsMemo(ca1, i - 1, ca2, j - 1, memo) + 1).min().getAsInt();
    }
    return memo[i][j];
  }

  private static int dp(char[] ca1, char[] ca2) {
    int[][] dp = new int[ca1.length + 1][ca2.length + 1];
    for (int k = 0; k < dp.length; ++k) {
      dp[k][0] = k;
    }
    for (int k = 1; k < dp[0].length; ++k) {
      dp[0][k] = k;
    }
    for (int k = 1; k < dp.length; ++k) {
      for (int l = 1; l < dp[0].length; ++l) {
        if (ca1[k - 1] == ca2[l - 1]) {
          dp[k][l] = dp[k - 1][l - 1];
        } else {
          int min = IntStream.of(
              dp[k - 1][l - 1],
              dp[k - 1][l],
              dp[k][l - 1]).min().getAsInt();
          dp[k][l] = min + 1;
        }

      }
    }

    return dp[dp.length - 1][dp[0].length - 1];
  }

}
