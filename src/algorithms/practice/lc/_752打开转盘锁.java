package algorithms.practice.lc;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class _752打开转盘锁 {
    HashSet<String> vis = new HashSet<>();

    public int openLock(String[] deadends, String target) {
        for (String string : deadends) {
            vis.add(string);
        }
        int step = 0;
        Queue<String> q = new LinkedList<>();
        q.add("0000");
        while (!q.isEmpty()) {
            for (int i = 0, l = q.size(); i < l; i++) {
                String p = q.poll();
                if (vis.contains(p))
                    continue;
                vis.add(p);
                if (p.equals(target))
                    return step;
                for (int j = 0; j < 4; ++j) {
                    q.add(plusOne(p, j));
                    q.add(minusOne(p, j));
                }
            }
            ++step;
        }
        return -1;
    }

    // 将 s[j] 向上拨动一次
    String plusOne(String s, int j) {
        char[] ch = s.toCharArray();
        if (ch[j] == '9')
            ch[j] = '0';
        else
            ch[j] += 1;
        return new String(ch);
    }

    // 将 s[i] 向下拨动一次
    String minusOne(String s, int j) {
        char[] ch = s.toCharArray();
        if (ch[j] == '0')
            ch[j] = '9';
        else
            ch[j] -= 1;
        return new String(ch);
    }

    /**
     * bfs双向扩散
     * 
     * @param start
     * @param target
     * @return
     */
    int bfs(String start, String target) {
        int step = 0;
        HashSet<String> q1 = new HashSet<>();
        HashSet<String> q2 = new HashSet<>();
        q1.add(start);
        q2.add(target);
        while (!q1.isEmpty() && !q2.isEmpty()) {
            HashSet<String> temp = new HashSet<>();
            for (String s : q1) {
                if (vis.contains(s))
                    continue;
                if (q2.contains(s))
                    return step;
                vis.add(s);
                for (String t : q2) {
                    if (s.equals(t))
                        return step;
                    for (int j = 0; j < 4; ++j) {
                        String a = plusOne(s, j);
                        if (!vis.contains(a))
                            temp.add(a);
                        String b = minusOne(s, j);
                        if (!vis.contains(b))
                            temp.add(b);
                    }
                }

            }
            ++step;
            q1 = q2;// 这里交换 q1 q2，下一轮 while 就是扩散 q2
            q2 = temp;
        }
        return -1;
    }

    public static void main(String[] args) {
        String[] deadends = { "8887", "8889", "8878", "8898", "8788", "8988", "7888", "9888" };
        String target = "8888";
        _752打开转盘锁 a = new _752打开转盘锁();
        int openLock = a.openLock(deadends, target);
        System.out.println(openLock);
    }
}
