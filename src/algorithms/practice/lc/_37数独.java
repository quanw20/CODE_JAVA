package algorithms.practice.lc;

import algorithms.Util;

/**
 * _37数独
 */
public class _37数独 {
    final int N = 9;

    // ! 没写完
    public void solveSudokuLoop(char[][] board) {
        int r = 0, c = 0;
        char next = 0;
        while (r < N) {
            if (c == N) {
                ++r;
                c = 0;
                continue;
            }
            if (board[r][c] != '.') {
                ++c;
                continue;
            }
            char k = '1';
            if (next != 0)
                k = next;
            for (; k <= '9'; ++k) {
                if (!check(board, r, c, k))
                    continue;
                board[r][c] = k;
                ++c;
                break;
            }
            if (k == '9'+1) {
                if (c != 0)
                    --c;
                else {
                    --r;
                    c = 8;
                }
                next = (char) (board[r][c] + 1);
                board[r][c] = '.';
            }
        }
    }

    private boolean check(char[][] board, int i, int j, char k) {
        for (int a = 0; a < N; ++a) {
            if (board[i][a] == k || board[a][j] == k)
                return false;
            if (board[i / 3 * 3 + a / 3][j / 3 * 3 + a % 3] == k)
                return false;
        }
        return true;
    }

    public void solveSudoku(char[][] board) {
        bt(board, 0, 0);
    }

    private boolean bt(char[][] board, int i, int j) {
        if (j == N)
            return bt(board, i + 1, 0);
        if (i == N)
            return true;
        if (board[i][j] != '.')
            return bt(board, i, j + 1);
        for (char k = '1'; k <= '9'; k++) {
            if (!check(board, i, j, k))
                continue;
            board[i][j] = k;
            if (bt(board, i, j + 1))
                return true;
            board[i][j] = '.';
        }
        return false;
    }

    public static void main(String[] args) {
        char[][] board = { { '5', '3', '.', '.', '7', '.', '.', '.', '.' },
                { '6', '.', '.', '1', '9', '5', '.', '.', '.' }, { '.', '9', '8', '.', '.', '.', '.', '6', '.' },
                { '8', '.', '.', '.', '6', '.', '.', '.', '3' }, { '4', '.', '.', '8', '.', '3', '.', '.', '1' },
                { '7', '.', '.', '.', '2', '.', '.', '.', '6' }, { '.', '6', '.', '.', '.', '.', '2', '8', '.' },
                { '.', '.', '.', '4', '1', '9', '.', '.', '5' }, { '.', '.', '.', '.', '8', '.', '.', '7', '9' } };
        _37数独 a = new _37数独();
        a.solveSudokuLoop(board);
        Util.print(board);
    }
}