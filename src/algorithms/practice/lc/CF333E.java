package algorithms.practice.lc;

import java.io.BufferedInputStream;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Scanner;

public class CF333E {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        int[] x = new int[n];
        int[] y = new int[n];
        int[][] d = new int[(n - 1) * n / 2][3];
        BitSet[] b = new BitSet[n];
        for (int i = 0; i < n; ++i) {
            b[i] = new BitSet(n);
            x[i] = in.nextInt();
            y[i] = in.nextInt();
        }
        int cnt = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                d[cnt][0] = i;
                d[cnt][1] = j;
                d[cnt][2] = (x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j]);
                ++cnt;
            }
        }
        Arrays.sort(d, (u, v) -> v[2] - u[2]);
        // System.out.println(Arrays.deepToString(d));
        for (int i = 0; i < d.length; i++) {
            if (b[d[i][0]].intersects(b[d[i][1]])) {
                System.out.println(Math.sqrt(d[i][2])/2);
                break;
            }
            b[d[i][0]].set(d[i][1]);
            b[d[i][1]].set(d[i][0]);
        }
        in.close();
    }
}
