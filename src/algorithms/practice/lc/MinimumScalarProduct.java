package algorithms.practice.lc;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class MinimumScalarProduct {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        int n = in.nextInt();
        int[] v1 = new int[n];
        Integer[] v2 = new Integer[n];
        for (int i = 0; i < n; ++i) {
            v1[i] = in.nextInt();
        }
        for (int i = 0; i < n; ++i) {
            v2[i] = in.nextInt();
        }
        Arrays.sort(v1);
        Arrays.sort(v2, Comparator.reverseOrder());
        int res = 0;
        for (int i = 0; i < n; ++i) {
            res += v1[i] * v2[i];
        }
        System.out.println(res);
    }
}
/**
 * 5
 * 1 2 3 4 5
 * 1 0 1 0 1
 */