package algorithms.practice.lc;

import java.util.ArrayList;
import java.util.HashMap;

public class _514FreedomTrail {
  public static void main(String[] args) {
    String ring = "godding";
    String Key = "gd";
    System.out.println(new Solution().findRotateSteps(ring, Key));
  }

  static class Solution {
    HashMap<Character, ArrayList<Integer>> map = new HashMap<>();
    int[][] memo;
    int N;

    int findRotateSteps(String ring, String key) {
      for (int i = 0; i < ring.length(); i++) {
        char c = ring.charAt(i);
        if (!map.containsKey(c)) {
          map.put(c, new ArrayList<>());
        }
        map.get(c).add(i);
      }
      memo = new int[ring.length()][key.length()];
      N = ring.length();
      return dp(key, 0, 0);
    }

    private int dp(String s, int i, int j) {
      if (j == s.length())
        return 0;
      if (memo[i][j] != 0)
        return memo[i][j];
      int res = Integer.MAX_VALUE;
      for (int k : map.get(s.charAt(j))) {
        int d = Math.abs(i - k);
        d = Math.min(d, N - d);
        int sub = dp(s, k, j + 1);
        res = Math.min(res, 1 + d + sub);
      }
      memo[i][j] = res;
      return res;
    }

  }
}
