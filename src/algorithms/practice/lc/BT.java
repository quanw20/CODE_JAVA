package algorithms.practice.lc;

import java.util.LinkedList;
import java.util.List;

public class BT {

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new LinkedList<Integer>();
        return tra(root, res);
    }

    private List<Integer> tra(TreeNode root, List<Integer> res) {
        if (root == null) {
            return res;
        }
        tra(root.left, res);
        res.add(root.val);
        tra(root.right, res);
        return res;
    }

    public int numTrees(int n) {
        int[] rec = new int[n + 1];
        rec[0] = rec[1] = 1;
        dfs(rec, n);
        return rec[n];
    }

    private int dfs(int[] rec, int n) {
        if (rec[n] != 0)
            return rec[n];
        int res = 0;
        for (int i = 1; i <= n; ++i) {
            res += dfs(rec, i - 1) * dfs(rec, n - i);
        }
        rec[n] = res;
        return rec[n];
    }

    public List<TreeNode> generateTrees(int n) {
        List<TreeNode> res = new LinkedList<>();
        for (int i = 1; i <= n; ++i) {
            TreeNode root = new TreeNode(i);
            root.left = dfs(res, 1, i - 1);
            root.right = dfs(res, i + 1, n);
            res.add(root);
        }
        return res;
    }

    private TreeNode dfs(List<TreeNode> res, int lo, int hi) {
        return null;// todo
    }

    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return build(nums, 0, nums.length);
    }

    private TreeNode build(int[] nums, int s, int e) {
        if (s >= e)
            return null;
        int i = maxi(nums, s, e);
        TreeNode root = new TreeNode(nums[i]);
        root.left = build(nums, s, i);
        root.right = build(nums, i + 1, e);
        return root;
    }

    private int maxi(int[] a, int s, int e) {
        int max = Integer.MIN_VALUE, index = 0;
        for (int i = s; i < e; i++) {
            if (a[i] > max) {
                max = a[i];
                index = i;
            }
        }
        return index;
    }

    public static void main(String[] args) {
        BT bt = new BT();
        // for (int i = 1; i < 10; i++) {
        // int numTrees = bt.numTrees(i);
        // System.out.println(i + "\t" + numTrees);
        // }

        List<TreeNode> g = bt.generateTrees(3);
        System.out.println(g);
    }

}
