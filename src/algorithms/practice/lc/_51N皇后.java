package algorithms.practice.lc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * N皇后 - 回溯
 * 
 * 模板
 * 
 * <pre>
 * def backtrack(...):
 *     if 触发条件:
 *         记录结果    
 *         return 
 *     for 选择 in 选择列表:
 *         做选择
 *         backtrack(...)
 *         撤销选择
 * </pre>
 */
public class _51N皇后 {
    List<List<String>> res = new LinkedList<>();
    int N;

    public List<List<String>> solveNQueens(int n) {
        N = n;
        char[] cs = new char[n];
        Arrays.fill(cs, '.');
        ArrayList<String> bored = new ArrayList<>(n);
        for (int i = 0; i < n; ++i) {
            bored.add(new String(cs));
        }
        backtrack(bored, 0);

        return res;
    }

    @SuppressWarnings("unchecked")
    private void backtrack(ArrayList<String> bored, int row) {
        if (row == N) {
            res.add((ArrayList<String>) bored.clone());
            return;
        }
        for (int i = 0; i < N; ++i) {
            if (!check(bored, row, i))// 剪枝
                continue;
            String str = bored.get(row);
            char[] ca = str.toCharArray();
            ca[i] = 'Q';
            bored.set(row, new String(ca));// 放置皇后

            backtrack(bored, row + 1);// 递归
            bored.set(row, str);// 回溯
        }
    }

    private boolean check(ArrayList<String> bored, int row, int col) {
        for (int i = 0; i < N; ++i) {
            if (bored.get(row).charAt(i) == 'Q' || bored.get(i).charAt(col) == 'Q')
                return false;
        }
        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; --i, --j) {
            if (bored.get(i).charAt(j) == 'Q')
                return false;
        }
        for (int i = row - 1, j = col + 1; i >= 0 && j < N; --i, ++j) {
            if (bored.get(i).charAt(j) == 'Q')
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        _51N皇后 a = new _51N皇后();
        List<List<String>> s = a.solveNQueens(8);
        for (List<String> ls : s) {
            ls.forEach(System.out::println);
            System.out.println();
        }
    }

}
