package algorithms.practice.lc;

import java.util.PriorityQueue;

public class _787CheapestFlightsWithinKStops {
  public static void main(String[] args) {
    int n = 5;
    // [fromi, toi, pricei]
    int[][] flights = { { 4, 1, 1 }, { 1, 2, 3 }, { 0, 3, 2 }, { 0, 4, 10 }, { 3, 1, 1 }, { 1, 4, 3 } };
    int src = 2, dst = 1, k = 1;
    System.out.println(new Solution().findCheapestPrice(n, flights, src, dst, k));

  }

  static class Solution {
    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int k) {
      int[][] g = new int[n][n];
      for (int[] i : flights) {
        g[i[0]][i[1]] = i[2];
      }
      int res = Integer.MAX_VALUE;
      PriorityQueue<int[]> pq = new PriorityQueue<>((x, y) -> x[1] - y[1]);
      for (int v = 0; v < n; ++v) {
        if (g[src][v] == 0)
          continue;
        pq.add(new int[] { v, g[src][v] });
        if (v == dst)
          res = Math.min(g[src][v], res);
      }
      for (; k != 0;) {
        int[] p = pq.poll();
        if (p == null)
          continue;
        int u = p[0];
        for (int v = 0; v < n; ++v) {
          if (g[u][v] == 0)
            continue;
          pq.add(new int[] { v, p[1] + g[u][v] });
          if (v == dst)
            res = Math.min(p[1] + g[u][v], res);
        }
        --k;
      }
      return res == Integer.MAX_VALUE ? -1 : res;
    }
  }
}
