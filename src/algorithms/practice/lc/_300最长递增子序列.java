package algorithms.practice.lc;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * _300最长递增子序列
 */
public class _300最长递增子序列 {

  public static void main(String[] args) {
    int[] a = { 1, 4, 3, 4, 2 };
    int lis = dp(a);
    System.out.println(lis);
    lis = bs(a);
    System.out.println(lis);
  }

  private static int bs(int[] a) {
    int[] top = new int[a.length];
    int piles = 0;
    for (int i = 0; i < a.length; ++i) {
      int l = 0, r = piles;
      while (l < r) {// left bound
        int mid = l + (r - l) / 2;
        if (top[mid] > a[i])
          r = mid;
        else
          l = mid + 1;
      }
      if (l == piles)
        ++piles;
      top[l] = a[i];
    }
    return piles;
  }

  private static int dp(int[] a) {
    int[] dp = new int[a.length];
    Arrays.fill(dp, 1);
    for (int i = 1; i < a.length; i++) {
      for (int j = i - 1; j >= 0; j--) {
        if (a[j] >= a[i]) {
          dp[i] = Math.max(dp[i], dp[j] + 1);
        }
      }
    }
    return IntStream.of(dp).max().getAsInt();
  }
}