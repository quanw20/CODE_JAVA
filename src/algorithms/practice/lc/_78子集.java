package algorithms.practice.lc;

import java.util.LinkedList;
import java.util.List;
/**
 * 无重复, 不可重用
 */
public class _78子集 {
    List<List<Integer>> res = new LinkedList<>();

    public List<List<Integer>> subsets(int[] nums) {
        LinkedList<Integer> ls = new LinkedList<>();
        bt(nums, 0, ls);
        return res;
    }

    @SuppressWarnings("unchecked")
    private void bt(int[] nums, int index, LinkedList<Integer> ls) {
        res.add((List<Integer>) ls.clone());
        for (int i = index; i < nums.length; ++i) {
            ls.add(nums[i]);
            bt(nums, i + 1, ls);
            ls.removeLast();
        }
    }

    public static void main(String[] args) {
        _78子集 a = new _78子集();
        List<List<Integer>> subsets = a.subsets(new int[] { 1, 2, 3 });
        System.out.println(subsets);
    }
}
