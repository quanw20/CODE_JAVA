package algorithms.string;

import java.util.Arrays;

public class StringAlgo {
    public static void stringDemo() {

    }

    /**
     * 判断是否有重复字符
     * 
     * @param s
     * @return
     */
    public static boolean isRepeat(String s) {
        // 打表 ascii码 0-127 增强版 0-255
        int[] flag = new int[128]; // 0-127;
        for (char i : s.toCharArray()) {
            flag[(int) i]++;
        }
        for (int i : flag)
            if (i > 1)
                return false;

        return true;
    }

    /**
     * 判断是否有重复字符
     * 
     * 不使用额外空间
     * 
     * @param s
     * @return
     */
    public static boolean isRepeat1(String s) {
        char[] a = s.toCharArray();
        Arrays.sort(a);
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] == a[i + 1])
                return false;
        }
        return true;
    }

    /**
     * 字符串逆序
     * 
     * O(n)
     * 
     * @param s
     * @return
     */
    public static String reverse(String s) {
        char[] a = s.toCharArray();
        int i = 0, j = a.length - 1;
        while (i < j) {
            char temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;
        }
        return new String(a);
    }

    /**
     * 单词逆序
     * 
     * 
     * @param s
     * @return
     */
    public static String reverseWords(String s) {
        String sr = reverse(s);
        String[] split = sr.split("\s");
        StringBuilder sb = new StringBuilder(split.length << 1);
        for (String t : split) {
            sb.append(reverse(t)).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    /**
     * 变形词
     * 判断两字符串是否"相等":
     * 
     * 重新排列后是否相等
     * 
     * "Here you are", "Are you here": false
     * "yere Hou are", "are you Here": true
     * 
     * @param a
     * @param b
     * @return
     */
    public static boolean isEqual(String a, String b) {
        if (a.length() != b.length())
            return false;
        // 记录元素(ascii码)出现次数
        int[] flag = new int[256];
        for (int i = 0; i < a.length(); i++) {
            flag[a.charAt(i)]++;
        }
        for (int i = 0; i < b.length(); i++) {
            flag[b.charAt(i)]--;
        }
        for (int i = 0; i < flag.length; i++) {
            if (flag[i] != 0)
                return false;
        }
        return true;
    }

    /**
     * 字符串压缩
     * aaaaaaaaaaaabcdefg -> a12b1c1d1e1f1g1
     * 
     * @return
     */
    public static String stringZip(String s) {
        class Pair {
            char c;
            int n = 0;
        }
        Pair[] a = new Pair[s.length()];
        
        int idx = -1;
        char[] ca = s.toCharArray();
        char pre = 0;

        for (int i = 0; i < ca.length; i++) {
            if (ca[i] != pre) {
                ++idx;
                a[idx] = new Pair();
                a[idx].c = ca[i];
                a[idx].n++;
                pre = ca[i];
            } else {
                a[idx].n++;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null)
                break;
            sb.append(a[i].c).append(a[i].n);
        }
        if (s.length() < sb.length())
            return s;
        return sb.toString();
    }
}
