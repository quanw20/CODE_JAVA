package algorithms.string;

import java.math.BigInteger;
import java.util.Random;

import static algorithms.string.StringAlgo.*;
import static algorithms.string.StringMatch.*;

import org.junit.jupiter.api.*;


public class StringTest {
    @Test
    public void testDemo() {

    }

    @Test
    public void testRepeat() {
        System.out.println(isRepeat("Tang Quanuwei"));
        System.out.println(isRepeat("Tang Quwei"));
        System.out.println(isRepeat1("Tang Quanuwei"));
        System.out.println(isRepeat1("Tang Quwei"));
    }

    @Test
    public void testReverse() {
        String s = "ok you Are you  唐权威 Thank Hello Quawnei Tang";
        System.out.println(reverse("Tang Quanwei"));
        StringBuilder sb = new StringBuilder("Tang Quanwei");
        System.out.println(sb.reverse().toString());

        System.out.print(reverseWords(s));
    }

    @Test
    public void testEqual() {
        String a = "yere Hou are";
        String b = "Here you are";
        String c = "Are you here";
        System.out.println(isEqual(a, b));
        System.out.println(isEqual(a, c));
    }

    @Test
    public void testZip() {
        String s = stringZip("aaaaaaaaaaaabcdefg");
        System.out.println(s);
    }

    @Test
    public void testSimpleMatch() {
        String a = "asdfghjkl";
        String b = "asdf";
        String c = "sdfg";
        String d = "dfgjkl";

        Assertions.assertEquals(a.indexOf(b), 0);

        Assertions.assertEquals(simpleMatch(a, b), 0);
        Assertions.assertEquals(hashMatch(a, b), 0);

        Assertions.assertEquals(simpleMatch(a, c), 1);
        Assertions.assertEquals(hashMatch(a, c), 1);

        Assertions.assertEquals(simpleMatch(a, d), -1);
        Assertions.assertEquals(hashMatch(a, d), -1);

    }

    @Test
    public void testP() {
        BigInteger prime = BigInteger.probablePrime(31, new Random());
        System.out.println(prime.longValue());

    }

    @Test
    public void testKmp() {
        String str = "ABABABC";
        String pat = "ABA";
        int kmp = kmp(str, pat);
        System.out.println(kmp);

    }
}
