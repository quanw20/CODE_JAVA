package algorithms.string;

/**
 * @Description: D-J-B-hash
 *               E-L-F-hash
 *               A-P-Hash
 *               J-S-Hash
 * @ClassName: MyHash
 * @Author: QUANWEI
 * @Date: 2021/12/12 21:06
 * @Version: 1.0
 */
public class HashFunc {
    /**
     * times 33
     * 
     * @param str
     * @return
     */
    public static long djbHash(String str) {
        byte[] bytes = str.getBytes();
        long hash = 5301;
        int c = 0;
        while (c < bytes.length) {
            hash = ((hash << 5) + hash) + bytes[c];
            c++;
        }
        return hash;
    }

    /**
     * My String Hash Funcation
     * 
     * @param str
     * @return
     */
    public static long strHash(String str) {
        long ret = 0;
        byte[] bytes = str.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            if (b >= 'A' && b <= 'Z') {
                ret = ret * 26 + (b * 26 - 'A');
            } else if (b >= 'a' && b <= 'z') {
                ret = ret * 26 + (b * 26 - 'a');
            }
        }
        return ret;
    }

    public static void main(String[] args) {

        System.out.println(djbHash("tang"));
        System.out.println(strHash("tang"));

    }
}
