package algorithms;

import java.util.Arrays;

class PrefixSumDifference {
    /**
     * 数组前缀和
     * 
     * 作用
     * 快速算出一段和
     * 
     * @param a
     * @return
     */
    static int[] arrayPrefixSum(int[] a) {
        int[] c = a.clone();
        for (int i = 1; i < c.length; i++) {
            c[i] = c[i] + c[i - 1];
        }
        return c;
    }

    /**
     * 矩阵前缀和
     * 
     * @param m
     * @return
     */
    static int[][] matrixPrefixSum(int[][] m) {
        for (int i = 1; i < m.length; i++) {
            m[i][0] = m[i][0] + m[i - 1][0];
        }
        for (int i = 1; i < m[0].length; i++) {
            m[0][i] = m[0][i] + m[0][i - 1];
        }
        for (int i = 1; i < m.length; i++) {
            for (int j = 1; j < m[i].length; j++) {
                m[i][j] = m[i][j] + m[i - 1][j] + m[i][j - 1] - m[i - 1][j - 1];
            }
        }
        return m;
    }

    /**
     * 差分 (前缀和的逆运算)
     * 
     * 区间 每个元素+c
     * 
     * @param a
     * @return
     */
    static int[] arrayDifference(int[] a) {
        int[] c = a.clone();
        for (int i = 1; i < c.length; i++) {
            c[i] = c[i] - c[i - 1];
        }
        return c;
    }

    /**
     * 矩阵差分
     * 
     * @param m
     * @return
     */
    static int[][] matrixdifference(int[][] m) {
        for (int i = 1; i < m.length; i++) {
            m[i][0] = m[i][0] - m[i - 1][0];
        }
        for (int i = 1; i < m[0].length; i++) {
            m[0][i] = m[0][i] - m[0][i - 1];
        }
        for (int i = 1; i < m.length; i++) {
            for (int j = 1; j < m[i].length; j++) {
                m[i][j] = m[i][j] - m[i - 1][j] - m[i][j - 1] + m[i - 1][j - 1];
            }
        }
        return m;
    }

    public static void main(String[] args) {
        int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int[] aps = arrayPrefixSum(a);
        System.out.println(Arrays.toString(aps));
        int[][] m = {
                { 1, 3, 7, 10 },
                { 6, 9, 15, 22 },
                { 12, 18, 29, 45 }
        };
        // matrixPrefixSum(m);
        matrixdifference(m);
        Util.print(m);
    }
}