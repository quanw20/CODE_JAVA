# 算法

## 二分
整数二分:
先找到确定可能的答案区间，就是我要找的那个答案肯定在这个区间里，
然后根据题目的条件，判断中值是否满足题意，
不管满不满足我最后都能丢掉一半的区间，进一步逼近答案，最后得到答案
```cpp
bool check(int mid){
	if （满足题意）
		return true;
	else
		return false;
}
int bsearch(int left, int right){
	while (left < right)	{
		int mid = (left + right) / 2;
		if (check(mid))
			right = mid - 1;
		else
			left = mid;
	}
	return left;
}
```
实数二分:
习惯循环100次来达到题目的精度要求
```cpp
bool check(double mid){
	if (满足题意)
		return true;
	else
		return false;
}
double bsearch(double left, double right){
	for (int i = 0; i < 100; i++)	{
		int mid = (left + right) / 2;
		if (check(mid))
			left = mid;
		else
			right = mid;
	}
	return mid;
}

```

## 位运算

1. 判断奇偶

```java
x & 1 == 1 // 奇数
x & 1 == 0 // 偶数

```

## Union Find 并查集

    1、用 parent 数组记录每个节点的父节点，相当于指向父节点的指针，所以 parent 数组内实际存储着一个森林（若干棵多叉树）。
    2、用 size 数组记录着每棵树的重量，目的是让 union 后树依然拥有平衡性，而不会退化成链表，影响操作效率。
    3、在 find 函数中进行路径压缩，保证任意树的高度保持在常数，使得 union 和 connected API 时间复杂度为 O(1)。
    4. 动态连通性其实就是一种等价关系，具有「自反性」「传递性」和「对称性

## 将二维坐标映射到一维

    二维坐标 (x,y) 可以转换成 x * n + y 这个数（m 是棋盘的行数，n 是棋盘的列数）

```java
int to(int x,int y){return x*n+y;}
int[] parse(int k){return new int[]{k/n,k%n};}
```

## 穷举有两个关键难点：无遗漏、无冗余

## [算法可视化](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)

## BFS 算法框架
```java
// 输入起点，进行 BFS 搜索
int BFS(Node start) {
    Queue<Node> q; // 核心数据结构
    Set<Node> visited; // 避免走回头路
    q.offer(start); // 将起点加入队列
    visited.add(start);
    int step = 0; // 记录搜索的步数
    while (!q.isEmpty()) {
        int sz = q.size();
        for (int i = 0; i < sz; i++) { /* 将当前队列中的所有节点向四周扩散一步 */
            Node cur = q.poll();
            printf("从 %s 到 %s 的最短距离是 %s", start, cur, step);
            for (Node x : cur.adj()) {/* 将 cur 的相邻节点加入队列 */
                if (!visited(x)) {
                    q.offer(x);
                    visited.add(x);
                }
            }
        }
        step++;
    }
}
```

## 动态规划
分类：
    线性DP (最长不下降子序列  路径问题 )
    背包问题 
    区间问题 

步骤：

    1. 结合原问题和子问题确定状态
       1. 题目求什么？要求出这个值我们知道什么？什么是影响答案的因素？
          1. 一维不行就二维
       2. 状态的参数
          1. 描述位置的（i，j)
          2. 描述数量的 n
          3. 描述对后面有影响的：状态压缩的，一些特殊的性质
    2. 确定状态转移方程
       1. 检查参数是否足够
       2. 分情况：最后一次操作的方式，取不取？怎样取？前一项是什么
       3. 初始边界是什么
       4. 注意无后效性。比如：求A要求B，求B要求C，而求C要求A，这就不符合无后效性
    3. 考虑优化
    4. 确定编程实现方式
       1. 递推
       2. 记忆化搜索
   