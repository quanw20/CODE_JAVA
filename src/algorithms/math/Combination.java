package algorithms.math;

import java.util.Arrays;

public class Combination {
    static int[] s;// stack
    static int idx;// stack下标

    public static void main(String[] args) {
        int[] a = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }; // 待取元素数组
        int k = 3; // 要选择的元素个数
        s = new int[k]; // stack
        comb(a, 0, 0, k); // 从这个数组中选择三个
    }

    /**
     * @param a   代取元素数组
     * @param has 已经有了多少个元素
     * @param cur 当前的下标
     * @param k   要选择的元素个数
     */
    public static void comb(int[] a, int has, int cur, int k) {
        if (has == k) {
            System.out.println(Arrays.toString(s));
            return;
        }
        for (int i = cur; i < a.length; ++i) {
            if (!contains(s, a[i])) {
                s[idx++] = a[i];
                comb(a, has + 1, i, k);
                idx--;//回溯
            }
        }
    }

    private static boolean contains(int[] a, int e) {
        for (int i = 0; i < a.length; ++i)
            if (a[i] == e)
                return true;
        return false;
    }
}
