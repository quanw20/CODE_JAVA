package algorithms.math;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import algorithms.Util;

/**
 * 旋转打印矩阵
 * 
 * 用[0,n*n] 螺旋填充数组
 * 
 * 对二维数组进行顺时针旋转
 * 
 * 对二维数组进行顺时针旋转90°
 * 
 * 一维数组逆序
 * 
 * 边界为1的最大方阵
 * 
 * 子数组最大累加和
 * 
 * 求二维数组所有元素之和
 * 
 */
public class Matrix {
    /**
     * 矩阵乘法 A(m*n)xB(n*p)=C(m*p)
     * 
     * @param a
     * @param b
     * @return
     */
    public static int[][] matrixMutiply(int[][] a, int[][] b) {
        int[][] c = new int[a.length][b[0].length];
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < b[0].length; j++)
                for (int k = 0; k < b.length; k++)
                    c[i][j] += a[i][k] * b[k][j];
        return c;
    }
     /**
     * 使用矩阵计算fib
     * @param n
     * @return
     */
    public static int fibMatrix(int n) {
        if (n <= 2)
            return 1;
        int[][] a = { { 1, 1 }, { 1, 0 } };
        int[][] pow = matrixPow(a, n);
        return pow[1][0];
    }

    /**
     * 矩阵 快速幂
     * 
     * 计算矩阵A的b次幂
     * 
     * @param a
     * @param b
     * @return
     */
    public static int[][] matrixPow(int[][] a, int b) {
        int[][] c = new int[a.length][a.length];
        // 单位阵
        for (int i = 0; i < a.length; i++)
            c[i][i] = 1;
        while (b > 0) {
            if ((b & 1) != 0) {
                c = matrixMutiply(a, c);
            }
            a = matrixMutiply(a, a);
            b >>= 1;
        }
        return c;
    }

    /**
     * 
     * 旋转打印矩阵
     * 
     * <pre>
     *  1-> 2-> 3-> 4
     *              |
     * 13->14->15   5
     * |        |   |
     * 12  17<-16   7
     * |            |
     * 11<-10<- 9<- 8
     * </pre>
     * 
     * @param a
     */
    public static void spiralOrder1(int[][] a) {
        StringBuilder sb = new StringBuilder(a.length * a.length * 2);
        /**
         * {@code k }: 左上边界
         * a[k].length - k : 右边界
         * a.length - k : 下边界
         */
        for (int k = 0; k < a.length >> 1; k++) {
            int i = k, j = k;
            // 在顶部从左向右遍历 j<j.length
            for (; j < a[k].length - k; j++) {
                sb.append(a[i][j]).append(" ");
            }
            // 在右侧从上向下遍历 j==j.length i<i.l
            for (i++, j--; i < a.length - k; i++) {
                sb.append(a[i][j]).append(" ");
            }
            // 在底部从右向左遍历 i==i.length j>=0
            for (i--, j--; j >= k; j--) {
                sb.append(a[i][j]).append(" ");
            }
            // 在左侧从下向上遍历 j==0 i>=0
            for (j++, i--; i > k; i--) {
                sb.append(a[i][j]).append(" ");
            }
        }
        System.out.println(sb);
    }

    /**
     * 代码清晰版
     * 
     * @param matrix
     * @return
     */
    List<Integer> spiralOrder(int[][] matrix) {
        int m = matrix.length, n = matrix[0].length;
        int upper_bound = 0, lower_bound = m - 1;
        int left_bound = 0, right_bound = n - 1;
        List<Integer> res = new LinkedList<>();
        // res.size() == m * n 则遍历完整个数组
        while (res.size() < m * n) {
            if (upper_bound <= lower_bound) {
                // 在顶部从左向右遍历
                for (int j = left_bound; j <= right_bound; j++) {
                    res.add(matrix[upper_bound][j]);
                }
                // 上边界下移
                upper_bound++;
            }

            if (left_bound <= right_bound) {
                // 在右侧从上向下遍历
                for (int i = upper_bound; i <= lower_bound; i++) {
                    res.add(matrix[i][right_bound]);
                }
                // 右边界左移
                right_bound--;
            }

            if (upper_bound <= lower_bound) {
                // 在底部从右向左遍历
                for (int j = right_bound; j >= left_bound; j--) {
                    res.add(matrix[lower_bound][j]);
                }
                // 下边界上移
                lower_bound--;
            }

            if (left_bound <= right_bound) {
                // 在左侧从下向上遍历
                for (int i = lower_bound; i >= upper_bound; i--) {
                    res.add(matrix[i][left_bound]);
                }
                // 左边界右移
                left_bound++;
            }
        }
        return res;
    }

    /**
     * 用[0,n*n] 螺旋填充数组
     * 
     * @param n
     * @return
     */
    public static int[][] fill(int n) {
        class Solution {

            public int[][] generateMatrix(int n) {
                int[][] ret = new int[n][n];
                int cnt = 1;
                int left = 0, right = n - 1, lo = 0, hi = n - 1;
                while (lo <= hi) {
                    for (int i = left; i <= right; i++) {
                        ret[lo][i] = cnt++;
                    }
                    lo++;
                    for (int i = lo; i <= hi; i++) {
                        ret[i][right] = cnt++;
                    }
                    right--;
                    for (int i = right; i >= left; i--) {
                        ret[hi][i] = cnt++;
                    }
                    hi--;
                    for (int i = hi; i >= lo; i--) {
                        ret[i][left] = cnt++;
                    }
                    left++;
                }
                return ret;
            }
        }
        return new Solution().generateMatrix(n);
    }

    /**
     * 对二维数组进行顺时针旋转
     * 
     * 按主对角线翻转 + 每行按列逆序
     * 
     * @param a
     */
    public static void rotate1(int[][] a) {
        // 按主对角线翻转
        for (int i = 0; i < a.length; i++) {
            // j==i
            for (int j = i; j < a[i].length; j++) {
                int temp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = temp;
            }
        }
        // 每行按列逆序
        for (int[] row : a) {
            reverse(row);
        }

    }

    /**
     * 对二维数组进行顺时针旋转90°
     * 
     * 按副对角线翻转 + 交换行
     * 
     * @param a
     */
    public static void rotate2(int[][] a) {
        // 按副对角线翻转
        int n = a.length - 1;
        for (int i = 0; i < a.length; i++) {
            // j==i
            for (int j = 0; j <= n - i; j++) {
                int temp = a[i][j];
                a[i][j] = a[n - j][n - i];
                a[n - j][n - i] = temp;
            }
        }
        // 交换行
        int i = 0, j = a.length - 1;
        while (i < j) {
            int[] temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;
        }

    }

    /**
     * 一维数组逆序
     * 
     * @param a
     */
    public static void reverse(int[] a) {
        int i = 0, j = a.length - 1;
        while (i < j) {
            Util.swap(a, i, j);
            i++;
            j--;
        }
    }

    /**
     * 将二维数组逆时针旋转90°
     * 
     * 按副对角线翻转 + 每行按列逆序
     * 
     * @param a
     */
    public static void rotateReverse2(int[][] a) {
        // 按副对角线翻转
        int n = a.length - 1;
        for (int i = 0; i < a.length; i++) {
            // j==i
            for (int j = 0; j <= n - i; j++) {
                int temp = a[i][j];
                a[i][j] = a[n - j][n - i];
                a[n - j][n - i] = temp;
            }
        }
        // 每行按列逆序
        for (int[] row : a) {
            reverse(row);
        }
    }

    /**
     * 逆时针旋转90度
     * 
     * 按主对角线翻转 + 交换列
     * 
     * @param a
     */
    public static void rotateReverse1(int[][] a) {
        // 按主对角线翻转
        for (int i = 0; i < a.length; i++) {
            // j==i
            for (int j = i; j < a[i].length; j++) {
                int temp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = temp;
            }
        }
        // 交换列
        int i = 0, j = a.length - 1;
        while (i < j) {
            int[] temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;
        }
    }

    /**
     * 边界为1的最大方阵
     * 
     * @param a
     * @return
     */
    public static int maxN(int[][] a) {
        int n = a.length;
        while (n > 0) {
            for (int i = 0; i < a.length; i++) {
                l3: for (int j = 0; j < a[i].length; j++) {
                    if (j + n > a.length || i + n > a.length)
                        break;
                    int r = i;
                    int c = j;
                    // 检查四条边
                    // 上
                    while (c < j + n) {
                        if (a[r][c] == 0)
                            continue l3;
                        c++;
                    }
                    c--;
                    // 右
                    while (r < i + n) {
                        if (a[r][c] == 0)
                            continue l3;
                        r++;
                    }
                    r--;
                    // 下
                    while (c >= j) {
                        if (a[r][c] == 0)
                            continue l3;
                        c--;
                    }
                    c++;
                    // 左
                    while (r >= i) {
                        if (a[r][c] == 0)
                            continue l3;
                        r--;
                    }
                    r++;
                    return n;
                }
            }
            n--;
        }
        return 1;
    }

    /**
     * 边界为1的最大方阵(打表)
     * 
     * 
     * @param a
     * @return
     */
    public static int maxN2(int[][] a) {
        final class Solution {
            // 记录用的数组
            static int[][][] rec;

            // 初始化rec
            static void heaper(int[][] a) {
                rec = new int[a.length][a[0].length][2];
                // 初始化最后一行
                int row = a.length - 1;
                for (int j = a.length - 1; j >= 0; j--) {
                    if (a[row][j] == 1) {
                        if (j == a.length - 1)
                            rec[row][j][0] = 1;
                        else
                            rec[row][j][0] = rec[row][j + 1][0] + 1;
                    }
                }
                row--;
                for (int i = row; i >= 0; i--) {
                    for (int j = a.length - 1; j >= 0; j--) {
                        if (a[i][j] == 1) {
                            if (j == a.length - 1)
                                rec[i][j][0] = 1;
                            else
                                rec[i][j][0] = rec[i][j + 1][0] + 1;

                            rec[i][j][0] = rec[i + 1][j + 1][0] + 1;
                        }
                    }
                }
            }

            public static boolean check(int i, int j, int n) {
                if (rec[i][j][0] >= n && rec[i][j][1] >= n && rec[i][j + n - 1][1] >= n && rec[i + n - 1][j][0] >= n)
                    return true;
                return false;
            }

        }
        Solution.heaper(a);
        int n = a.length;
        while (n > 0) {
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[i].length; j++) {
                    if (j + n > a.length || i + n > a.length)
                        break;
                    if (Solution.check(i, j, n) == true) {
                        return n;
                    }
                }
            }
            n--;
        }
        return 1;
    }

    /**
     * 子数组最大累加和
     * 
     * O(n)
     * 
     * @param a
     * @return
     */
    public static int[] maxSumSub(int[] a) {
        int p1 = 0;
        int p2 = 0;
        int max = 0;
        int sum = a[0];
        for (int i = 1; i < a.length; i++) {
            if (sum >= 0) {
                sum += a[i];
            } else {
                sum = a[i];
                p1 = i;
            }
            if (sum > max) {
                max = sum;
                p2 = i;
            }
        }
        int[] ret = new int[p2 - p1 + 1];
        System.arraycopy(a, p1, ret, 0, ret.length);
        return ret;
    }

    /**
     * 子数组最大累加和(可以获取下标)
     * 
     * @param a
     * @param p 用来带出下标的数组
     * @return
     */
    public static int maxSum(int[] a, int[] p) {
        int max = 0;
        int sum = a[0];
        for (int i = 1; i < a.length; i++) {
            if (sum >= 0) {
                sum += a[i];
            } else {
                sum = a[i];
                p[0] = i;
            }
            if (sum > max) {
                max = sum;
                p[1] = i;
            }
        }
        return max;
    }

    /**
     * 子矩阵最大累加和
     * 
     * @param a
     * @param b 用来带出下标的数组
     * @return
     */
    public static int maxSum(int[][] a, int[][][] b) {
        int m = a.length;
        int n = a[0].length;
        int max = Integer.MIN_VALUE;
        int[] colSum = new int[n];
        int[] p0 = new int[2];// 从计算行数组的函数中带值回来可能会被覆盖
        int[] q = null;// 左右 ,当最大值获得时, 将p0赋给q
        int p1 = 0, p2 = 0;// 上下
        // 把一行当作一个元素累加
        for (int row = 0; row < m; row++) {
            // 起始行row到m
            for (int i = row; i < m; i++) {
                // 按列累加
                for (int j = 0; j < n; j++) {
                    colSum[j] += a[i][j];
                }

                // 求列和的最大子列和
                int t = maxSum(colSum, p0);

                // 每加一行比较一次
                if (t > max) {
                    max = t;
                    p1 = row;
                    p2 = i;
                    q = Util.deepCopy(p0);
                }
            }
            // 清空,准备下一次
            Arrays.fill(colSum, 0);
        }
        // 将子矩阵返回
        if (b != null) {
            b[0] = Util.deepCopy(a, p1, p2 - p1 + 1, q[0], q[1] - q[0] + 1);
        }
        return max;
    }

    public static int maxSum(int[][] a) {
        return maxSum(a, null);
    }

    /**
     * 求二维数组所有元素之和
     * 
     * @param a
     * @return
     */
    public static int sum(int[][] a) {
        int ret = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                ret += a[i][j];
            }
        }
        return ret;
    }

}
