package algorithms.math;

public class Fraction {
    int up, dn;

    public Fraction() {

    }

    public Fraction(int u, int d) {
        up = u;
        dn = d;
    }

    public static Fraction reduction(Fraction f) {
        if (f.dn < 0) {
            f.dn = -f.dn;
            f.up = -f.up;
        }
        if (f.up == 0) {
            f.dn = 1;
        } else {
            int d = MathProblems.gcd(Math.abs(f.up), Math.abs(f.dn));
            f.up /= d;
            f.dn /= d;
        }
        return f;
    }

    public static Fraction add(Fraction f1, Fraction f2) {
        Fraction f = new Fraction();
        f.up = f1.up * f2.dn + f2.up * f1.dn;
        f.dn = f1.dn * f2.dn;
        return reduction(f);
    }

    public static Fraction minus(Fraction f1, Fraction f2) {
        Fraction f = new Fraction();
        f.up = f1.up * f2.dn - f2.up * f1.dn;
        f.dn = f1.dn * f2.dn;
        return reduction(f);
    }

    public static Fraction mutiply(Fraction f1, Fraction f2) {
        Fraction f = new Fraction();
        f.up = f1.up * f2.up;
        f.dn = f1.dn * f2.dn;
        return reduction(f);
    }

    public static Fraction divide(Fraction f1, Fraction f2) {
        Fraction f = new Fraction();
        f.up = f1.up * f2.dn;
        f.dn = f1.dn * f2.up;
        return reduction(f);
    }

    @Override
    public String toString() {
        return dn == 1 ? "" + up : up + "/" + dn + "\t";
    }
}
