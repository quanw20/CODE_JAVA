package algorithms.math;

import org.junit.jupiter.api.Test;

import static algorithms.Util.*;
import static algorithms.math.Matrix.*;

public class MatrixTest {
    @Test
    public void testRP() {
        int[][] a = {
                { 1, 2, 3, 4 },
                { 5, 6, 7, 8 },
                { 9, 10, 11, 12 },
                { 13, 14, 15, 16 } };
        spiralOrder1(a);
    }

    @Test
    public void testGenerate() {
        int[][] fill = fill(1);
        print(fill);
    }

    @Test
    public void testM2D() {
        int[][] a = m2d(4, 5);
        print(a);

        int[][] b = m2dr(5, 3);
        print(b);

        int[] c = getRandomArray(20);
        print(c);
    }

    @Test
    public void testMaxN() {
        int[][] a = {
                { 0, 1, 1, 1, 1 },
                { 0, 1, 0, 0, 1 },
                { 0, 1, 0, 0, 1 },
                { 0, 1, 1, 1, 1 },
                { 0, 1, 1, 1, 1 } };

        System.out.println(maxN(a));
    }

    @Test
    public void testMaxSum() {
        int[] a = { 1, -2, 3, 5, -3, 6 };
        int[] maxSum = maxSumSub(a);
        print(maxSum);

    }

    /**
     * 10 98 29 -3
     * 15 3 -9 61
     * 19 54 27 27
     * -27 62 45 -6
     * 
     */
    @Test
    public void testMaxSun2() {
        random.setSeed(System.currentTimeMillis());
        int[][] m = m2dr(4, 4);
        int[][][] b = new int[1][][];
        int maxSum2 = maxSum(m, b);

        System.out.println("maxSum2: " + maxSum2);

        print(m);
        System.out.println("sum(m): " + sum(m));
        System.out.println("sum(b[0]): " + sum(b[0]));
        print(b[0]);
    }

    @Test
    public void testMatrixCopy() {
        int[][] a = {
                { 0, 1, 1, 1, 1 },
                { 1, 1, 0, 0, 1 },
                { 0, 1, 0, 0, 1 },
                { 1, 1, 1, 1, 1 },
                { 0, 1, 1, 1, 1 } };
        print(a);
        int[][] cp = deepCopy(a, 1, 4, 2, 3);
        print(cp);
    }

    @Test
    public void testRotate() {
        int[][] a = m2dr(5, 5);
        System.out.println("原数组:");
        print(a);
        rotate1(a);
        System.out.println("90:");
        print(a);
        rotate2(a);
        System.out.println("90:");
        print(a);
        rotateReverse1(a);
        System.out.println("-90:");
        print(a);
        System.out.println("-90:");
        rotateReverse2(a);
        print(a);
    }

}
