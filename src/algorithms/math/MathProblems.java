package algorithms.math;

import java.util.*;

/**
 * 快速幂
 * 
 * 矩阵乘法
 * 
 * 矩阵快速幂
 * 
 * 数字黑洞 6174
 * 
 * gcd/clm
 * 
 * 组合数combination
 * 
 * Lucas定理计算comb(n,m)%p(p是素数)
 * 
 * 素数判断
 * 
 * 筛法求素数 [2,n]之间的素数
 * 
 * 质因子分解
 * 
 * 计算n!中有多少个质因子p
 * 
 * 十进制小数转二进制小数
 * 
 * 分数 Fraction
 */
public class MathProblems {
    /**
     * 快速幂 a^b%m
     * 
     * @param a
     * @param b
     * @param m
     * @return
     */
    public static long fastPow(int a, int b, int m) {
        long ret = 1;
        while (b > 0) {
            if ((b & 1) == 1) {// b最后一位是奇数1
                ret = ret * a % m;
            }
            a = a * a % m;
            b >>= 1;
        }
        return ret;
    }

    

    // 数字黑洞 6174
    private static Integer[] toArray(int n) {
        Integer[] ret = new Integer[4];
        for (int i = 0; i < 4; i++) {
            ret[i] = n % 10;
            n /= 10;
        }
        return ret;
    }

    private static int toInt(Integer[] a) {
        int n = 0;
        for (int i = 0; i < 4; ++i) {
            n = n * 10 + a[i];
        }
        return n;
    }

    public static void numberHole() {
        int max, min;
        try (Scanner in = new Scanner(System.in)) {
            int n = in.nextInt();
            Integer[] num;
            while (true) {
                num = toArray(n);
                Arrays.sort(num);
                min = toInt(num);
                Arrays.sort(num, Comparator.reverseOrder());
                max = toInt(num);
                n = max - min;
                // 04少于4位用0填充
                System.out.printf("%04d - %04d = %04d\n", max, min, n);
                if (n == 0 || n == 6174)
                    break;
            }
        }
    }

    // gcd
    public static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    public static int lcm(int a, int b) {
        return a / gcd(a, b) * b;
    }

    /**
     * Combination
     * 
     * 递归简洁版 非装13不用
     */
    public static long combination(int n, int m) {
        if (m == 0 || n == m)
            return 1;
        return combination(n - 1, m) + combination(n - 1, m - 1);
    }

    /**
     * 快速循环版
     * 
     * @param n
     * @param m
     * @return
     */
    public static long comb(int n, int m) {
        long ret = 1;
        for (long i = 1; i <= m; ++i) {
            ret = ret * (n - m + i) / i; // 不要写成 *=
        }
        return ret;
    }

    /**
     * Lucas定理计算comb(n,m)%p(p是素数)
     * 
     * @param n
     * @param m
     * @param p
     * @return
     */
    public static long lucas(int n, int m, int p) {
        if (m == 0)
            return 1;
        return comb(n % p, m % p) * lucas(n / p, m / p, p) % p;
    }

    /**
     * 素数
     * 
     * @param n
     * @return
     */
    public static boolean isPrime(int n) {
        if (n < 2)
            return false;
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    /**
     * 筛法求素数 [2,n]之间的素数
     * 
     * 复杂度O(nloglogn)
     */
    public static int[] findPrime(final int n) {
        int[] prime = new int[n];
        int cnt = 0;
        boolean[] np = new boolean[n];// Not a Prime
        for (int i = 2; i < n; ++i) {
            if (np[i] == false) {
                prime[cnt++] = i;
                // 筛
                for (int j = i + i; j < n; j += i) {
                    np[j] = true;
                }
            }
        }
        return Arrays.copyOf(prime, cnt);
    }

    public static int[] findPrime(final int m, final int n) {
        int[] prime = new int[n];
        int cnt = 0;
        boolean[] np = new boolean[n];// Not a Prime
        for (int i = 2; i < n; ++i) {
            if (np[i] == false) {
                // 添加条件
                if (i >= m)
                    prime[cnt++] = i;
                // 筛
                for (int j = i + i; j < n; j += i) {
                    np[j] = true;
                }
            }
        }
        return Arrays.copyOf(prime, cnt);
    }

    /**
     * 质因子分解
     * 
     * @param n
     * @return
     */
    public static int[][] primeFactor(int n) {
        int m = n;
        int sqr = (int) Math.sqrt(n);
        int[][] ret = new int[sqr][2];
        int cnt = 0;
        int[] prime = findPrime(sqr);
        for (int i = 0; i < prime.length; i++) {
            if (n % prime[i] == 0) {
                ret[cnt][0] = prime[i];
                ret[cnt][1] = 0;
                while (n % prime[i] == 0) {
                    ++ret[cnt][1];
                    n /= prime[i];
                }
                cnt++;
            }
        }
        if (n != 1) {// 未除尽
            ret[cnt][0] = m;
            ret[cnt++][1] = 1;
        }
        return Arrays.copyOf(ret, cnt);
    }

    public static void printFactor(int[][] p) {
        int ret = 1;
        for (int i = 0; i < p.length; ++i) {
            for (int j = 0; j < p[i][1]; ++j) {
                System.out.print(p[i][0] + (i != p.length - 1 || j != p[i][1] - 1 ? " x " : " = "));
                ret *= p[i][0];
            }

        }
        System.out.println(ret);
    }

    /**
     * 连续因子
     * 
     * @param n
     * @return
     */
    public static int[] maxConsecutiveFactors(int n) {
        int sqr = (int) Math.sqrt(n);
        int idx = 0;
        int[] ret = new int[sqr];
        for (int i = 2; i < sqr; i++) {
            if (n % i == 0) {
                ret[idx++] = i;
                // n /= i;
            }
        }
        return Arrays.copyOf(ret, idx);
    }

    public static String sep(int[] a, String s) {
        StringBuilder sb = new StringBuilder(a.length << 1);
        for (int i : a) {
            sb.append(i).append(s);
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    /**
     * 最大连续递增序列
     * 
     * @param a
     * @return
     */
    public static int[] LongestConsecutiveIncrease(int[] a) {
        int p = 0, max = 0, length;
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] + 1 == a[i + 1]) {
                for (int j = i + 1; j < a.length - 1; j++) {
                    if (a[j] + 1 != a[j + 1]) {
                        length = j - i + 1;
                        if (length > max) {
                            p = i;
                            max = length;
                        }
                        break;
                    }
                }
            }
        }
        int[] ret = new int[max];
        System.arraycopy(a, p, ret, 0, max);
        return ret;
    }

    /**
     * 十进制小数转二进制小数
     * 
     * 乘基取整
     * 
     * @param d
     * @return
     */
    public static String binaryFloat(double d) {
        StringBuilder s = new StringBuilder("0.");
        while (d > 0) {
            d *= 2;
            if (d >= 1) {
                d -= 1;
                s.append("1");
            } else {
                s.append("0");
            }
            if (s.length() > 34) {
                return s.toString();
                // s.delete(0, s.length());
                // s.append("Error");
                // break;
            }
        }
        return s.toString();
    }

    // 将10进制小数转化位二进制小数
    public static String toBinary(double d) {
        int n = (int) d;
        // Integer.toString(i, radix)
        return Integer.toBinaryString(n) + binaryFloat(d - n);
    }

    /**
     * 将一个数用3的幂表示
     * 
     * 5= 9-3-1
     * 
     * 5= 1*3^1 + 2*3^0
     * 
     * @param n
     */
    public static void weight(int n) {
        String s = Integer.valueOf(n + "", 3).toString();
        StringBuilder sb = new StringBuilder(s.length() + 1);
        toArray(s);
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '2') {
                sb.append('-');
            } else {
                sb.append(s.charAt(i));
            }
        }
    }

    private static int[] toArray(String s) {
        int[] a = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            a[i] = s.charAt(i);
        }
        return a;
    }

}
