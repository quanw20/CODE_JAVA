package algorithms.math;

import org.junit.jupiter.api.Test;

import static algorithms.math.MathProblems.*;
import static algorithms.math.Matrix.*;

import java.util.Arrays;

public class MathProblemsTest {
    @Test
    public void testMatrix() {
        int[][] a = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        int[][] b = { { 1, 2, 3, 4, 1 }, { 5, 6, 7, 8, 1 }, { 9, 10, 11, 12, 1 } };
        int[][] c = matrixMutiply(a, b);
        System.out.println(Arrays.deepToString(c));
        int[][] d = matrixPow(a, 3);
        System.out.println(Arrays.deepToString(d));
    }

    @Test
    public void testFraction() {
        Fraction f1 = new Fraction(1, 3);
        Fraction f2 = new Fraction(100001, 3);
        System.out.println(Fraction.add(f1, f2));
    }

    @Test
    public void testPrime() {
        int n = 1000000;
        long s = System.currentTimeMillis();
        int[] res = findPrime(n);
        System.out.println("Time use: " + (System.currentTimeMillis() - s));// 14
        // System.out.println(Arrays.toString(res));
        System.out.println(res.length);
        s = System.currentTimeMillis();
        for (int i = 2; i < n; i++) {
            isPrime(i);
        }
        System.out.println("Time use: " + (System.currentTimeMillis() - s));// 313

        System.out.println(Arrays.deepToString(primeFactor(180)));
        printFactor(primeFactor(630));

    }

    @Test
    public void testConsecutiveFactors() {
        int[] ma = maxConsecutiveFactors(630);
        int[] fm = LongestConsecutiveIncrease(ma);
        System.out.println(fm.length);
        System.out.println(sep(fm, "*"));

    }

    @Test
    public void testCombination() {

        int n = 50, m = 12;
        long s = System.currentTimeMillis();
        long a = MathProblems.comb(n, m);
        System.out.println("Time use: " + (System.currentTimeMillis() - s));
        System.out.println(a);

        s = System.currentTimeMillis();
        a = MathProblems.combination(n, m);
        System.out.println("Time use: " + (System.currentTimeMillis() - s));
        System.out.println(a);

        System.out.println(MathProblems.comb(5, 2));
        System.out.println(MathProblems.comb(120, 23));
        System.out.println(MathProblems.lucas(120, 23, 10000));

    }

    @Test
    public void testBit() {
        // int c = countOneInBinary(121231234);
        // System.out.println(c);
        // // int n=0;
        // for (int n = 0; n < 20; n++) {
        // System.out.println(n + "是奇数? " + isOdd(n));
        // System.out.println(n + "是2的整数次方? " + isPowOf2(n));
        // }

        // System.out.println(exchangeOddAndEven(9));
        // Assertions.assertTrue(exchangeOddAndEven(9)==6);

        System.out.println(binaryFloat(0.625));
        System.out.println(toBinary(125.625));
    }

}
