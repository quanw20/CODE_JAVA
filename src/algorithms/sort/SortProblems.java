package algorithms.sort;

import java.util.*;

import algorithms.Util;

public class SortProblems {
    /**
     * 从有序数组中找出n的两个因子a,b使得 a+b=n
     * 
     * @param a
     * @param n
     * @return
     */
    public static int[][] findFactorOfSum(int[] a, int n) {
        int[][] ret = new int[a.length >> 1][];
        int idx = -1;// ret 下标计数
        int p1 = 0, p2 = a.length - 1;
        while (p1 < p2) {
            if (a[p1] + a[p2] > n)
                p2--;
            else if (a[p1] + a[p2] < n)
                p1++;
            else {
                ret[++idx] = new int[] { a[p1], a[p2] };
                p1++;
                p2--;
            }
        }
        return Arrays.copyOf(ret, idx);
    }

    /**
     * 从部分有序的数组中找到最短的需要排序的序列
     * 
     * @param a
     * @return
     */
    public static int[] findSegmentNeedSort(int[] a) {
        // 左右边界的下标
        int p1 = 0;
        int p2 = 0;
        int max = a[0];
        int min = a[a.length - 1];
        // 扩展右端点:更新历史最高,只要右侧出现比历史最高地的,就应该将右边界扩展到此处
        for (int i = 0; i < a.length; i++) {
            if (a[i] > max)
                max = a[i];
            // 只要低于历史高峰,就要扩展排序区间的右端点
            if (a[i] < max)
                p2 = i;
        }
        // 找左端点: 更新历史最低,只要左边出现比历史最低更低的就应该将左边界扩展到此处
        for (int i = a.length - 1; i >= 0; i--) {
            if (a[i] < min)
                min = a[i];
            if (a[i] > min)
                p1 = i;
        }
        return new int[] { p1, p2 };

    }

    /**
     * 用数组中元素组合一个最小的数
     * 
     * @param a
     * @return
     */
    public static int findMinCombination(int[] a) {
        Integer[] box = Util.box(a);
        // 使用比较器
        Arrays.sort(box, (c, b) -> {
            String s1 = c + "" + b;
            String s2 = b + "" + c;
            return s1.compareTo(s2);
        });
        StringBuilder s = new StringBuilder();
        for (Integer i : box) {
            s.append(i);
        }
        return Integer.parseInt(s.toString());
    }

}
