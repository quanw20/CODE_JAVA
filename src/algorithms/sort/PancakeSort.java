package algorithms.sort;

import java.util.Arrays;

public class PancakeSort {
    public int[] pancakeSort(int[] a) {
        return sort(a, a.length - 1);
    }

    private int[] sort(int[] a, int n) {
        if (n == 0)
            return a;
        // find max
        int maxIndex = 0;
        for (int i = 0; i <= n; ++i) {
            if (a[i] > a[maxIndex]) {
                maxIndex = i;
            }
        }
        // reverse
        reverse(a, 0, maxIndex);
        // place last
        reverse(a, 0, n);

        return sort(a, n - 1);
    }

    private void reverse(int[] a, int i, int j) {
        int temp;
        while (i < j) {
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            ++i;
            --j;
        }
    }

    public static void main(String[] args) {
        int[] a = { 12, 314, 125, 12, 41, 521, 5134, 6346 };
        int[] pancakeSort = new PancakeSort().pancakeSort(a);
        System.out.println(Arrays.toString(pancakeSort));
    }
}
