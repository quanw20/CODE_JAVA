package algorithms.sort;

import java.util.stream.IntStream;

public class CountingSort {
    /**
     * 计数排序
     * 
     * O(n+k) k=maxElementOf(sourceArray)
     * 
     * 适合元素分布计较集中的
     * 
     * @param a 
     */
    public static void countingSort(int[] a) {
        int max = IntStream.of(a).max().getAsInt();
        int[] b = new int[max + 1];
        for (int i = 0; i < a.length; i++) {
            b[a[i]]++;
        }
        int cnt = 0;
        for (int i = 0; i < b.length; i++) {
            while (b[i] != 0) {
                a[cnt++] = i;
                b[i]--;
            }
        }
    }

    /**
     * 返回最大值
     * 
     * @param a
     * @return
     */
    private static int max(int[] a) {
        int ret = 0;
        for (int i = 1; i < a.length; i++) {
            if (a[i] > ret)
                ret = a[i];
        }
        return ret;
    }
}
