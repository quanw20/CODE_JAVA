package algorithms.sort;

import java.util.Arrays;

public class QuickSelect {
    int partition(int[] a, int l, int r) {
        int pivot = a[l];
        while (l < r) {
            while (l < r && a[r] >= pivot)// 大于等于都跳过
                --r;
            a[l] = a[r];
            while (l < r && a[l] <= pivot)
                ++l;
            a[r] = a[l];
            a[l] = pivot;
        }
        return l;
    }

    void sort(int[] a, int l, int r) {
        if (l >= r)
            return;
        int mid = partition(a, l, r);
        System.out.println(mid);
        sort(a, l, mid - 1);
        sort(a, mid + 1, r);
    }

    public static void main(String[] args) {
        int[] a = { 1, 5, 4, 2, 6, 9, 7, 8, 6 };
        new QuickSelect().sort(a, 0, a.length - 1);
        System.out.println(Arrays.toString(a));
    }
}
