package algorithms.sort;

import java.util.ArrayList;

/**
 * 基数排序
 * 
 * 创建一个0-9的二维表
 * 
 * a={12,22,34,46,9,11,3,88,45,70}
 * 
 * <pre>
 * 
 * 第一轮按最低为排序
 * +-+--+
 * |0|70
 * |1|11
 * |2|12|22
 * |3| 3
 * |4|34
 * |5|45
 * |6|46
 * |7|
 * |8|88
 * |9| 9
 * +-+--+
 * 得到{70,11,12,22,3,34,45,46,88,9}(任然无序)
 * 
 * 第二轮按次低为排序
 * +-+--+
 * |0| 3| 9
 * |1|11|12
 * |2|22
 * |3|34
 * |4|45|46
 * |5|
 * |6|
 * |7|70
 * |8|88
 * |9|
 * +-+--+
 * 得到{3,9,11,12,22,34,45,46,70,88}
 * 
 * 最高位数有多少位就做多少轮
 * </pre>
 * 
 * O(kn)
 * 
 * 有负数加上一个偏移(offset)使其变为正数,拍完后减去
 * 
 */
@SuppressWarnings("unchecked")
public class RadixSort {

    static ArrayList<Integer>[] bucket = (ArrayList<Integer>[]) new ArrayList[10];
    static {
        for (int i = 0; i < bucket.length; i++) {
            bucket[i] = new ArrayList<>();
        }
    }

    public static void sort(int[] a, int d) {
        for (int i = 0; i < a.length; i++) {
            push(a[i], getNumOn(a[i], d));
        }
        int k = 0;
        for (int i = 0; i < bucket.length; i++) {
            for (Integer b : bucket[i]) {
                a[k++] = b;
            }
        }
        clearAll();
    }

    /**
     * 返回最大值
     * 
     * @param a
     * @return
     */
    private static int max(int[] a) {
        int ret = 0;
        for (int i = 1; i < a.length; i++) {
            if (a[i] > ret)
                ret = a[i];
        }
        return ret;
    }

    public static void radixSort(int[] a) {
        int maxNum = max(a);
        int d = 1, dNum = 1;
        while (maxNum / 10 != 0) {
            dNum++;
            maxNum /= 10;
        }
        while (d <= dNum) {
            sort(a, d++);
        }

    }

    private static void clearAll() {
        for (ArrayList<?> a : bucket) {
            a.clear();
        }
    }

    private static void push(int i, int numOn) {
        bucket[numOn].add(i);
    }

    private static int getNumOn(int i, int d) {
        if (d == 1)
            return i % 10;
        int j = 10;
        while (d-- != 1) {
            j *= 10;
        }
        return i % j / (j / 10);
    }
}
