package algorithms.onoff;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Switch 开关问题
 * 1. 同一个开关点击两次就会恢复原状,所以大于等于2此的点击是多余的
 * 2. 点击的开关集合如果相同的话,其次序是无关的
 */
public class Switch {
    final int N = 1001;
    int[] dir = new int[N];
    int[] f = new int[N];
    int n;

    int calc(int k) {
        Arrays.fill(f, 0);
        int res = 0;
        int sum = 0;// f的和
        for (int i = 0; i + k <= n; ++i) {
            if ((dir[i] + sum) % 2 != 0) {
                ++res;
                f[i] = 1;
            }
            sum += f[i];
            if ((i - k + 1 >= 0)) {
                sum -= f[i - k + 1];
            }
        }
        for (int i = n - k + 1; i < n; ++i) {
            if ((dir[i] + sum) % 2 != 0) {
                return -1;
            }
            if (i - k + 1 >= 0) {
                sum -= f[i - k + 1];
            }
        }
        return res;
    }

    /**
     * POJ 3276
     * 
     * 7
     * BBFBFBB
     * 
     * @throws IOException
     */
    void faceTheRightWay() throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        n = in.nextInt();
        char[] ca = in.next().toCharArray();
        for (int i = 0; i < n; ++i) {
            dir[i] = ca[i] == 'B' ? 1 : 0;
        }
        int K = 1, M = n;
        for (int k = 1; k <= n; ++k) {
            int m = calc(k);
            if (m != -1 && m < M) {
                M = m;
                K = k;
            }
        }
        System.out.printf("%d %d\n", K, M);
    }

    public static void main(String[] args) throws IOException {
        new Switch().faceTheRightWay();
    }
}