package algorithms;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;


public class DataAndTime {
    public static void main(String[] args) {
        System.out.println(countDays());
    }

    static int countDays() {
        LocalDate d1 = LocalDate.of(2002, 10, 9);

        LocalDate now = LocalDate.now();
        int cnt = 0;
        for (LocalDate d = d1; !d.equals(now); d = d.plus(1, ChronoUnit.DAYS)) {
            ++cnt;
        }
        System.out.printf("%s -> %s, 已经过了 %d天,也即 %d个小时或 %d分钟\n", d1.toString(), now.toString(), cnt, cnt * 24,
                cnt * 24 * 60);
        return cnt;
    }

}
