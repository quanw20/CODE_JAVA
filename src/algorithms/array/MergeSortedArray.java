package algorithms.array;

import java.util.Arrays;

/**
 * Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
 * Output: [1,2,2,3,5,6]
 * Explanation: The arrays we are merging are [1,2,3] and [2,5,6].
 * The result of the merge is [1,2,2,3,5,6] with the underlined elements coming
 * from nums1.
 */
public class MergeSortedArray {
  public void merge(int[] nums1, int m, int[] nums2, int n) {
    if (n == 0) {
      return;
    }
    int p1 = m - 1, p2 = n - 1, p = m + n - 1;
    while (p >= 0) {
      if (p1 != -1 && p2 != -1 && nums1[p1] > nums2[p2]
          || p2 == -1) {
        nums1[p] = nums1[p1];
        --p1;
      } else if (p1 != -1 && p2 != -1
          && nums1[p1] <= nums2[p2]
          || p1 == -1) {
        nums1[p] = nums2[p2];
        --p2;
      }
      --p;
    }
  }

  public static void main(String[] args) {
    new MergeSortedArray().merge(
        new int[] { 0 },
        0,
        new int[] { 1 },
        1);
  }

}
