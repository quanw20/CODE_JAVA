package algorithms.array;

import java.util.HashMap;
import java.util.stream.IntStream;
/**
 * 1. sort
 * 2. hash
 */
public class ContainsDuplicate {
  public boolean containsDuplicate(int[] nums) {
    HashMap<Integer, Integer> map = new HashMap<>();
    for (int i = 0; i < nums.length; ++i) {
      map.merge(nums[i], 1, Integer::sum);
    }
    return map.size() == nums.length;
  }

  public boolean containsDuplicate2(int[] nums) {
    return IntStream.of(nums).distinct().count() != nums.length;
  }
}
