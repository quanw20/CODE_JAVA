package datastructure.digraph;

public class DirectedDFS {
    private boolean[] marked;

    /**
     * 单点可达性
     * 
     * @param G
     * @param s
     */
    public DirectedDFS(Graph G, int s) {
        marked = new boolean[G.V()];
        dfs(G, s);
    }

    private void dfs(Graph g, int v) {
        marked[v] = true;
        for (int w : g.adj(v)) {
            if (!marked[w])
                dfs(g, w);
        }
    }

    /**
     * 多点可达性
     * 
     * @param G
     * @param s
     */
    public DirectedDFS(Graph G, Iterable<Integer> s) {
        marked = new boolean[G.V()];
        for (Integer integer : s) {
            dfs(G, integer);
        }
    }

    public boolean marked(int v) {
        return marked[v];
    }
}
