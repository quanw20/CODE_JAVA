package datastructure.digraph;

import java.util.LinkedList;
import java.util.Queue;

public class DirectedBFS {
    private boolean[] marked;

    /**
     * 从G中找到s可达的所有顶点
     * 
     * @param G
     * @param s
     */
    public DirectedBFS(Graph G, int s) {
        marked = new boolean[G.V()];
        bfs(G, s);
    }

    private void bfs(Graph G, int s) {
        Queue<Integer> q = new LinkedList<>();
        q.offer(s);
        marked[s] = true;
        while (!q.isEmpty()) {
            Integer p = q.poll();
            for (int w : G.adj(p))
                if (!marked[w])
                    marked[w] = true;
        }
    }

    public DirectedBFS(Graph G, Iterable<Integer> s) {
        marked = new boolean[G.V()];
        for (Integer integer : s) {
            bfs(G, integer);
        }
    }

    public boolean marked(int v) {
        return marked[v];
    }
}
