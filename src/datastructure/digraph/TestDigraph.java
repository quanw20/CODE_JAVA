package datastructure.digraph;

import java.util.HashSet;

import org.junit.jupiter.api.*;

public class TestDigraph {
    static Graph G;

    @BeforeAll
    static void beforeAll() {
        System.out.println("init");
        G = new Graph(10);
        G.addEdge(1, 0);
        G.addEdge(2, 1);
        G.addEdge(2, 3);
        G.addEdge(2, 8);
        G.addEdge(3, 4);
        // G.addEdge(4, 5);
        G.addEdge(5, 6);
        G.addEdge(6, 7);
        G.addEdge(7, 8);
        G.addEdge(8, 9);
        // G.addEdge(5, 5);
        // G.addEdge(2, 3);
        // G.addEdge(3, 0);
        System.out.println(G);
    }

    @Test
    public void testDFS() {
        DirectedDFS d0 = new DirectedDFS(G, 0);
        DirectedDFS d2 = new DirectedDFS(G, 2);
        for (int i = 0; i < G.V(); ++i) {
            System.out.print(i + ":" + d0.marked(i) + " ");
        }
        System.out.println();
        for (int i = 0; i < G.V(); ++i) {
            System.out.print(i + ":" + d2.marked(i) + " ");
        }
        System.out.println();
        HashSet<Integer> s = new HashSet<>();
        s.add(2);
        s.add(3);
        DirectedDFS d23 = new DirectedDFS(G, s);
        for (int i = 0; i < G.V(); ++i) {
            System.out.print(i + ":" + d23.marked(i) + " ");
        }
    }

    @Test
    public void testBFS() {
        DirectedBFS b0 = new DirectedBFS(G, 0);
        DirectedBFS b2 = new DirectedBFS(G, 2);
        for (int i = 0; i < G.V(); ++i) {
            System.out.print(i + ":" + b0.marked(i) + " ");
        }
        System.out.println();
        for (int i = 0; i < G.V(); ++i) {
            System.out.print(i + ":" + b2.marked(i) + " ");
        }
        System.out.println();
        HashSet<Integer> s = new HashSet<>();
        s.add(2);
        s.add(3);
        DirectedBFS d23 = new DirectedBFS(G, s);
        for (int i = 0; i < G.V(); ++i) {
            System.out.print(i + ":" + d23.marked(i) + " ");
        }
    }

    @Test
    public void testCycle() {
        Cycle dc = new Cycle(G);
        if (dc.hasCycle())
            dc.cycle().forEach(x -> System.out.print(x + " "));
    }

    @Test
    public void testTopo() {
        Topological topo = new Topological(G);
        if (topo.isDAG()) {
            topo.order().forEach(x -> System.out.print(x + " "));
        }
    }

    @Test
    public void testSCC() {
        SCC scc = new SCC(G);
        System.out.println(scc.count());
        System.out.println(scc.stronglyConnected(1, 2));
        System.out.println(scc.stronglyConnected(1, 6));
    }
}