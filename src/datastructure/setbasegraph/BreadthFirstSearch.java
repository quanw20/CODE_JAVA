package datastructure.setbasegraph;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

public class BreadthFirstSearch<VertexType extends Comparable<VertexType>> {
    private Set<VertexType> marked;
    private Queue<VertexType> queue;
    private int cnt;

    public BreadthFirstSearch(Graph<VertexType> g, VertexType s) {
        marked = new HashSet<>();
        queue = new ArrayDeque<>();
        bfs(g, s);
    }

    private void bfs(Graph<VertexType> g, VertexType v) {
        queue.add(v);
        marked.add(v);
        ++cnt;
        while (!queue.isEmpty()) {
            VertexType w = queue.poll();
            if (!marked(w)) {
                queue.add(w);
                marked.add(w);
                ++cnt;
            }
        }

    }

    public boolean marked(VertexType v) {
        return marked.contains(v);
    }

    public int cnt() {
        return cnt;
    }
}