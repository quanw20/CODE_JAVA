package datastructure.setbasegraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * TwoCloor
 * 二分图-双色问题
 */
public class TwoCloor<VertexType extends Comparable<VertexType>> {
    private Set<VertexType> marked;
    private Map<VertexType, Boolean> color;
    private boolean isTwoColorable;

    public TwoCloor(Graph<VertexType> g) {
        marked = new HashSet<>();
        color = new HashMap<>();
        isTwoColorable = true;
        boolean first = true;
        for (VertexType v : g.getVertexs()) {
            if (first) {
                color.put(v, true);
            }
            if (!marked.contains(v))
                dfs(g, v);
        }
    }

    private void dfs(Graph<VertexType> g, VertexType v) {
        marked.add(v);
        for (VertexType u : g.adj(v)) {
            if (!marked.contains(u)) {
                color.put(u, !color.get(v));
                dfs(g, u);
            } else if (color.get(u).equals(color.get(v))) {
                isTwoColorable = false;
            }
        }
    }

    public boolean isBipartite() {
        return isTwoColorable;
    }

}