package datastructure.setbasegraph;

import java.util.Iterator;

class Search<VertexType extends Comparable<VertexType>> {
    private VertexType v;
    private Graph<VertexType> g;

    Search(Graph<VertexType> g, VertexType v) {
        this.v = v;
        this.g = g;
    }

    /**
     * 是否联通
     * 
     * @param w
     * @return
     */
    public boolean connected(VertexType w) {
        for (VertexType u : g.adj(v)) {
            if (u.equals(w))
                return true;
        }
        return false;
    }

    /**
     * 联通的顶点数
     * 
     * @return
     */
    public int count() {
        Iterator<VertexType> it = g.adj(v).iterator();
        int cnt = 0;
        while (it.hasNext()) {
            it.next();
            ++cnt;
        }
        return cnt;

    }
}