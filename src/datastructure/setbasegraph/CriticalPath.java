package datastructure.setbasegraph;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CriticalPath {
    /** 边 */
    static class Edge {
        /** 权重 */
        int weight;
        /** 出度指向的点 */
        int toVertex;
        Edge next;

        public Edge(int weight, int toVertex, Edge next) {
            this.weight = weight;
            this.toVertex = toVertex;
            this.next = next;
        }
    }

    /** 顶点 */
    static class Vertex {
        /** 入度 数量 */
        int inNumber;
        /** 顶点信息 */
        Integer data;
        /** 第一条边 */
        Edge firstEdge;

        public Vertex(int inNumber, Integer data, Edge firstEdge) {
            this.inNumber = inNumber;
            this.data = data;
            this.firstEdge = firstEdge;
        }
    }

    static void criticalPath(List<Vertex> graph) {
        // 顶点数量
        int length = graph.size();
        // 边数量
        int numOfEdges = 0;
        for (Vertex vertex : graph) {
            Edge edge = vertex.firstEdge;
            while (edge != null) {
                numOfEdges++;
                edge = edge.next;
            }
        }
        // 事件最早发生时间
        int[] ve = new int[length];
        // 事件最晚发生时间
        int[] vl = new int[length];
        // 活动最早发生时间
        int[] ee = new int[numOfEdges];
        // 活动最晚发生时间
        int[] el = new int[numOfEdges];
        // 1. 通过拓扑排序求 etv 「事件最早发生时间」
        // etvStack 用于储存拓扑排序后的顺序
        Stack<Vertex> etvStack = new Stack<>();
        // stack 用于拓扑排序
        Stack<Vertex> stack = new Stack<>();
        for (Vertex vertex : graph) {
            if (vertex.inNumber == 0) {
                stack.push(vertex);
            }
        }
        while (!stack.isEmpty()) {
            Vertex pop = stack.pop();
            // 储存拓扑排序后的结构
            etvStack.push(pop);
            // 遍历出度
            Edge edge = pop.firstEdge;
            while (edge != null) {
                Vertex vertex = graph.get(edge.toVertex);
                vertex.inNumber--;
                if (vertex.inNumber == 0) {
                    stack.push(vertex);
                }
                // 赋值更大的距离给 etv
                if (ve[pop.data] + edge.weight > ve[edge.toVertex]) {
                    ve[edge.toVertex] = ve[pop.data] + edge.weight;
                }
                edge = edge.next;
            }
        }
        // 2.通过 etv 反向推导求出 ltv「事件最晚发生时间」
        System.out.println("====etv====");
        for (int i = 0; i < ve.length; i++) {
            System.out.print("V" + i + " = " + ve[i] + " ");
        }
        System.out.println();

        // 初始化 ltv
        Integer endVertex = etvStack.peek().data;
        for (int i = 0; i < vl.length; i++) {
            vl[i] = ve[endVertex];
        }
        while (!etvStack.isEmpty()) {
            Vertex pop = etvStack.pop();
            Edge edge = pop.firstEdge;
            while (edge != null) {
                // 赋值更小的距离给 ltv
                if (vl[pop.data] > vl[edge.toVertex] - edge.weight) {
                    vl[pop.data] = vl[edge.toVertex] - edge.weight;
                }
                edge = edge.next;
            }
        }
        System.out.println("====ltv====");
        for (int i = 0; i < vl.length; i++) {
            System.out.print("V" + i + " = " + vl[i] + " ");
        }
        System.out.println();
        // 3. 通过 etv 求 ete
        int index = 0;
        for (Vertex vertex : graph) {
            Edge edge = vertex.firstEdge;
            while (edge != null) {
                ee[index++] = ve[vertex.data];
                edge = edge.next;
            }
        }
        System.out.println("====ete====");
        for (int i = 0; i < ee.length; i++) {
            System.out.print("E" + i + " = " + ee[i] + " ");
        }
        System.out.println();
        // 4. 通过 ltv 求 lte
        index = 0;
        for (Vertex vertex : graph) {
            Edge edge = vertex.firstEdge;
            while (edge != null) {
                el[index++] = vl[edge.toVertex] - edge.weight;
                edge = edge.next;
            }
        }
        System.out.println("====lte====");
        for (int i = 0; i < el.length; i++) {
            System.out.print("E" + i + " = " + el[i] + " ");
        }
        System.out.println();
        // 5. 用 lte - ete 求关键路径
        System.out.println("====关键路径====");
        for (int i = 0; i < ee.length; i++) {
            if (el[i] - ee[i] == 0) {
                System.out.print("E" + i + " ");
            }
        }
        return;
    }

    /** 测试 */
    public static void main(String[] args) {
        char[] vertices = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };
        Edge e3 = new Edge(2, 4, null);
        Edge e2 = new Edge(1, 3, e3);
        Edge e1 = new Edge(3, 2, e2);
        Edge e0 = new Edge(2, 1, e1);
        Edge e4 = new Edge(1, 5, null);
        Edge e5 = new Edge(1, 5, null);
        Edge e6 = new Edge(1, 5, null);
        Edge e7 = new Edge(1, 5, null);
        Edge e8 = new Edge(2, 6, null);
        Vertex a = new Vertex(0, 0, e0);
        Vertex b = new Vertex(1, 1, e4);
        Vertex c = new Vertex(1, 2, e5);
        Vertex d = new Vertex(1, 3, e6);
        Vertex e = new Vertex(1, 4, e7);
        Vertex f = new Vertex(4, 5, e8);
        Vertex g = new Vertex(1, 6, null);
        ArrayList<Vertex> graph = new ArrayList<>();
        graph.add(a);
        graph.add(b);
        graph.add(c);
        graph.add(d);
        graph.add(e);
        graph.add(f);
        graph.add(g);
        criticalPath(graph);
    }
}
