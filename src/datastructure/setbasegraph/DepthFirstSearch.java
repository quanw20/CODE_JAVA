package datastructure.setbasegraph;

import java.util.HashSet;
import java.util.Set;

public
/**
 * 标记所有联通的节点
 * 
 */
class DepthFirstSearch<VertexType extends Comparable<VertexType>> {
    private Set<VertexType> marked;
    private int cnt;

    public DepthFirstSearch(Graph<VertexType> g, VertexType s) {
        marked = new HashSet<>();
        dfs(g, s);
    }

    private void dfs(Graph<VertexType> g, VertexType v) {
        marked.add(v);
        ++cnt;
        for (VertexType w : g.adj(v)) {
            if (!marked(w))
                dfs(g, w);
        }
    }

    public boolean marked(VertexType v) {
        return marked.contains(v);
    }

    public int cnt() {
        return cnt;
    }

}
