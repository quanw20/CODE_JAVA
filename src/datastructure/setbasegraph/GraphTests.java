package datastructure.setbasegraph;

import org.junit.jupiter.api.*;

public class GraphTests {
    static Graph<String> g;

    @BeforeAll
    static void before() {
        System.out.println("init...");
        g = new Graph<>();
        g.addVertex("唐权威");
        g.addVertex("唐1");
        g.addVertex("唐2");
        g.addVertex("唐3");
        g.addVertex("唐4");
        g.addVertex("唐5");
        g.addVertex("唐6");
        g.addVertex("唐7");
        g.addVertex("唐8");
        g.addVertex("唐9");

        g.addEdge("唐权威", "唐1");
        g.addEdge("唐权威", "唐4");
        g.addEdge("唐1", "唐2");
        g.addEdge("唐2", "唐3");
        g.addEdge("唐3", "唐4");
        g.addEdge("唐1", "唐4");
        g.addEdge("唐1", "唐5");

        g.addEdge("唐6", "唐7");
        g.addEdge("唐7", "唐8");
        g.addEdge("唐6", "唐8");
        g.addEdge("唐8", "唐9");
        System.out.println("init done");
    }

    // @AfterEach
    // public void attr() {
    // System.out.println("\n节点数: " + g.V() + ", 边数: " + g.E());
    // System.out.println(g);
    // }
    @Test
    public void test() {
        Iterable<String> adj = g.adj("唐权威");
        adj.forEach((x) -> System.out.print(x + " "));
    }

    @Test
    public void testSearch() {
        Search<String> search = new Search<>(g, "唐权威");
        System.out.println(search.connected("唐1"));
        System.out.println(search.connected("唐5"));
        System.out.println(search.count());
    }

    @Test
    public void testDFS() {
        DepthFirstSearch<String> dfs = new DepthFirstSearch<>(g, "唐权威");
        System.out.println("dfs.cnt() " + dfs.cnt());
        System.out.println("dfs.marked() " + dfs.marked("唐9"));

        DepthFirstSearch<String> dfs1 = new DepthFirstSearch<>(g, "唐9");
        System.out.println("dfs.cnt() " + dfs1.cnt());
        System.out.println("dfs.marked() " + dfs1.marked("唐6"));
    }

    @Test
    public void testPath() {
        Paths<String> path = new Paths<>(g, "唐权威");
        for (String s : g.getVertexs()) {
            if (!s.equals("唐权威")) {
                Iterable<String> pathTo = path.pathTo(s);
                if (pathTo != null) {
                    pathTo.forEach(x -> System.out.print(x + " "));
                    System.out.println();
                }
            }
        }
    }

    @Test
    public void testCycle() {
        Cycle<String> cycle = new Cycle<>(g);
        System.out.println(cycle.hasCycle());
        if (cycle.hasCycle()) {
            Iterable<String> cyclePath = cycle.getCyclePath();
            cyclePath.forEach(x -> System.out.print(x + " "));
        }
    }

    @Test
    public void testTwoColor() {
        TwoCloor<String> twoCloor = new TwoCloor<>(g);
        boolean bipartite = twoCloor.isBipartite();
        System.out.println(bipartite);
    }

    @Test
    public void testCC() {
        ConnectedComponent<String> cc = new ConnectedComponent<>(g);
        System.out.println(cc.count());
        System.out.println(cc.connected("唐权威", "唐1"));
    }
}
