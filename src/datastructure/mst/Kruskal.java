package datastructure.mst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Kruskal算法
 */
public class Kruskal {
    static class UF {
        int[] p;

        UF(int n) {
            p = new int[n];
            for (int i = 1; i < n; ++i)
                p[i] = i;
        }

        int find(int u) {
            while (p[u] != u)
                u = p[u] = p[p[u]];
            return u;
        }

        void union(int u, int v) {
            p[find(u)] = find(v);
        }

        boolean connected(int u, int v) {
            return find(u) == find(v);
        }
    }

    /**
     * 
     * @param edges []{u,v,w}
     * @return
     */
    int kruskal(int[][] edges) {
        int w = 0;
        PriorityQueue<int[]> pq = new PriorityQueue<>((x, y) -> x[2] - y[2]);
        for (int i = 0; i < edges.length; ++i)
            pq.add(edges[i]);
        UF uf = new UF(edges.length + 1);// 边数至少为n-1
        // List<int[]> ret = new ArrayList<>();

        while (!pq.isEmpty()) {
            int[] e = pq.poll();
            if (uf.connected(e[0], e[1]))
                continue;
            uf.union(e[0], e[1]);
            w += e[2];
            // ret.add(e);
        }
        // ret.forEach(x -> System.out.println(x[0] + " " + x[1] + " " + x[2]));

        return w;
    }

    @SuppressWarnings("all")
    public static void main(String[] args) {
        List<int[]>[] g = new List[] {
                List.of(new int[] { 1, 1 }),
                List.of(new int[] { 2, 12 }),
                List.of(new int[] { 3, 23 }),
                List.of(new int[] { 4, 34 }, new int[] { 5, 35 }),
                List.of(new int[] { 5, 45 }),
                List.of(new int[] { 0, 50 })
        };
        // adjlist->edge[][]
        List<int[]> e = new ArrayList<>();
        for (int i = 0, l = g.length; i < l; ++i) {
            for (int[] a : g[i])
                e.add(new int[] { i, a[0], a[1] });
        }
        int[][] array = e.toArray(new int[e.size()][]);
        int kruskal = new Kruskal().kruskal(array);
        System.out.println(kruskal);
    }
}
