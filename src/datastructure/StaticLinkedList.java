package datastructure;

import java.io.BufferedInputStream;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Node
 * 静态链表的节点
 */
class Node implements Comparable<Node> {
    int addr;// 节点地址
    int next;// 指针域
    int data;// 数据域 (int)char
    int prop;// 节点的某种性质,不同题目可能不一样

    // 按小于的顺序
    @Override
    public int compareTo(Node o) {
        if (this.prop == -1 || o.prop == -1) {
            return o.prop - prop;
        } else {
            return data - o.data;
        }
    }
}

/**
 * StaticLinkedList
 */
public class StaticLinkedList {
    public static final int N = 100001;
    @SuppressWarnings("all")
    static Node[] list = new Node[N];
    static {
        // 初始化属性
        for (int i = 0; i < N; i++) {
            list[i] = new Node();
            list[i].data = (1 << 31) - 1;
            list[i].prop = -1;
        }

    }

    /**
     * 静态链表排序
     * 
     * 输入:
     * 5 00001
     * 11111 100 -1
     * 00001 0 22222
     * 33333 100000 11111
     * 12345 -1 33333
     * 22222 1000 12345
     * 
     * 输出:
     * 5 00001
     * 11111 100 -1
     * 00001 0 22222
     * 33333 100000 11111
     * 12345 -1 33333
     * 22222 1000 12345
     * 
     * @param args
     */
    public static void main(String[] args) {
        // 输入
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        int begin = in.nextInt();
        for (int i = 0; i < n; i++) {
            int addr = in.nextInt();
            list[addr].addr = addr;
            list[addr].data = in.nextInt();
            list[addr].next = in.nextInt();
        }
        in.close();
        int cnt = 0;
        // 遍历
        // -1代表结尾
        for (int i = begin; i != -1; i = list[i].next) {
            System.out.println(list[i].data);
            list[i].prop = 1;//
            cnt++;
        }
        Arrays.sort(list);
        System.out.println();

        for (int i = 0; i < cnt - 1; i++) {
            System.out.println(list[i].addr + " " + list[i].data + " " + list[i + 1].addr);
        }
        System.out.println(list[cnt - 1].addr + " " + list[cnt - 1].data + " -1");
    }
}

