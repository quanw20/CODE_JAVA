package datastructure;

import java.util.*;

import org.junit.jupiter.api.*;

public class CollectionDemo {
    /**
     * Class Stack<E> extends Vector<E>
     * 
     * 
     * public E push(E item)<br/>
     * public E pop()<br/>
     * public E peek()<br/>
     * public boolean empty()<br/>
     * public int search(Object o) 以1为基础<br/>
     */
    @Test
    public void testStack() {
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        System.out.println(stack.search(1));// 4
        System.out.println(stack.search(2));// 3
    }

    /**
     * interface Queue<E>
     * 
     * boolean offer(E e) 添加元素<br/>
     * E poll() 删除元素<br/>
     * E peek() 返回队首元素<br/>
     */
    @Test
    public void testQueue() {
        Queue<Integer> q = new LinkedList<>();
        q.offer(1);
        q.offer(2);
        q.offer(3);
        q.offer(4);

        System.out.println(q.peek());
        while (!q.isEmpty())
            System.out.println(q.poll());
        System.out.println(q.poll());

    }

    /**
     * Class PriorityQueue<E>
     * boolean offer(E e) 添加元素<br/>
     * E poll() 删除元素<br/>
     * E peek() 返回队首元素<br/>
     * 
     * DEFAULT_INITIAL_CAPACITY = 11;
     * 
     */
    @Test
    public  void testPriorityQueue() {
        // 小顶堆
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        pq.offer(452);
        pq.offer(3);
        pq.offer(2352);
        pq.offer(4);
        pq.offer(34);
        pq.offer(2);
        System.out.println(pq.peek());
        while (!pq.isEmpty())
            System.out.println(pq.poll());

        // 大顶堆
        // PriorityQueue<Integer> pq1 = new PriorityQueue<>(new Comparator<Integer>() {
        // @Override
        // public int compare(Integer o1, Integer o2) {
        // return o2 - o1;
        // }
        // });
        System.out.println();
        PriorityQueue<Integer> pq1 = new PriorityQueue<>((a, b) -> b - a);
        // var p=new PriorityQueue<Integer>((a,b)->b-a);
        pq1.offer(452);
        pq1.offer(3);
        pq1.offer(2352);
        pq1.offer(4);
        pq1.offer(34);
        pq1.offer(2);
        System.out.println(pq1.peek());
        while (!pq1.isEmpty())
            System.out.println(pq1.poll());
    }

    @SuppressWarnings("unchecked")
    public <T> void test_array(T a) {
        Class<? extends Object> class1 = a.getClass();
        System.out.println(class1);

        T[] ta;
        // ta = (T[]) Array.newInstance(a.getClass(), 10);
        ta = (T[]) new Object[10];
        System.out.println(ta);
        Arrays.fill(ta, a);
        System.out.println(Arrays.toString(ta));
    }

    @Test
    public void testArray() {
        test_array("ads");
    }

    @Test
    public void test_Map() {
        HashMap<String, Integer> map = new HashMap<>();
        // 更新值
        Integer cnt = map.merge("key", 1, Integer::sum);
        System.out.println(cnt);
        System.out.println(map);

        map.put("key", map.getOrDefault("key", 0) + 1);
        System.out.println(map);

        // 如果不在则添加
        map.putIfAbsent("q", 0);
        map.put("q", map.get("q") + 1);// 更新
        System.out.println(map);

        // 如果key不存在或key->value==null
        Integer compute = map.compute("key1", (k, v) -> (v == null) ? 123 : v + 123);
        System.out.println(compute);
        System.out.println(map);

        // 如果key不存在就做后面的操作
        Integer c1 = map.computeIfAbsent("key2", (k) -> (k.length() > 6) ? 123 : (123 + 123));
        System.out.println(c1);
        System.out.println(map);

        // 如果key存在就做后面的操作
        Integer c2 = map.computeIfPresent("key2", (k, v) -> (v == null) ? 123 : v + 123);
        System.out.println(c2);
        System.out.println(map);

        // replaceAll
        map.replaceAll((k, v) -> (k.length() <= 3) ? 12 : v + 1);
        System.out.println(map);
    }

    @Test
    public void test_Set() {
        HashSet<Integer> s1 = new HashSet<>();
        s1.add(1);
        s1.add(2);
        s1.add(3);
        s1.add(4);
        HashSet<Integer> s2 = new HashSet<>(s1);
        s2.add(5);
        s2.add(6);
        s2.remove(3);
        // 交 unite
        HashSet<Integer> unite = new HashSet<>(s2);
        unite.retainAll(s1);
        System.out.println(s2 + " 交 " + s1 + " = " + unite);
        // 并 merge
        HashSet<Integer> merge = new HashSet<>(s2);
        merge.addAll(s1);
        System.out.println(s2 + " 并 " + s1 + " = " + merge);
        // 补 complementary
        HashSet<Integer> comp = new HashSet<>(s1);
        comp.removeAll(unite);
        System.out.println(unite + " 对 " + s1 + " 的补集 = " + comp);
        // 差集 difference set
        HashSet<Integer> diff = new HashSet<>(s2);
        diff.removeAll(s1);
        System.out.println(s2 + " - " + s1 + " = " + diff);
        // 对称差 symmetric difference (Boolean sum)
        HashSet<Integer> symm = new HashSet<>(merge);
        symm.removeAll(unite);
        System.out.println(s2 + " ^ " + s1 + " = " + symm);
    }

    @Test
    public void test_EnumSet() {
        enum WeekDay {
            A, B, C, D, E, F, G
        }
        EnumSet<WeekDay> allOf = EnumSet.allOf(WeekDay.class);
        System.out.println(allOf);
        EnumSet<WeekDay> noneOf = EnumSet.noneOf(WeekDay.class);
        System.out.println(noneOf);
        EnumSet<WeekDay> range = EnumSet.range(WeekDay.B, WeekDay.F);
        System.out.println(range);
        EnumSet<WeekDay> of = EnumSet.of(WeekDay.A, WeekDay.B, WeekDay.F);
        System.out.println(of);
    }

    @Test
    public void test_nCopys() {
        List<String> nCopies = Collections.nCopies(10, "DEFAULT");
        System.out.println(nCopies);
    }

    /**
     * 小集合
     */
    @Test
    public void test_of() {
        List<String> l1 = List.of("Tang", "Quanwei");// 长度不可变,元素不可修改
        System.out.println(l1);
        List<String> l2 = Arrays.asList("Tang", "Quawnei");// 长度不可变,元素可更改
        System.out.println(l2);
        // l2.add("dasd");//java.lang.UnsupportedOperationException
        // l1.set(1,"asd");//java.lang.UnsupportedOperationException
        l2.set(1, "asd");
        System.out.println(l2);

        Map<String, Integer> m1 = Map.of("Tang", 1, "Quanwei", 2);
        System.out.println(m1);
        Map<Integer, String> m2 = Map.ofEntries(
                Map.entry(1, "Tang"),
                Map.entry(2, "Quanwei"));
        System.out.println(m2);
    }

    /**
     * 子范围
     */
    @Test
    public void test_sub() {
        TreeSet<Integer> ts = new TreeSet<>();
        for (int i = 0; i < 10; i++) {
            ts.add(i);
        }
        SortedSet<Integer> subSet = ts.subSet(2, 6);
        System.out.println(subSet);
        SortedSet<Integer> headSet = ts.headSet(5);
        System.out.println(headSet);
        SortedSet<Integer> tailSet = ts.tailSet(5);
        System.out.println(tailSet);
    }

    @Test
    public void test_unmodifiable() {
        List<Integer> ul1 = Collections.unmodifiableList(List.of(1, 2, 3, 2, 345, 23, 45123, 531, 4));
        int frequency = Collections.frequency(ul1, 2);
        System.out.println(frequency);

    }

    @Test
    public void test_sync() {
        List<Integer> sl1 = Collections.synchronizedList(List.of(1, 2, 3, 4));
        System.out.println(sl1);
    }

    @Test
    public void test_check() {
        // 类型检测
        List<Integer> checkedList = Collections.checkedList(List.of(1, 2), Integer.class);
        // List cl=checkedList;
        // cl.add("asd");// 出错
        System.out.println(checkedList);
    }

    @Test
    public void test_convery() {
        int[] a = { 1, 2, 3, 4, 5, 6, 7, 8 };
        String str = Arrays.toString(a);
        String[] s = str.substring(1, str.length() - 1).split(", ");
        // 数组转列表
        List<String> l1 = List.of(s);// String... == String[]
        System.out.println(l1);
        for (String string : l1) {
            System.out.print(string + "->");
        }
        System.out.println(l1.size());
        LinkedList<Object> l2 = l1.stream().mapToInt(Integer::valueOf).collect(LinkedList::new, LinkedList::add,
                (x, y) -> x.add(y));
        // 列表转数组
        // l1.toArray(new int[l2.size()]);//! not applicable for the arguments (int[])
        // Integer[] arr = (Integer[]) l2.toArray();//! Object; cannot be cast to
        // Integer
        // ! can not cast one of the elements of java.lang.Object[] to the type of the
        // destination array,
        // Integer[] array = l1.toArray(new Integer[l2.size()]);
        Integer[] array = l2.toArray(new Integer[l2.size()]);
        System.out.println(Arrays.toString(array));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_Array() {
        class Pair<K, V> {
            K k;
            V v;
        }
        Pair<Integer, Integer>[] ps = (Pair<Integer, Integer>[]) new Pair<?, ?>[10];
        ps[0] = new Pair<>();
        ps[0].k = 123;
        ps[0].v = 123123;

        System.out.println(ps[0].k + " " + ps[0].v);
    }
}
