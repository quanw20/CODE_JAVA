package datastructure.graph;

import java.util.ArrayList;

/**
 * Node
 */
public class Node {
  public int value;
  public int in;// 入度
  public int out;// 出度
  public ArrayList<Node> nexts;// 邻接点
  public ArrayList<Edge> edges;// 邻接边

  public Node(int value) {
    this.value = value;
    in = out = 0;
    nexts = new ArrayList<>();
    edges = new ArrayList<>();
  }

  @Override
  public String toString() {
    return "(%d, %d, %d)".formatted(value, in, out);
  }
}