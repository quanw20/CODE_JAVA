package datastructure.graph;

public class Edge {
  public int from;
  public int to;
  public int weight;

  public Edge(int from, int to, int weight) {
    this.from = from;
    this.to = to;
    this.weight = weight;
  }

  @Override
  public String toString() {
  return "(%d, %d, %d)".formatted(from,to,weight);
  }

}
