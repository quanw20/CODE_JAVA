package datastructure.sp;

/**
 * 
 */
public class Floyd {
    /**
     * 路径矩阵
     */
    public static int[][] path;

    public static void floyd(int[][] graph) {
        int V = graph.length;
        // 初始化路径
        path = new int[V][V];
        for (int i = 0; i < V; i++)
            for (int j = 0; j < V; j++)
                path[i][j] = j;

        for (int i = 0; i < V; i++) // 开始 Floyd 算法 每个点为中转
            for (int j = 0; j < V; j++) // 所有入度
                for (int k = 0; k < V; k++) // 所有出度
                    if (graph[j][i] != -1 && graph[i][k] != -1) // 以每个点为「中转」，刷新所有出度和入度之间的距离;
                        if (graph[j][i] + graph[i][k] < graph[j][k] || graph[j][k] == -1) {
                            graph[j][k] = graph[j][i] + graph[i][k];// 刷新距离
                            path[j][k] = i;// 刷新路径
                        }
    }

    /**
     * 测试
     */
    public static void main(String[] args) {
        int[][] graph = new int[][] {
                { 0, 2, -1, 6 }, { 2, 0, 3, 2 }, { -1, 3, 0, 2 }, { 6, 2, 2, 0 } };
        floyd(graph);
        System.out.println("====dist====");
        for (int[] ints : graph) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
        System.out.println("====path====");
        for (int[] ints : path) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }
}
