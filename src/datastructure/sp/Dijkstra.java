package datastructure.sp;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * @Description: 使用pq
 * @ClassName: Dijkstra
 * @Author: QUANWEI
 * @Date: 2022/2/17 21:31
 * @Version: 1.0
 */
public class Dijkstra {
    /**
     * 
     * @param graph 图
     * @param src   源点
     * @return 源点到各店的距离数组
     */
    public static int[] dijkstra(int[][] graph, int src) {
        // init dist visited pq
        int V = graph.length;
        int[] dist = new int[V];
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[src] = 0;
        // visited
        boolean[] visited = new boolean[V];
        // pq
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> dist[a] - dist[b]);
        pq.add(src);
        // 填充dist
        for (int i = 0; i < V - 1; i++) {// 更新v-1个节点
            int u = pq.poll();
            visited[u] = true;
            for (int v = 0; v < V; v++)
                // 没访问过,存在边,s->u->v < s->v => 更新& 添加新d[v]到pq
                if (!visited[v] && graph[u][v] != 0 && dist[v] > dist[u] + graph[u][v]) {
                    dist[v] = dist[u] + graph[u][v];
                    pq.add(v);
                }
        }
        return dist;
    }

    public static void main(String[] args) {
        int graph[][] = new int[][] { { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
                { 4, 0, 8, 0, 0, 0, 0, 11, 0 },
                { 0, 8, 0, 7, 0, 4, 0, 0, 2 },
                { 0, 0, 7, 0, 9, 14, 0, 0, 0 },
                { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
                { 0, 0, 4, 14, 10, 0, 2, 0, 0 },
                { 0, 0, 0, 0, 0, 2, 0, 1, 6 },
                { 8, 11, 0, 0, 0, 0, 1, 0, 7 },
                { 0, 0, 2, 0, 0, 0, 6, 7, 0 } };
        int[] dijkstra = dijkstra(graph, 0);
        System.out.println(Arrays.toString(dijkstra));
        

    }

}
