package datastructure.tree;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

// * 写递归算法的关键是要明确函数的「定义」是什么, 然后相信这个定义, 利用这个定义推导最终结果, 绝不要跳入递归的细节
// * 写树相关的算法, 简单说就是, 先搞清楚当前 root 节点「该做什么」以及「什么时候做」, 然后根据函数定义递归调用子节点

/**
 * BT BinaryTree
 * 
 * <pre>
 * 1. 二叉树
 * 
 * 2. 完全二叉树 n 个结点的完全二叉树的深度为 ⌊log2n⌋+1
 *      当 i>1 时, 父亲结点为结点 [i/2].（i=1 时, 表示的是根结点, 无父亲结点）
 *      如果 2*i>n（总结点的个数） , 则结点 i 肯定没有左孩子（为叶子结点）; 否则其左孩子是结点 2*i 
 *      如果 2*i+1>n , 则结点 i 肯定没有右孩子; 否则右孩子是结点 2*i+1 
 * 
 * 3. 满二叉树
 *      满二叉树中第 i 层的节点数为 2^(n-1) 个
 *      深度为 k 的满二叉树必有 2^k-1 个节点 , 叶子数为 2^(k-1)
 *      满二叉树中不存在度为 1 的节点, 每一个分支点中都两棵深度相同的子树, 且叶子节点都在最底层
 *      具有 n 个节点的满二叉树的深度为 log2(n+1)
 * 
 * 性质:
 * 节点数: n
 * 度数: d
 * 边数: e = n-1
 * 高度: h = log(n) 向下取整,完全二叉树
 * 第i层有n=2^i个节点
 * 高度为h:至少有: n=2^(h+1)-1
 * 
 * 
 * 问题:
 * 2.判断
 *      isBalance
 *      isAVLTree
 *      isRBTree
 * </pre>
 * 
 */
public class BinaryTree {
    private BinaryTree() {
    }

    /**
     * *先要搞清楚 root 节点它自己要做什么
     * 
     * *然后根据题目要求选择使用前序, 中序, 后续的递归框架
     * 
     * @param root
     */
    @SuppressWarnings("all")
    private void traverse(TreeNode root) {
        // *base case
        if (root == null)
            return;
        // *preorder traversal
        traverse(root.left);
        // *inorder traversal
        traverse(root.right);
        // *postorder traversal
    }

    /**
     * 计算以root为根的节点个数
     * 
     * @param root 树根
     * @return 以root为根的节点个数
     */
    public static int count(TreeNode root) {
        if (root == null)
            return 0;
        return 1 + count(root.left) + count(root.right);
    }

    /**
     * 将整棵树节点反转
     * 
     * @param root 根
     * @return 反转后的根
     */
    public static TreeNode invertTreee(TreeNode root) {
        if (root == null)
            return null;
        // 反转自己
        TreeNode tmp = root.left;
        root.left = root.right;
        root.right = tmp;

        // 反转左右
        invertTreee(root.left);
        invertTreee(root.right);
        return root;
    }

    /**
     * 定义：将以 root 为根的树拉平为链表
     */
    public static void flatten(TreeNode root) {
        // base case
        if (root == null)
            return;

        // recursive
        flatten(root.left);
        flatten(root.right);

        // 1. 左右子树已经被拉成一条链表
        TreeNode left = root.left;
        TreeNode right = root.right;

        // 2. 将左子树作为右子树
        root.left = null;
        root.right = left;

        // 3. 原右子树接到现右子树末端
        TreeNode p = root;
        while (p.right != null)
            p = p.right;
        p.right = right;

    }

    static int[] pre;// 前序遍历的数组
    static int[] in;// 中序遍历的数组
    static int[] post;// 后续编列的数组

    /**
     * 由中序遍历和后续遍历建树
     * 
     * @param pl post数组左边的下标
     * @param pr post数组右边的下标
     * @param il in数组左边的下标
     * @param ir in数组右边的下标
     * @return 原树的根节点
     */
    static TreeNode createFromInAndPost(int pl, int pr, int il, int ir) {
        if (pl > pr)
            return null;
        TreeNode root = new TreeNode();
        root.val = post[pr];
        int k = 0;
        for (k = il; k <= ir; k++) {
            if (in[k] == post[pr]) {
                break;
            }
        }
        int numLeft = k - il;
        root.left = createFromInAndPost(pl, pl + numLeft - 1, il, k - 1);
        root.right = createFromInAndPost(pl + numLeft, pr - 1, k + 1, ir);
        return root;
    }

    /**
     * 由中序遍历和前续遍历建树
     * 
     * @param pl pre数组左边的下标
     * @param pr pre数组右边的下标
     * @param il in数组左边的下标
     * @param ir in数组右边的下标
     * @return 原树的根节点
     */
    static TreeNode createFromInAndPre(int pl, int pr, int il, int ir) {
        if (pl > pr)
            return null;
        TreeNode root = new TreeNode();
        root.val = pre[pl];
        int k = 0;
        for (k = il; k <= ir && in[k] != pre[pl]; k++) {
        }
        int numLeft = k - il;
        root.left = createFromInAndPre(pl + 1, pl + numLeft, il, k - 1);
        root.right = createFromInAndPre(pl + numLeft + 1, pr, k + 1, ir);
        return root;
    }

    /**
     * 用有序数组构建高度最低的二叉树
     * 
     * @param a 有序数组
     * @return 二叉树
     */
    public static TreeNode bstWithMinHight(int[] a) {
        return create(a, 0, a.length - 1);
    }

    private static TreeNode create(int[] a, int i, int j) {
        if (i > j)
            return null;
        int mid = i + ((j - i) >> 1);
        TreeNode node = new TreeNode(a[mid]);
        node.left = create(a, i, mid - 1);
        node.right = create(a, mid + 1, j);
        return node;
    }

    /**
     * 判断是否是否平衡
     * 
     * @param root 根
     * @return 是否平衡
     */
    public static boolean isBalanced(TreeNode root) {
        return isBalanced(root, new HashMap<TreeNode, Integer>());
    }

    /**
     * 判断是否平衡
     * 
     * @param root 根
     * @param map  记录节点的高度
     * @return 是否平衡
     */
    private static boolean isBalanced(TreeNode root, HashMap<TreeNode, Integer> map) {
        if (root == null || (root.left == null && root.right == null))
            return true;
        boolean b = Math.abs(hight(root.left, map) - hight(root.right, map)) <= 1;// 两节点高度之差不超过一
        return isBalanced(root.left, map) && isBalanced(root.right, map) && b;
    }

    /**
     * 计算某个节点的高度
     * 
     * @param node 某个节点
     * @param map  记录节点高度的map
     * @return 某个节点的高度
     */
    private static int hight(TreeNode node, HashMap<TreeNode, Integer> map) {
        if (node == null)
            return 0;
        Integer integer = map.get(node);
        if (integer != null)
            return integer;
        map.put(node, 1 + Math.max(hight(node.left, map), hight(node.right, map)));
        return map.get(node);
    }

    // public static boolean isBalanced2(TreeNode root) {
    // if (root == null || (root.left == null && root.right == null))
    // return true;
    // boolean b = Math.abs(hight(root.left) - hight(root.right)) <= 1;//
    // 两节点高度之差不超过一
    // return isBalanced(root.left) && isBalanced(root.right) && b;
    // }

    // static ConcurrentHashMap<TreeNode, Integer> concurrentHashMap = new
    // ConcurrentHashMap<>();

    // private static int hight(TreeNode node) {
    // if (node == null)
    // return 0;
    // return concurrentHashMap.computeIfAbsent(node, x -> 1 +
    // Math.max(hight(x.left), hight(x.right)));
    // }

    /**
     * 打印depth层的元素
     * 
     * @param root  根
     * @param depth 第几层
     */
    public static void printLayar(TreeNode root, int depth) {
        Queue<Pair<TreeNode, Integer>> q = new LinkedList<>();
        q.offer(new Pair<TreeNode, Integer>(root, 1));
        while (!q.isEmpty()) {
            Pair<TreeNode, Integer> poll = q.poll();
            if (poll.val == depth) {
                System.out.print(poll.key.val + " ");
            }
            if (poll.key.left != null) {
                q.offer(new Pair<TreeNode, Integer>(poll.key.left, poll.val + 1));
            }
            if (poll.key.right != null) {
                q.offer(new Pair<TreeNode, Integer>(poll.key.right, poll.val + 1));
            }
        }

    }

    public static void printLayar2(TreeNode root, Integer depth) {
        Queue<Object[]> q = new ArrayDeque<>();
        q.offer(new Object[] { root, 1 });
        while (!q.isEmpty()) {
            Object[] poll = q.poll();
            TreeNode node = (TreeNode) poll[0];
            Integer dep = (Integer) poll[1];
            if (depth.equals(dep)) {
                System.out.print(node.val + " ");
            }
            if (node.left != null) {
                q.offer(new Object[] { node.left, dep + 1 });
            }
            if (node.right != null) {
                q.offer(new Object[] { node.right, dep + 1 });
            }
        }

    }

    /**
     * 前一个元素的值
     */
    static long preVal = Long.MIN_VALUE;

    /**
     * 按中序遍历的顺序,看前一个是不是小于后一个
     * 
     * @param root 根
     * @return 是否是二叉树
     */
    public static boolean isBST(TreeNode root) {
        if (root == null)
            return true;
        boolean dfs = isBST(root.left);
        if (!dfs)
            return false;
        if (preVal >= root.val)
            return false;
        preVal = root.val;
        return isBST(root.right);
    }

    /**
     * 后继节点的值
     * 
     * 使用非递归中序遍历
     * 找到值等于val -> 标记
     * 找到下一个值 -> 检查标记 -> true?返回
     * 
     * @param root 根
     * @param val  查找的值
     * @return 找到返回{@code 后继},没找到返回{@code null}
     */
    public static Integer successor(TreeNode root, int val) {
        if (root == null)
            return null;
        Stack<TreeNode> s = new Stack<>();
        TreeNode cur = root;
        boolean isFound = false;
        while (cur != null || !s.isEmpty()) {
            while (cur != null) {
                s.push(cur);
                cur = cur.left;
            }
            TreeNode p = s.pop();
            if (isFound)
                return p.val;
            if (p.val == val)
                isFound = true;
            if (p.right != null)
                cur = p.right;
        }
        return null;
    }

    /**
     * 找到值的前驱
     * 
     * 使用非递归中序遍历
     * pre=null;
     * 找->找到了->reurn pre;
     * 没找到->更新pre
     * 
     * @param root 根
     * @param val  值
     * @return 找到了{@code 前驱}或者, 没找到{@code null}
     */
    public static Integer predecessor(TreeNode root, int val) {
        if (root == null)
            return null;
        Stack<TreeNode> s = new Stack<>();
        TreeNode cur = root;
        Integer pre = null;
        while (cur != null || !s.isEmpty()) {
            while (cur != null) {
                s.push(cur);
                cur = cur.left;
            }
            TreeNode p = s.pop();
            if (p.val == val) // 找到了
                return pre;
            pre = p.val;
            if (p.right != null)
                cur = p.right;
        }
        return null;
    }

    /**
     * 最近公共祖先(Lowest Common Ancestor,LCA)
     * 
     * 没有parent指针
     * 不借用额外容器
     * 前提p,q存在于root
     * 
     * 判断节点的归属
     * 
     * @param root 根
     * @param p    节点
     * @param q    节点
     * @return p,q的lca
     */
    public static TreeNode LCA(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null)
            return null;
        if (root.equals(p) || root.equals(q))
            return root;

        boolean onLeft = cover(root.left, p);
        boolean onRight = cover(root.right, q);
        if (onLeft == onRight)// 都是true的情况, 假设都一定含有p,q
            return root;
        else if (onLeft)
            return LCA(root.left, p, q);
        else
            return LCA(root.right, p, q);
    }

    /**
     * 判断 树root是否包含x
     * 
     * @param root 根
     * @param x    节点
     * @return 是否包含
     */
    private static boolean cover(TreeNode root, TreeNode x) {
        if (root == null)
            return false;
        if (root.equals(x))
            return true;
        return cover(root.left, x) || cover(root.right, x);
    }

    /**
     * LCA 优化版
     * 
     * @param root
     * @param p
     * @param q
     * @return
     */
    public static TreeNode LCAs(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null)
            return null;
        if (root.equals(p) || root.equals(q))
            return root;
        TreeNode x = LCAs(root.left, p, q);// 可能返回lca,p(p在这一侧),q,null
        if (x != null && !x.equals(p) && !x.equals(q)) {
            return x;
        }
        TreeNode y = LCAs(root.right, p, q);
        if (y != null && !y.equals(p) && !y.equals(q)) {
            return y;
        }
        // x: p,q,null y: q,p,null
        if (x != null && y != null) {// 一边找着一个
            return root;
        } else {
            return x == null ? y : x;// 有一个不为null,返回,都为null,返回null
        }
    }
}

class Pair<K, V> {
    K key;
    V val;

    Pair(K k, V v) {
        key = k;
        val = v;
    }
}