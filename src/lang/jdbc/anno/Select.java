package lang.jdbc.anno;

import java.lang.annotation.*;

@Target(value = { ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Select {
    String value();
}
