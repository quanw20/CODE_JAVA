package lang.jdbc.anno;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;

import static lang.jdbc.MySql.*;

public class DaoMapperImplement implements IDaoMapper {
    private static Connection connection = null;
    static {
        connection = getConnection();
    }

    @Override
    public void insert(String username, String password) {
        Insert insert = null;
        try {
            Method insertMethod = Dao.class.getMethod("insert", String.class, String.class);
            insert = insertMethod.getAnnotation(Insert.class);
            String sql = insert.value();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            int rows = ps.executeUpdate();
            System.out.println(rows);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete() {
        // TODO Auto-generated method stub

    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

    @Override
    public void select() {
        // TODO Auto-generated method stub

    }
    public static void main(String[] args) {
        DaoMapperImplement dao = new DaoMapperImplement();
        dao.insert("username2", "password2");
    }

}
