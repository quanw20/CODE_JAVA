package lang.jdbc;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * SQLite3 工具类
 */

public class SQLite3 {
    private static final Path path = Path.of(System.getProperty("user.dir"), "/src/resources/data/java-sqlite.db");
    private static final String url = "jdbc:sqlite:" + path.toString();
    private static Connection connection;

    public static Connection getConnection() {
        try {
            connection = DriverManager.getConnection(url);
            System.out.println("[OK] Connection to SQLite has been established.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }

    public static void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }


    public static void main(String[] args) {
        // Connection conn = getConnection();
        // try {
        // Statement state = conn.createStatement();
        // ResultSet rs = state.executeQuery("SELECT * FROM login;");
        // } catch (SQLException e) {
        // e.printStackTrace();
        // }
        // runSQLite3();

    }
}
