package lang.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.Scanner;


/**
 * @Description:
 * @ClassName: Login
 * @Author: QUANWEI
 * @Date: 2021/11/7 16:12
 * @Version: 1.0
 */
public class Login {
    public boolean login(String username, String password) {
        if (username == null || password == null) return false;
        /**
         * sql注入:
         * 请输入账号:
         * ko
         * 请输入密码:
         * a' or 'a'='a
         * select * from login where username='ko' and password='a' or 'a'='a'
         * 登录成功
         * 使用PrepaaredStatement
         */
        String query = "select * from login where username='" + username + "' and password='" + password + "'";
        System.out.println(query);
        Connection connection = null;
        Statement statement = null;
        ResultSet set = null;
        try {
            connection = MySql.getConnection();
            statement = connection.createStatement();
            set = statement.executeQuery(query);
            return set.next();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySql.close(set, statement, connection);
        }
        return false;
    }

    public boolean improvedLogin(String username, String password) {
        if (username == null || password == null) return false;
        String query = "select * from login where username=? and password=?";
        Connection connection = null;
        Statement statement = null;
        PreparedStatement prepareStatement = null;
        ResultSet set = null;
        try {
            //获取连接对象
            connection = MySql.getConnection();
//            获取执行对象
            prepareStatement = connection.prepareStatement(query);
//            给?赋值
            prepareStatement.setString(1, username);
            prepareStatement.setString(2, password);
//            执行sql
            set = prepareStatement.executeQuery();
            return set.next();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MySql.close(set, prepareStatement, connection);
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入账号:");
        String username = scanner.nextLine();
        System.out.println("请输入密码:");
        String password = scanner.nextLine();
        scanner.close();
        System.out.println(new Login().improvedLogin(username, password) ? "登录成功" : "登陆失败");
    }

    
}
