package lang.thread;

import java.util.LinkedList;

public class ProducerConsumer3 {
  private final LinkedList<Integer> list = new LinkedList<>();
  private final int LENGTH = 5;
  private final int[] arr = { 0 };

  private final Runnable producer = () -> {
    synchronized (list) {
      while (true) {
        if (list.size() < LENGTH) {// 生产
          sleep();
          list.add(++arr[0]);
          System.out.println("Produce : " + arr[0]);
          list.notify();
        } else {// 等待
          try {
            System.out.println("Produce : " + "Waiting...");
            list.wait();
          } catch (InterruptedException e) {
            throw new RuntimeException(e);
          }
        }
      }
    }
  };

  private final Runnable consumer = () -> {
    synchronized (list) {
      while (true) {
        if (list.size() > 0) {// 消费
          sleep();
          Integer integer = list.removeFirst();
          --arr[0];
          System.out.println("Consume : " + integer);
          list.notify();
        } else {// 等待
          try {
            System.out.println("Consume : " + "Waiting...");
            list.wait();
          } catch (InterruptedException e) {
            throw new RuntimeException(e);
          }
        }
      }
    }
  };

  public static void main(String[] args) {
    ProducerConsumer3 pc = new ProducerConsumer3();
    new Thread(pc.producer, "Producer").start();
    new Thread(pc.consumer, "Consumer").start();

  }

  private static void sleep() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
