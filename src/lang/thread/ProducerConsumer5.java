package lang.thread;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用 ReentrantLock + Condition
 */
public class ProducerConsumer5 {
  private final ReentrantLock lock = new ReentrantLock();
  private final Condition notEmpty = lock.newCondition();
  private final Condition notFull = lock.newCondition();
  private final LinkedList<Integer> list = new LinkedList<>();
  private final int LENGTH = 5;
  private final int[] arr = {0};

  private final Runnable producer = () -> {
    while (true) {// 生产
      try {
        lock.lockInterruptibly();
        while (list.size() == LENGTH) {// 等待
          sleep();
          System.out.println("Produce : " + "Waiting...");
          notFull.await();
        }
        sleep();
        list.add(++arr[0]);
        notEmpty.signal();
        System.out.println("Produce : " + arr[0]);
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        lock.unlock();
      }
    }
  };

  private final Runnable consumer = () -> {
    while (true) {
      try {
        lock.lockInterruptibly();
        while (list.size() == 0) {
          sleep();
          System.out.println("Consume : " + "Waiting...");
          notEmpty.await();
        }
        sleep();
        --arr[0];
        list.removeLast();
        notFull.signal();
        System.out.println("Consume : " + arr[0]);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      } finally {
        lock.unlock();
      }
    }
  };

  public static void main(String[] args) {
    ProducerConsumer5 pc = new ProducerConsumer5();
    new Thread(pc.producer, "Producer").start();
    new Thread(pc.consumer, "Consumer").start();

  }

  private static void sleep() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
