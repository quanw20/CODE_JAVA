package lang.thread;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

/**
 * 意思是说，ThreadLocal 中填充的的是当前线程的变量，
 * 该变量对其他线程而言是封闭且隔离的，ThreadLocal
 * 为变量在每个线程中创建了一个副本，这样每个线程都可以访问自己内部的副本变量。
 * 
 * 1、在进行对象跨层传递的时候，使用ThreadLocal可以避免多次传递，打破层次间的约束。
 * 2、线程间数据隔离
 * 3、进行事务操作，用于存储线程事务信息。
 * 4、数据库连接，Session会话管理。
 * 5、Spring 声明事务
 */
public class TheadLocalDemo {
    static class User {
        private String name;

        public String getName() {
            return name;
        }

        public User(String name) {
            this.name = name;
        }

        public User() {
        }

        /**
         * 不用自己重写,这里只是演示
         */
        @Override
        protected void finalize() throws Throwable {
            System.out.println("Called User.finalize");
        }

        @Override
        public String toString() {
            return "User:{name:" + name + "}";
        }
    }

    // static class Entry extends WeakReference<ThreadLocal<?>>
    // Key(ThreadLocal)是一个弱引用
    // 当我们new的ThreadLocal对象指向null时，key为弱引用，对象可以被GC回收
    // expungeStaleEntry() 中 tab[staleSlot].value = null; 将value对象的引用设为null
    static ThreadLocal<User> tl = new ThreadLocal<>();// 本身是一个Map<ThreadLocal,对象>

    @Test
    void test_ThreadLocal() {
        // 第一个线程设置的第二个线程读不到
        new Thread(() -> {
            tl.set(new User("quanwei"));
            System.out.println("Thread-1:" + tl.get());
            // tl.remove();// 不用时记得删除
        }).start();

        new Thread(() -> {
            System.out.println("Thread-2:" + tl.get());
        }).start();
    }

    @Test
    void test_ThreadLocal1() {
        while (true) {
            User user = new User("tang");
            tl.set(user);
            user = null;// user会被释放
        }
    }

    @Test
    void test_ThreadLocal2() {
        while (true) {
            new ThreadLocal<>().set(new User("tang"));
            // user会被释放
        }

    }

    public static void main(String[] args) {

    }
}

