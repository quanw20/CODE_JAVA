package lang.thread;

import java.time.LocalTime;

/**
 * 守护线程
 */
public class DaemonDemo {
    public static void main(String[] args) {
        new Thread(() -> {
            int i = 10;
            while (i-- > 0) {
                System.out.println(Thread.currentThread().getName());
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "线程1").start();

        var t = new Thread(() -> {
            while (true) {
                System.out.println(Thread.currentThread().getName() + " -> " + LocalTime.now());
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    break;
                }
            }
        }, "Daemon");// 守护进程
        t.setDaemon(true);
        t.start();

    }
}