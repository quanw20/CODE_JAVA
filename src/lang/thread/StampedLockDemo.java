package lang.thread;

import java.util.concurrent.locks.StampedLock;

public class StampedLockDemo {
  private double x, y;
  private final StampedLock lock = new StampedLock();

  void move(double dx, double dy) {
    long stamp = lock.writeLock();
    try {
      x += dx;
      y += dy;
    } finally {
      lock.unlockWrite(stamp);
    }
  }

  double distanceFromOrigin() {
    long stamp = lock.tryOptimisticRead();
    double cx = x, cy = y;
    if (!lock.validate(stamp)) {
      stamp = lock.readLock();
      try {
        cx = x;
        cy = y;
      } finally {
        lock.unlockRead(stamp);
      }
    }
    return Math.sqrt(cx * cx + cy * cy);
  }

  public static void main(String[] args) {
    StampedLockDemo m = new StampedLockDemo();
    m.move(12.5,3.2);
    System.out.println(m.distanceFromOrigin());
  }
}
