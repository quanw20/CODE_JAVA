package lang.io.bio;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 65535)) {
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            byte[] bb=new byte[8196];
            int read = is.read(bb);
            System.out.println(new String(bb,0,read,"utf-8"));
            String line = br.readLine();
            os.write(line.getBytes());
            byte[] b = new byte[66];
            is.read(b);
            System.out.println(new String(b));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
