package lang.io.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * 客户端与服务器建立连接后，向服务器发送一条消息，并从服务器读取返回的消息
 */
public class ReactorClient {
  private final String host;
  private final int port;

  public ReactorClient(String host, int port) {
    this.host = host;
    this.port = port;
  }

  public void start() throws IOException {
    InetSocketAddress serverAddr = new InetSocketAddress(host, port);
    SocketChannel channel = SocketChannel.open(serverAddr);

    System.out.println("Connected to " + channel.getRemoteAddress());

    ByteBuffer buffer = ByteBuffer.allocate(1024);
    buffer.put("Hello, Reactor!".getBytes());
    buffer.flip();
    channel.write(buffer);

    buffer.clear();
    channel.read(buffer);
    byte[] data = new byte[buffer.position()];
    System.arraycopy(buffer.array(), 0, data, 0, buffer.position());

    System.out.println("Received " + new String(data) + " from " + channel.getRemoteAddress());

    channel.close();
  }

  public static void main(String[] args) throws IOException {
    new ReactorClient("localhost", 9090).start();
  }
}
