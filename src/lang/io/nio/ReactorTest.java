package lang.io.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class ReactorTest {

  public static void main(String[] args) throws Exception {
    new Thread(new Reactor(9090)).start();
  }
}

class Client {
  public static void main(String[] args) throws IOException {
    SocketChannel socketChannel = SocketChannel.open();
    InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", 9090);
    socketChannel.configureBlocking(false);

    socketChannel.connect(inetSocketAddress);
    while (!socketChannel.finishConnect()) {
      // wait, 或者做其它事情
    }
    String requestMsg = "Hello, Reactor!";
    ByteBuffer writeBuffer = ByteBuffer.allocate(requestMsg.getBytes().length);
    writeBuffer.put(requestMsg.getBytes());
    writeBuffer.flip();
    socketChannel.write(writeBuffer);
    System.out.println("向服务端发送消息：" + requestMsg);

    ByteBuffer readBuffer = ByteBuffer.allocate(1024);
    int len = socketChannel.read(readBuffer);
    if (len != -1) {
      readBuffer.flip();
      System.out.println("接收到服务端的消息：" + new String(readBuffer.array(), 0, len));
    }
    socketChannel.close();
  }
}
