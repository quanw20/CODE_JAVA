package lang.system;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

public class SystemDemo {
    public static void main(String[] args) {
        property();

        // 时间的格式为当前计算机时间与 GMT 时间（格林尼治时间）
        // 1970 年 1 月 1 日 0 时 0 分 0 秒所差的毫秒数
        Date date = new Date(System.currentTimeMillis());//
        System.out.println(date); // Mon Jan 10 20:20:43 CST 2022

        int[] a = { 1, 2, 3, 4, 5 };
        int[] b = { 2, 3, 4, 5, 6 };
        // 合并数组
        int[] marge = marge(a, b);
        System.out.println(Arrays.toString(marge));// [1, 2, 3, 4, 5, 2, 3, 4, 5, 6]

        // 请求系统进行垃圾回收
        System.gc();

        // 终止当前正在运行的 Java 虚拟机
        System.exit(0);
    }

    public static int[] marge(int[] a, int[] b) {
        int[] ret = new int[a.length + b.length];
        System.arraycopy(a, 0, ret, 0, a.length);
        System.arraycopy(b, 0, ret, a.length, b.length);
        return ret;
    }

    public static <T> T[] marge(T[] a, T[] b) {
        T[] ret = (T[]) Array.newInstance(a.getClass(), a.length + b.length);
        if (ret.getClass().equals(a.getClass())) {
            System.arraycopy(a, 0, ret, 0, a.length);
            System.arraycopy(b, 0, ret, a.length, b.length);
            return (T[]) ret;
        }
        return null;
    }

    public static void property() {

        Properties properties = System.getProperties();
        Set<Entry<Object, Object>> entrySet = properties.entrySet();
        for (Entry<Object,Object> entry : entrySet) {
            System.out.println(entry);
        }
    }

}
