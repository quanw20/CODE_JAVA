package lang.ahead;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.LinkedList;

import org.junit.jupiter.api.Test;

/**
 * 强引用: 普通的引用, 强引用指向的对象不会被回收
 * 软引用: 仅有软引用指向的对象, 只有发生gc且内存不足, 才会被回收
 * 虚引用: 指向将要被回收的对象, 调用gc就会被回收
 * 弱引用: 仅有弱引用指向的对象, 只要发生gc就会被回收
 */
public class ReferenceType {
    static class User {
        private String name;

        public String getName() {
            return name;
        }

        public User(String name) {
            this.name = name;
        }

        public User() {
        }

        /**
         * 不用自己重写,这里只是演示
         */
        @Override
        protected void finalize() throws Throwable {
            System.out.println("Called User.finalize");
        }

        @Override
        public String toString() {
            return "User:{name:" + name + "}";
        }
    }

    static class PR {// Phantom Reference
        private static final LinkedList<Object> LIST = new LinkedList<>();
        private static final ReferenceQueue<User> QUEUE = new ReferenceQueue<>();

        public static void main(String[] args) {
            // 通知GC 追踪垃圾回收
            PhantomReference<User> phantomReference = new PhantomReference<>(new User(), QUEUE);
            System.out.println(phantomReference.get());

            new Thread(() -> {
                while (true) {
                    LIST.add(new byte[1024 * 1024]);
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(phantomReference.get());
                }
            }).start();
            // 垃圾回收线程
            new Thread(() -> {
                while (true) {
                    Reference<? extends User> p = QUEUE.poll();
                    if (p != null)
                        System.out.println("Phantom reference object \"" + p + "\" is collected by GC");
                }
            }).start();
            // interface DirectBuffer
            ByteBuffer.allocateDirect(1024);
            // 以前: 网卡 -> 内存 -> JVM堆
            // 现在: Java程序直接读写内存 但是不属于JVM堆的内存不归GC管理
            // 我们使用了这些内存就要用虚引用指向它们
        }
    }

    @Test
    void test_StrongReference() {
        // 强引用
        var user = new User();
        System.out.println(user);
        user = null;
        System.gc();
        System.out.println(user);
    }

    @Test
    void test_SoftReferences() {
        // user1-(强引用)->User-(软引用)->name
        var user1 = new User("quanwei");
        String name = user1.getName();// 内存不足再清理
        System.out.println(user1);
        user1 = null;
        System.gc();
        System.out.println(user1);
        System.out.println(name);
    }

    @Test
    void test_PhantomReference() {

    }

    @Test
    void test_WeakReference() {
        WeakReference<User> wr = new WeakReference<>(new User());
        System.out.println(wr.get());
        System.gc();
        System.out.println(wr.get());
        // ThreadLocal
    }

}
