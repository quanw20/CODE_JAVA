package lang.functional;

public class Demo {
    // 递归函数
    public static final Function<Integer, Integer> fact;
    static {
        fact = n -> n <= 1 ? 1 : n * Tests.fact.apply(n - 1);
    }

    public static void main(String[] args) {
        System.out.println(fact.apply(10));
    }
}
interface Int{
    class IntImpl implements Int{}
}
