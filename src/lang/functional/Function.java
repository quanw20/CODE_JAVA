package lang.functional;

/**
 * 函数式接口
 * 
 * <pre>
 * 纯函数:
 * 1. 不能修改函数外的任何东西. 外部观测不到内部的任何变化
 * 2. 不能修改自己的参数
 * 3. 不能抛出错误或异常
 * 4. 必须返回一个值
 * 5. 只要调用它的参数相同,结果也必然相同
 * 
 * 复合函数
 * 多态函数
 * 柯里化
 * 高阶函数:接受函数为参数,返回函数
 * 函数式编程的特点: 如果组件不会出错, 那么整个程序也不会
 * </pre>
 */
public interface Function<T, U> {
    U apply(T arg);// T -> U
    // 复合函数 复合次数太多会导致栈溢出(大概能嵌套6000次)
    default <V> Function<V, U> compose(Function<V, T> f) {
        return x -> apply(f.apply(x)); // V->T->U
    }

    default <V> Function<T, V> andThen(Function<U, V> f) {
        return x -> f.apply(apply(x)); // T->U->V
    }

    static <T> Function<T, T> identity() {
        return t -> t;
    }

    static <T, U, V> Function<V, U> compose(Function<T, U> f, Function<V, T> g) {
        return x -> f.apply(g.apply(x)); // V->T->U
    }

    static <T, U, V> Function<T, V> andThen(Function<T, U> f, Function<U, V> g) {
        return x -> g.apply(f.apply(x)); // T->U->V
    }

    static <T, U, V> Function<Function<T, U>, Function<Function<U, V>, Function<T, V>>> compose() {
        return x -> y -> y.compose(x); 
    }

    static <T, U, V> Function<Function<T, U>, Function<Function<V, T>, Function<V, U>>> andThen() {
        return x -> y -> y.andThen(x);
    }

    static <T, U, V> Function<Function<T, U>, Function<Function<U, V>, Function<T, V>>> higherAndThen() {
        return x -> y -> z -> y.apply(x.apply(z));
    }

    static <T, U, V> Function<Function<U, V>, Function<Function<T, U>, Function<T, V>>> higherCompose() {
        return (Function<U, V> x) -> (Function<T, U> y) -> (T z) -> x.apply(y.apply(z));
    }
}
