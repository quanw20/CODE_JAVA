package lang.xml;

import java.io.File;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomXml {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<Student>(3);
        File file = new File("D:/workspaceFolder/CODE_JAVA/src/lang/xml/student.xml");
        DocumentBuilderFactory Factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder Builder = Factory.newDocumentBuilder();
            Document doc = Builder.parse(file);

            Element elem = doc.getDocumentElement();
            elem.normalize();
            System.out.println("Root element: " + elem.getNodeName());
            NodeList nList = doc.getElementsByTagName("student");
            for (int i = 0; i < nList.getLength(); ++i) {
                org.w3c.dom.Node nNode = nList.item(i);
                System.out.println("Current element: " + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    var element = (Element) nNode;
                    int id = Integer.parseInt(element.getAttribute("id"));
                    String name = element.getElementsByTagName("name").item(0).getTextContent();
                    int age = Integer.parseInt(element.getElementsByTagName("age").item(0).getTextContent());
                    students.add(new Student(name, age, id));
                }
            }
            for (Student student : students) {
                System.out.println("-------------------------");
                System.out.println(student);
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
