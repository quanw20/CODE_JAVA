package lang.net;

import java.io.IOException;
import java.net.InetAddress;

/**
 * IpDemo
 */
public class IpDemo {

    public static void main(String[] args) throws IOException {
        InetAddress Aliyun = InetAddress.getByName("39.99.54.127");
        InetAddress localHost = InetAddress.getLocalHost();
        InetAddress loopbackAddress = InetAddress.getLoopbackAddress();
        boolean reachable = localHost.isReachable(10);
        System.out.println(reachable);// true
        System.out.println(localHost);// DESKTOP-V9EL4A5/192.168.0.193
        System.out.println(Aliyun);/// 39.99.54.127
        String canonicalHostName = Aliyun.getCanonicalHostName();
        String hostName = Aliyun.getHostName();// 39.99.54.127
        System.out.println(hostName);// 39.99.54.127
        System.out.println(canonicalHostName);
        System.out.println(loopbackAddress);// localhost/127.0.0.1
    }

}