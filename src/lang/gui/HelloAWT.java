package lang.gui;

import java.awt.*;
import java.awt.event.*;

public class HelloAWT {
    public static void main(String[] args) {
        // 1.创建窗体
        Frame frame = new Frame("AWT");
        // 2.创建标签组件
        Label label = new Label("Hello Quanwei");
        // 3.设置相关属性
        label.setBackground(Color.GREEN);
        label.setSize(20, 10);
        // 4.将组件添加到窗体上
        frame.add(label);
        // 5.设置窗体关闭事件
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        // 6.设置窗体大小
        frame.setSize(500, 200);
        // 7.显示窗体
        frame.setVisible(true);
    }
}
