package lang.gui;

import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

public class Main {

    public void window1() {
        // 1.创建窗体
        JFrame frame = new JFrame("Swing");
        // 2.设置关闭事件
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // 3.设置大小位置
        frame.setSize(500, 500);
        frame.setLocation(500, 200);

        // 4.设置事件监听处理
        frame.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                frame.setTitle("点击坐标为(" + x + ", " + y + ")");
            }
        });
        // 5.显示窗体
        frame.setVisible(true);
    }

}
