package lang.classes;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Test;

public class MathDemo {
    @Test
    public void testBigInteger() {

    }

    @Test
    public void testBigDecimal() {
        BigDecimal d1 = new BigDecimal("9876543210.0123456789");
        BigDecimal d2 = new BigDecimal("123.45000000000000000");
        System.out.println(d1.scale()); // 10,两位小数
        System.out.println(d1.add(d2));
        System.out.println(d1.subtract(d2));
        System.out.println(d1.multiply(d2));
        // 无法除尽时，就必须指定精度以及如何进行截断
        System.out.println(d1.divide(d2, 10, RoundingMode.HALF_UP));// 四舍五入
    }

}
