package lang.classes;

public class WeekTestMain {
    public static void main(String[] args) {
        for (var i : Week.values()) {
            System.out.println(i);
        }
        // 通过values()获取枚举数组
        Week[] weeks = Week.values();
        // 遍历Week枚举类
        for (Week day : weeks) {
            System.out.println("name:" + day.name() +
                    ",desc:" + day.getDesc());
        }
        // 不符合则抛出java.lang.IllegalArgumentException
        System.out.println(Week.valueOf("MONDAY"));
        // 返回对应的name属性
        System.out.println(Week.FRIDAY.toString());
        // 返回4，根据我们定义的次序，从0开始。如果在定义时调换FRIDAY
        // 的次序，返回的数字也会对应的变化
        System.out.println(Week.FRIDAY.ordinal());
    }
}

