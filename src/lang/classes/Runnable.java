package lang.classes;
/**
 * 可以跑的接口 :)
 */
public interface Runnable {
    // 跑的方法
    void run();
}
