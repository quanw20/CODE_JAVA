package lang.classes;

import java.io.BufferedInputStream;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

public class ScannerDemo {
    @Test
    public void testScnner() {
        Scanner in = new Scanner(System.in);
        String next = in.next();
        int nextInt = in.nextInt();
        String rn = in.nextLine();// 读取int后面的\r\n
        String nextLine = in.nextLine();
        int cnt = 0, sum = 0;
        while (in.hasNextInt()) {// 当下一个输入不是整数时退出
            int nextInt1 = in.nextInt();
            sum += nextInt1;
            cnt++;
        }
        System.out.println(sum * 1.0 / cnt);

        in.close();
    }

    public static void main(String[] args) {
        new ScannerDemo().testScnner();
    }
}
