package lang.classes;

import java.io.IOException;

import org.junit.jupiter.api.Test;

public class PackageClassDemo {
    public static void main(String[] args) {

        PackageClassDemo p = new PackageClassDemo();
        p.testRead();
    }

    @Test
    public void testRead() {
        try {
            int read;
            while ((read = System.in.read()) != -1) {
                // if(read==13)
                // break;
                // System.in.read(); // \r
                // System.in.read(); // \n
                System.out.println("read:" + read);
                System.out.println("char:" + (char) read);
                // System.out.print((char) read);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInteger() {
        int parseInt = Integer.parseInt("12");
        Integer valueOf = Integer.valueOf("23");
        int intValue = valueOf.intValue();
        System.out.println(Integer.BYTES);
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        System.out.println(Integer.SIZE);
        System.out.println(Integer.bitCount(-1));
        System.out.println(Integer.reverse(-2));
        System.out.println(Integer.rotateLeft(1, 2));
        System.out.println(Integer.toUnsignedString(1147483647));
        System.out.println();

    }
}
