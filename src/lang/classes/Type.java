package lang.classes;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.junit.jupiter.api.Test;

public class Type {
    /* 基本类型 默认值 */
    byte y; // byte: 0
    short s; // short: 0
    int i; // int: 0
    long l; // long: 0
    double d; // double: 0.0
    float f; // float: 0.0
    boolean b; // boolean: false
    char c; // char:

    void show() {
        System.out.println("byte: " + y);
        System.out.println("short: " + s);
        System.out.println("int: " + i);
        System.out.println("long: " + l);
        System.out.println("double: " + d);
        System.out.println("float: " + f);
        System.out.println("boolean: " + b);
        System.out.println("char: " + c);
    }

    void assign() {
        y = -1;
        s = 1;
        i = 2;
        l = 3L;
        d = 12.5;
        f = 6.25F;
        b = true;
        c = 'c';
    }

    public static void main(String[] args) {
        Type type = new Type();
        type.assign();
        type.show();
    }

    @Test
    public void test() {
        // 1.创建array对象, 并赋值
        String[] strings = { "hello", "thank", "you", "are", "ok" };
        // 顺序
        Arrays.sort(strings);
        System.out.println(Arrays.toString(strings)); // [are, hello, ok, thank, you]
        // 逆序
        Arrays.sort(strings, Comparator.reverseOrder());
        System.out.println(Arrays.toString(strings)); // [you, thank, ok, hello, are]

    
    }
}
