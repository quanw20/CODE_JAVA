package lang.classes;

// 抽象类
public abstract class Human {
    String name;
    int age;
    String language;

    // 抽象方法
    abstract public void say();
}

class Chinese extends Human implements Runnable{
    Chinese(String name, String language) {
        this.name = name;
        this.language = language;
    }

    @Override
    public void say() {
        System.out.println(name + " says " + language);

    }
    /**
     * 实现接口须重写run方法
     */
    @Override
    public void run() {
       System.out.println("I am Running...");        
    }
}
