package lang.classes;

import org.junit.jupiter.api.Test;


/**
 * String 不
 * 
 * StringBuffer 可变,同步安全,效率低
 * 
 * StringBuilder JDK5.0新增 可变,同步不安全,效率高
 *
 * 底层都用char[]存储
 */
public class TestStringBufferBuilder {
    @Test
    public void testStringBuilder() {
        var sb1 = new StringBuilder("Tang");
        var sb2 = new StringBuilder("t");
        System.out.println(sb1.capacity());// 20
        System.out.println(sb1.length());// 4
        sb1.ensureCapacity(40);
        System.out.println(sb1.capacity());// 42
        System.out.println(sb2.capacity());// 17
        sb1.append("Quawnei");
        System.out.println(sb1);//TangQuawnei
        sb1.insert(4, "->");
        System.out.println(sb1);//Tang->Quawnei
        sb1.delete(0, 4);
        System.out.println(sb1);//->Quawnei
    }

}
