package lang.classes;

public class User {
    /* 私有属性 */
    private int id;
    private String username;
    private String passpord;

    /**
     * 构造函数
     */
    public User() {
    }

    /**
     * @return the passpord
     */
    public String getPasspord() {
        return passpord;
    }

    /**
     * @param passpord the passpord to set
     */
    public void setPasspord(String passpord) {
        this.passpord = passpord;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
   @Override
   public String toString() {
       return "id: "+getId()+"\tusername: "+getUsername()+"\tpassword: "+getPasspord();
   }

}
