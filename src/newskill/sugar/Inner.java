package newskill.sugar;

public class Inner {
    public static void main(String[] args) {
        Over1 o1 = new Over1();
        newskill.sugar.Over1.Inner3 inner3 = o1.new Inner3();
        var in = new Inner().new Inner3();
        in.go();
        inner3.go();
    }

    /**
     * Inner
     */
    public static class Inner2 extends SyntacticSuger {
        public static void main(String[] args) {
            System.out.println("Inner");
            var it = new MyInterface() {
            };
            it.defaultMethod();
            new Inner2().testFunctionalInterface();
        }
    }

    public class Inner3 {
        public static void main(String... args) {
            System.out.println("Inner");
        }

        public void go() {
            System.out.println("Inner3.go");
        }
    }
}

class Over1 extends Inner {
    public class Inner3 /* extends Inner.Inner3 */{
        public static void main(String... args) {
            System.out.println("Over.Inner");
        }

        // @Override //不能重写
        public void go() {
            System.out.println("Over1.go");
        }
    }

}
