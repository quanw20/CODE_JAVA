package newskill.sugar;

public interface MyInterface {// �ӿ�û��ctor,û��main����(��������),û��protected�ķ���
    // Ĭ�Ϸ��� java7
    // 0.default�ؼ��ֱ�ʾ�ӿڵ�Ĭ�Ϸ���,
    // 1.�ӿڵ�Ĭ�Ϸ���������дObject��ķ���
    // 2.ʵ������Լ̳�/��д���ӿڵ�Ĭ�Ϸ���
    // 3.�ӿڿ��Լ̳�/��д���ӿڵ�Ĭ�Ϸ���
    // 4.�����ุ�ӿڶ���Ĭ�Ϸ���,����̳и����Ĭ�Ϸ���
    // 5.����̳��������ӿ�(����ͬĬ�Ϸ���),�������д���default����
    public default void defaultMethod() {
        // System.out.println("This is the *default* method of interface SS, \nwhich is implemented in class "
        //         + this.getClass().getName());
        // privateStaticMethod();
        // privateMethod();

    }

    // ��̬���� java8
    // ֻ���ڽӿ�, ���ɱ��̳�
    public static void staticMethod() {
        System.out.println("This is the *static* method of interface SS");
        privateStaticMethod();

    }

    // ˽�з��� java9
    // ֻ���ڽӿ�, ���ɱ��̳�, �����ڽӿ��ⱻֱ�ӵ��� use locally
    // ��̬˽�з������Ա�Ĭ�Ϸ�������
    private void privateMethod() {
        System.out.println("This is the *private* method of interface SS");

    }

    private static void privateStaticMethod() {
        System.out.println("This is the *private static* method of interface SS");

    }



}
