package tang.quanwei._5;

public class Client {
  public static void main(String[] args) throws CloneNotSupportedException {
    Resume resume = new Resume();
    resume.setPhoto(new Object());

    Resume resume1 = resume.clone();
    System.out.println((resume == resume1 ? "是" : "不是") + "同一份简历");
    System.out.println((resume.getPhoto() == resume1.getPhoto() ? "是" : "不是") + "同一张照片");
  }
}
