package tang.quanwei._3;

public class MySQLFactory extends DBFactory {
  @Override
  public Connection createConnection() {
    return new DirectedConnection();
  }

  @Override
  public Statement createStatement() {
    return new SimpleStatement();
  }
}
