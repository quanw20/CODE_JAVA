package tang.quanwei._3;

public class SimpleStatement extends Statement {
  @Override
  public void execute(String sql) {
    System.out.println("SimpleStatement.execute "+sql);
  }
}
