package tang.quanwei._3;

public class PooledConnection extends Connection{
    @Override
    public void connect() {
        System.out.println("PooledConnection.connect");
    }

    @Override
    public void disconnect() {
        System.out.println("PooledConnection.disconnect");
    }
}
