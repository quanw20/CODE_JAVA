package tang.quanwei._3;

// 抽象数据库工厂 测试类
public class Client {
  public static void main(String[] args) {
    System.out.println("=================MySQL=================");
    // 创建MySQL数据库工厂
    DBFactory mysqlFactory = new MySQLFactory();
    // 创建MySQL数据库连接对象
    Connection mysqlConn = mysqlFactory.createConnection();
    // 创建MySQL数据库语句对象
    Statement mysqlStatement = mysqlFactory.createStatement();
    // 调用共有方法
    mysqlConn.status();
    // 调用连接对象的方法
    mysqlConn.connect();
    // 调用语句对象的方法
    mysqlStatement.execute("select * from table");
    // 调用连接对象的方法
    mysqlConn.disconnect();
    System.out.println("=================Oracle=================");
    // 创建Oracle数据库工厂
    DBFactory oracleFactory = new OracleFactory();
    // 创建Oracle数据库连接对象
    Connection oracleConnection = oracleFactory.createConnection();
    // 创建Oracle数据库语句对象
    Statement oracleStatement = oracleFactory.createStatement();
    // 调用共有方法
    oracleConnection.status();
    // 调用连接对象的方法
    oracleConnection.connect();
    // 调用语句对象的方法
    oracleStatement.execute("select * from table");
    // 调用连接对象的方法
    oracleConnection.disconnect();
  }
}
