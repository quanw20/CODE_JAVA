package tang.quanwei._3;

public class OracleFactory extends DBFactory {
  @Override
  public Connection createConnection() {
    return new PooledConnection();
  }

  @Override
  public Statement createStatement() {
    return new PreparedStatement();
  }
}
