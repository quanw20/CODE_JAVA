package tang.quanwei._1;

public interface Shape {
  void draw();
  void erase();
}
