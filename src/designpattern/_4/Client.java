package tang.quanwei._4;

/**
 * 在某赛车游戏中，赛车包括方程式赛车、场地越野赛车、运动汽车、卡车等类型，
 * 不同类型的赛车的车身、发动机、轮胎、变速箱等部件有所区别。
 * <p>
 * 玩家可以自行选择赛车类型，系统将根据玩家的选择创建出一辆完整的赛车。
 * <p>
 * 现采用建造者模式实现赛车的构建，绘制对应的类图并编程模拟实现
 */
public class Client {
  public static void main(String[] args) {
    System.out.println("===============FormulaRacing===============");
    CarBuilder builder = new FormulaRacingBuilder();
    CarDirector director = new CarDirector(builder);
    director.construct();
    Car car = builder.getCar();
    car.run();
    System.out.println("=================SportsCar=================");
    builder = new SportsCarBuilder();
    director = new CarDirector(builder);
    director.construct();
    car = builder.getCar();
    car.run();
  }
}


