package tang.quanwei._4;

public abstract class Car {
  private String autoBody;
  private String motor;
  private String tyre;
  private String gearbox;


  public void setAutoBody(String autoBody) {
    this.autoBody = autoBody;
  }

  public void setMotor(String motor) {
    this.motor = motor;
  }

  public void setTyre(String tyre) {
    this.tyre = tyre;
  }

  public void setGearbox(String gearbox) {
    this.gearbox = gearbox;
  }

  @Override
  public String toString() {
    return "{" +
            "autoBody='" + autoBody + '\'' +
            ", motor='" + motor + '\'' +
            ", tyre='" + tyre + '\'' +
            ", gearbox='" + gearbox + '\'' +
            '}';
  }

  public final void run() {
    System.out.println(this);
  }
}
