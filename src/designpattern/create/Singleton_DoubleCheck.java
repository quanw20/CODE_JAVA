package designpattern.create;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;

/**
 * Singleton Double Check
 * 
 * 优点：线程安全，延迟加载
 * 缺点：无法防止反射
 * 
 */
public class Singleton_DoubleCheck implements Serializable {
    /**
     * volatile
     * 1.线程间可见
     * 2.禁止指令重排序
     */
    private static volatile Singleton_DoubleCheck INSTANCE;
    static {
        System.out.println("outer init");
    }

    private Singleton_DoubleCheck() {
        System.out.println("private ctor init");
    }

    public static Singleton_DoubleCheck getInstance() {
        if (INSTANCE == null) {
            synchronized (Singleton_DoubleCheck.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Singleton_DoubleCheck();
                }
            }
        }
        return INSTANCE;
    }

    public static void hello() {
        System.out.println("hello called");
    }

    /**
     * 解决序列化破坏单例
     */
    private Object readResolve() {
        return INSTANCE;
    }

    /**
     * 解决克隆破坏单例
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return INSTANCE;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        testLazy();
        testMThread();
        testSer();
        testClone();
        testReflect();
    }

    private static void testSer() {
        try {
            var instance = Singleton_DoubleCheck.getInstance();
            var bos = new ByteArrayOutputStream(1024);
            var oos = new ObjectOutputStream(bos);
            oos.writeObject(instance);
            oos.close();
            var bis = new ByteArrayInputStream(bos.toByteArray());
            var ois = new ObjectInputStream(bis);
            var o = ois.readObject();
            ois.close();
            System.out.println(o == instance);// true
        } catch (Exception e) {
        }

    }

    private static void testClone() {
        var instance = Singleton_DoubleCheck.getInstance();
        Singleton_DoubleCheck clone = null;
        try {
            clone = (Singleton_DoubleCheck) instance.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println(instance == clone);// true 是同一个对象

    }

    private static void testMThread() {
        Runnable r = () -> {
            var instance = Singleton_DoubleCheck.getInstance();
            System.out.println(instance);
          };
          new Thread(r).start();
          new Thread(r).start();
    }

    private static void testLazy() {
        System.out.println("1");
        Singleton_DoubleCheck.hello();
        System.out.println("2");
        var instance = Singleton_DoubleCheck.getInstance();
    }

    /**
     * 反射破坏单例
     */
    private static void testReflect() {
        var c = Singleton_DoubleCheck.class;
        try {
            var ctor = c.getDeclaredConstructor();
            var instance = ctor.newInstance();
            var instance2 = ctor.newInstance();
            System.out.println(instance == instance2);// false 不是同一个对象
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
