package designpattern.create;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

// 默认user
public class BuilderTestMain{
    public static void main(String[] args) {
        User user = new User.Builder("000000", "123456")
        .id(0)
        .birthday(LocalDate.now())
        .registration(LocalDate.now())
        .name("Quanwei")
        .gender("male")
        .build();
        System.out.println(user);
    }
}
class User implements Serializable {
    private int id;
    private String name;
    private String gender;
    private LocalDate registration;
    private LocalDate birthday;
    private String account;
    private String password;

    private User(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.gender = builder.gender;
        this.registration = builder.registration;
        this.birthday = builder.birthday;
        this.account = builder.account;
        this.password = builder.password;
    }

    /**
     * 使用Builder模式
     */
    public static class Builder {
        // Optional
        private int id;
        private String name;
        private String gender;
        private LocalDate registration;
        private LocalDate birthday;
        // Required
        private final String account;
        private final String password;

        public Builder(String account, String password) {
            this.account = account;
            this.password = password;
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }
        public Builder gender(String gender){
            this.gender=gender;
            return this;
        }

        public Builder registration(LocalDate registration) {
            this.registration = registration;
            return this;
        }

        public Builder birthday(LocalDate birthday) {
            this.birthday = birthday;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    public User() {
    }

    public String getAccount() {
        return account;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getGender() {
        return gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public LocalDate getRegistration() {
        return registration;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", registration=" + registration +
                ", birthday=" + birthday +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof User user))
            return false;
        return getAccount().equals(user.getAccount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccount());
    }

 
}
