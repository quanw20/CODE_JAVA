package tang.quanwei._6;

/**
 * 饿汉式单例
 */
public class VirtualUserGenerator {
    private static final VirtualUserGenerator INSTANCE = new VirtualUserGenerator();

    private VirtualUserGenerator() {
    }

    public static VirtualUserGenerator getInstance() {
        return INSTANCE;
    }

    public VirtualUser generate() {
        return new VirtualUser();
    }

}
