package tang.quanwei._6;

/**
 * 静态内部类单例
 */
public class VirtualUserGenerator_IoDH {
    private VirtualUserGenerator_IoDH() {
    }

    public static VirtualUserGenerator_IoDH getInstance() {
        return Holder.INSTANCE;
    }

    public VirtualUser generate() {
        return new VirtualUser();
    }

    private static class Holder {
        private static final VirtualUserGenerator_IoDH INSTANCE = new VirtualUserGenerator_IoDH();
    }
}
