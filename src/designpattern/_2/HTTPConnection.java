package tang.quanwei._2;

public class HTTPConnection extends Connection{
    @Override
    public void connect() {
        System.out.println("HTTPConnection.connect");
    }
    @Override
    public void disconnect() {
        System.out.println("HTTPConnection.disconnect");
    }
}
