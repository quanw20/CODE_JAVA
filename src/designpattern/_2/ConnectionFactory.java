package tang.quanwei._2;

public abstract class ConnectionFactory {
  public abstract <T extends Connection> T getConnection(Class<T> type);
}
