package tang.quanwei._2;

public class ConnectionFactoryImpl extends ConnectionFactory {
  @Override
  public <T extends Connection> T getConnection(Class<T> type) {
    try {
      return type.getDeclaredConstructor().newInstance();
    } catch (Exception ignored) {
      System.out.println("cannot create connection");
    }
    return null;
  }
}
